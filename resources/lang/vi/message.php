<?php 
return[
		'tensp_required'         => 'Vui lòng nhập tên.',
		'tensp_unique'         	 => 'Tên sản phẩm đã tồn tại.',
            'danhmuc_required'       => 'Vui lòng chọn danh mục.',
            'type_product_required'  => 'Vui lòng chọn type.',
            'soluong_required'       => 'Vui lòng nhập số lượng.',
            'price_required'         => 'Vui lòng nhập giá.',
            'photos_required'        => 'Vui lòng chọn hình ảnh.',
            
            'title_required'         => 'Vui lòng nhập tiêu đề.',
            'category_required'      => 'Vui lòng chọn danh mục.',
            'photos_required'        => 'Vui lòng chọn hình ảnh.',
            'intro_required'         => 'Vui lòng nhập giới thiệu.',
            'content_required'       => 'Vui lòng nhập nội dung.',

            'name_required'          => 'Vui lòng nhập tên.',
            'name_unique'            => 'Tên đã tồn tại.',
            'vitri_required'         => 'Vui lòng nhập vị trí.',

            'id_required'            => 'Vui lòng nhập mã phiếu.',
            'id_unique'              => 'Mã phiếu đã tồn tại.',
            'begin_required'         => 'Vui lòng nhập ngày bắt đầu.',
            'end_required'           => 'Vui lòng nhập ngày kết thúc',
            'money_required'         => 'Vui lòng nhập số tiền.',
            'address_required'        => 'Vui lòng nhập địa chỉ.',

            'sodtgh_required'         => 'Vui lòng nhập số điện thoại.',
            'sodtgh_unique'           => 'Số điện thoại đã tồn tại.',

            'btn_them'               => 'Thêm',
            'btn_xoa'                => 'Xóa',
            'btn_sua'                => 'Sửa',
            'btn_luu'                => 'Lưu',

            'trangchu'               => 'Trang chủ',
            'cauhinh'                => 'Cấu hình',
            'danhsach'               => 'Danh sách',
            'tintuc'                 => 'Tin tức',
            'danhsach_admin'         => 'Danh sách quản trị viên',
            'danhsach_thanhvien'     => 'Danh sách thành viên',
            'danhsach_tintuc'        => 'Danh sách tin tức',
            'danhsach_phieu'         => 'Danh sách phiếu giảm giá',
            'danhsach_lienhe'        => 'Danh sách liên hệ',
            'danhsach_slide'         => 'Danh sách slide',
            'danhsach_sanpham'       => 'Danh sách sản phẩm',
            'danhsach_hinhthuc'      => 'Danh sách hình thức thanh toán',
            'danhsach_vanchuyen'     => 'Danh sách vận chuyển',
            'danhsach_donhang'       => 'Danh sách đơn hàng',
            'danhmuc_tintuc'         => 'Danh mục tin tức',
            'danhmuc_sanpham'        => 'Danh mục sản phẩm',
            
            'tencongty'              => 'CÔNG TY TNHH BÁCH KHOA',

            'email_email'                 => 'Vui lòng nhập chính xác định dạng email của bạn',
            'email_required'              => 'Vui lòng nhập email.',
            'email_unique'                => 'Email đã tồn tại.',

            'password_required'           => 'Vui lòng nhập mật khẩu.',
            'passwordnew_required'        => 'Mật khẩu mới không được để trống',
            'repeatpasswordnew_required'  => 'Mật khẩu nhập lại không được để trống',
            'passwordnew_min'             => 'Mật khẩu mới phải nhiều hơn 6 ký tự',
            'passwordnew_max'             => 'Mật khẩu mới phải ít hơn 16 ký tự',

            'fullname_required'           => 'Vui lòng nhập họ tên.',
            'fullname_max'                => 'Họ và tên tối đa 50 ký tự',

            'username_required'           => 'Vui lòng nhập tên hiển thị.',
            'username_unique'             => 'Tên hiển thị đã tồn tại.',
            'username_max'                => 'Tên hiển thị tối đa 50 ký tự',

            'phone_required'              => 'Vui lòng nhập số điện thoại.',
            'phone_unique'                => 'Số điện thoại này đã được đăng ký.',
            'phone_min'                   => 'Số điện thoại phải nhiều hơn 10 số',
            'phone_max'                   => 'Số điện thoại phải ít hơn 11 số',

            'province_required'           => 'Province không được để trống',
            'district_required'           => 'District không được để trống',
            'ward_required'               => 'Ward không được để trống',

            'maxacminh_required'          => 'Vui lòng nhập mã xác minh.',
            'maxacminh_min'               => 'Mã xác minh là 6 số',
            'maxacminh_max'               => 'Mã xác minh là 6 số',
            
];
