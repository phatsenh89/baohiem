<!DOCTYPE html>
<html lang="en">
<head>
	@include('admin.blocks.head')
</head>

<body>
	@include('admin.blocks.sidebar')	
	@yield('breadcrumb')
	{{-- @include('admin.blocks.header') --}}
	<div class="page-content pt-0">
	@include('admin.blocks.menu')
	@yield('content')
	</div>
	@include('admin.blocks.footer')
	@include('admin.blocks.script')
</body>
</html>
