@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('news.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Update</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Update News</h5>              
            </div>
            <div class="card-body">
                <form action="{{ route('news.update',['id'=> $news->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-12">
                            <label class="lable_edit">Tiêu đề <span class="text-danger">*</span></label>
                            <input value="{{old('title',$news->title)}}" type="text" name="title" class="form-control" required="" >
                        </div>
                    </div>
                     <div class="form-group col-sm-12 row">                        
                        <div class="col-sm-6">
                            <label class="lable_edit">Mô tả ngắn <span class="text-danger">*</span></label>
                            <textarea rows="1" class="form-control" name="intro" placeholder="Mô tả ngắn">{{old('intro',$news->intro)}}</textarea>
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Trạng thái <span class="text-danger">*</span></label>
                            <select name="status" class="form-control">
                                <option value="0" {{$news->status == 0 ? 'selected' : ''}}>Chưa duyệt bài</option>
                                <option value="1" {{$news->status == 1 ? 'selected' : ''}}>Đã duyệt bài</option>
                            </select>
                        </div>
                        
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-12">                        
                            <label class="lable_edit">Nội dung <span class="text-danger">*</span></label>
                            <textarea rows="1" id="editor1" class="form-control" name="content" placeholder="Nội dung">{{old('content',$news->content)}}</textarea>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection