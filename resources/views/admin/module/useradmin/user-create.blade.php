@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('useradmin.index')}}" class="breadcrumb-item">List Admin</a>
                <span class="breadcrumb-item active">Add</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Add administrator</h5>                
            </div>
            <div class="card-body">
                <form action="{{ route('useradmin.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Fullname <span class="text-danger">*</span></label>
                            <input value="{{old('fullname')}}" type="text" name="fullname" class="form-control" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Username <span class="text-danger">*</span></label>
                            <input value="{{old('username')}}" type="text" name="username" class="form-control" required="">
                        </div>
                    </div>
                     <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Email <span class="text-danger">*</span></label>
                            <input value="{{old('email')}}" type="text" name="email" class="form-control" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Password <span class="text-danger">*</span></label>
                            <input value="{{old('password')}}" type="password" name="password" class="form-control" required="">
                        </div> 
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Role <span class="text-danger">*</span></label>
                            <select name="role" class="form-control pro-edt-select form-control-primary">
                                <option value='0' selected="">Admin</option>
                                    <option value='1'>Technical</option>                                                          
                            </select>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button name="button" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Save
                        </button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection