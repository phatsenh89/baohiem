@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('useradmin.index')}}" class="breadcrumb-item">List Admin</a>
                {{-- <span class="breadcrumb-item active">Current</span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">List of administrators</h5>                
            </div>
            <div class="card-body">
                <div class="product-status-wrap">
                    <div class="form-group">
                        <a class="btn btn-success" href="{{ route('useradmin.create') }}">ADD</a>
                    </div>
                        <div class="table-responsive-sm form-group">
                            <table class="table table-sm table-striped table-bordered">
                                <thead>
                                    <tr >
                                        {{-- <th scope="col">
                                            <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                                        </th> --}}
                                        <th scope="col">ID</th>
                                        <th scope="col">Fullname</th>
                                        <th scope="col">Username</th>
                                        <th scope="col">Staff type</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Created date</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($useradmin as $item)
                                    <tr>
                                       {{--  <td >
                                            <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                                        </td> --}}
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->fullname}}</td>
                                        <td>{{$item->username}}</td>
                                        <td>
                                            @if($item->role==0)
                                                <span class="badge badge-danger">Admin</span>
                                                
                                            @elseif($item->role==1)
                                                <span class="badge badge-success">Technical</span>    
                                            @endif
                                        </td>
                                        <td>{{$item->email}}</td>
                                        <td>{{ date("d/m/Y-H:i:s",strtotime($item ->created_at))}}</td>
                                        <td style="text-align: center;">
                                            <a href="{{ route('useradmin.edit', ['useradmin' => $item->id]) }}" data-toggle="tooltip" title="Edit" class="pd-setting-ed">
                                                <i class="icon-pencil7" aria-hidden="true"></i>
                                            </a>  &nbsp; &nbsp;

                                             <a href="{{ route('useradmin.destroy',['useradmin' => $item->id]) }}" onClick="return xacnhan('Bạn có chắc chắn muốn xóa ?');" class="list-icons-item text-danger-600" title="Delete">
                                                <i class="icon-trash"></i>
                                             </a>
                                        </td>                   
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group row">
                            <div style="position: absolute;right: 20px">
                                {{$useradmin->links()}}
                            </div>
                        </div>                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection