@extends('admin.welcome')
@section('title','Change Password')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('useradmin.index')}}" class="breadcrumb-item">List Admin</a>
                <span class="breadcrumb-item active">Change Password</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">           
            <div class="card-body">
                <form class="form-validate-jquery" action="" method="POST" enctype="multipart/form-data">
                    @csrf
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Change Password</legend>

                         <!-- Password field -->
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3">Old Password<span class="text-danger">*</span></label>
                            <div class="col-lg-4">
                                <input type="password" name="currentpassword" id="password" class="form-control" required="" />
                            </div>
                        </div>
                        <!-- /password field -->

                        <!-- Password field -->
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3">New Password<span class="text-danger">*</span></label>
                            <div class="col-lg-4">
                                <input type="password" name="newpassword" id="password" class="form-control" required placeholder="Minimum 6 characters allowed"/>
                            </div>
                        </div>
                        <!-- /password field -->

                        <!-- Repeat password -->
                        <div class="form-group row">
                           <label class="col-form-label col-lg-3">Enter a new password<span class="text-danger">*</span></label>
                           <div class="col-lg-4">
                               <input type="password" name="newpassword_confirmation" class="form-control" required placeholder="Try different password"/>
                           </div>
                       </div>
                        <!-- /repeat password -->

                    </fieldset>

                    <div class="d-flex justify-content-center align-items-center" style="margin-right: 260px">
                        <button type="submit" class="btn btn-primary ml-4">Change<i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection