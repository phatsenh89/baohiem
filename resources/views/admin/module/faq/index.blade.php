@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('faq.index')}}" class="breadcrumb-item">Danh sách</a>
                {{-- <span class="breadcrumb-item active">Current</span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">List Contact</h5>                
            </div>
            <div class="card-body">
                <div class="product-status-wrap">
                    <div class="form-group">
                        <a class="btn btn-success" href="{{ route('faq.create') }}">ADD</a>
                    </div>
                    <form action="{{route('faq.destroy',['action' => 'delete'])}}" name="listForm" id="listForm" method="POST" >
                        @csrf
                        <div class="table-responsive-sm form-group">
                            <table class="table table-sm table-striped table-bordered">
                                <thead>
                                    <tr >
                                        <th scope="col">
                                            <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                                        </th>
                                        <th scope="col">ID</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Content</th>
                                        <th scope="col">Created date</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($faq as $item)
                                    <tr>
                                        <td >
                                            <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                                        </td>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->content}}</td>
                                        <td>{{ date("d/m/Y-H:i:s",strtotime($item ->created_at))}}</td>
                                        <td style="text-align: center;">
                                            <a href="{{ route('faq.edit', ['faq' => $item->id]) }}" data-toggle="tooltip" title="Edit" class="pd-setting-ed">
                                                <i class="icon-pencil7" aria-hidden="true"></i>
                                            </a>  
                                        </td>                   
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group row">
                            <button onclick="return acceptDelete()" type="submit" data-toggle="tooltip" title="Xóa" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                            <div style="position: absolute;right: 20px">
                                {{$faq->links()}}
                            </div>
                        </div>
                     </form>                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection