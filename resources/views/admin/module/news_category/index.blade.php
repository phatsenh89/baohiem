@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('news_category.index')}}" class="breadcrumb-item">Danh sách</a>
                {{-- <span class="breadcrumb-item active">Current</span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content-wrapper">
    @include('client.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Danh mục </h5>               
            </div>

            <div class="card-body">
                <div class="product-status-wrap">
                    <div class="form-group">
                        <a class="btn btn-success" href="{{ route('news_category.create') }}">{{trans('message.btn_them')}}</a>
                    </div>  
                     <form action="{{route('news_category.destroy')}}" name="listForm" id="listForm" method="GET" >
                        @csrf            
                        <div class="table-responsive-sm form-group">
                            <table class="table table-sm table-striped table-bordered">
                                <thead>
                                    <tr >
                                        <th scope="col">
                                            <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                                        </th>
                                        <th scope="col">ID</th>
                                        <th scope="col">Tên danh mục</th>                                       
                                        <th scope="col">Slug</th>
                                        <th scope="col">Thứ tự</th>
                                        <th scope="col">Hoạt động</th>                                      
                                        <th scope="col">Cập nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($news_category as $item)
                                    <tr>
                                        <td >
                                            <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                                        </td>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->slug}}</td>
                                        <td>{{$item->position}}</td>                                        
                                        <td>
                                                {{convertdate1($item->created_at)}}
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="{{ route('news_category.edit', ['news_category' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                                <i class="icon-pencil7" aria-hidden="true"></i>
                                            </a>
                                        </td>                   
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                         <div class="form-group">
                            {{ method_field('DELETE') }}
                           <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaListCate" type="submit" data-toggle="tooltip" title="Xóa" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </div>
                    
                    </form>
                     
                     {{$news_category->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection