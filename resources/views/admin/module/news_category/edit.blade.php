@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('news_category.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Sửa</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('client.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Cập nhật Category</h5>              
            </div>
            <div class="card-body">
                <form action="{{ route('news_category.update',['news_category' => $news_category->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Danh mục </label>
                            <select name="id_parent" class="form-control">
                                <option value="0">ROOT</option>                                
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Tên danh mục *</label>
                            <input value="{{old('name',$news_category->name)}}" type="text" name="name" class="form-control" required="" >
                        </div>
                    </div>
                     <div class="form-group col-sm-12 row">                        
                        <div class="col-sm-6">
                            <label class="lable_edit">Thứ tự *</label>
                            <input value="{{old('position',$news_category->position)}}" type="text" name="position" class="form-control" >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection