{{-- @extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('lienhe.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">ADD</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">ADD Contact</h5>              
            </div>
            <div class="card-body">
                <form action="{{ route('lienhe.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Tên User <span class="text-danger">*</span></label>
                            <input value="{{old('name')}}" type="text" name="name" class="form-control" required="" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Email <span class="text-danger">*</span></label>
                            <input value="{{old('email')}}" type="email" name="email" class="form-control" required="" >
                        </div>
                    </div>
                     <div class="form-group col-sm-12 row">                        
                        <div class="col-sm-6">
                            <label class="lable_edit">Mô tả ngắn <span class="text-danger">*</span></label>
                            <textarea rows="1" class="form-control" name="message" placeholder="Mô tả ngắn">{{old('message')}}</textarea>
                        </div>
                      
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection --}}