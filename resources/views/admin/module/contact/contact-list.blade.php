@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('contact.index')}}" class="breadcrumb-item">Contact list</a>
                {{-- <span class="breadcrumb-item active">Current</span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content-wrapper">
    @include('client.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Contact list</h5>               
            </div>
            <div class="card-body">
                <div class="product-status-wrap">
                <form action="" name="listForm" id="listForm" method="post" >
                   @csrf          
                    <div class="table-responsive-sm form-group">
                        <table class="table table-sm table-striped table-bordered">
                            <thead>
                                <tr >
                                   {{--  <th scope="col">
                                        <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                                    </th> --}}
                                    <th scope="col">ID</th>
                                    <th scope="col">Fullname</th>                                       
                                    <th scope="col">Phone</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Content</th>
                                    <th scope="col">Created at </th>
                                    <th scope="col">Update</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contact as $item)
                                <tr>
                                   {{--  <td >
                                        <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                                    </td> --}}
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->fullname}}</td>
                                    <td>{{$item->phone}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->content}}</td>
                                    <td>
                                            {{convertdate($item->created_at)}}
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="{{ route('contact.edit', ['contact' => $item->id]) }}" data-toggle="tooltip" title="Edit" class="pd-setting-ed">
                                            <i class="icon-pencil7" aria-hidden="true"></i>
                                        </a>
                                    </td>                   
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>  
                        <div class="form-group row">
                            <div style="position: absolute;right: 20px">
                                {{$contact->links()}}
                            </div>
                        </div> 
                </form> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection