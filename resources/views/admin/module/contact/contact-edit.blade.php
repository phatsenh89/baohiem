@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('contact.index')}}" class="breadcrumb-item">Contact list</a>
                <span class="breadcrumb-item active">Edit</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content-wrapper">
    @include('client.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Update contact</h5>              
            </div>
            <div class="card-body">
                <form action="{{route('contact.update', ['contact' => $contact->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Fullname <span class="text-danger">*</span></label>
                            <input value="{{old('fullname',$contact->fullname)}}" type="text" name="fullname" class="form-control" readonly="">
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Email <span class="text-danger">*</span></label>
                            <input value="{{old('email',$contact->email)}}" type="text" name="email" class="form-control" readonly="">
                        </div>
                    </div>
                     <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Phone <span class="text-danger">*</span></label>
                            <input value="{{old('phone',$contact->phone)}}" type="number" name="phone" class="form-control"  >
                        </div>                        
                       <div class="col-sm-6">
                            <label class="lable_edit">Content <span class="text-danger">*</span></label>                           
                            <textarea rows="1" class="form-control" placeholder="Please enter content" name="content">{{old('content',$contact->content)}}</textarea>
                        </div>
                    </div>
                  
                    <div class="form-group text-center">                      
                        <button name="addProduct" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Update</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>


@endsection