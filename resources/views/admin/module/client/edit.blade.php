@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('client.index')}}" class="breadcrumb-item">Danh sách Client</a>
                <span class="breadcrumb-item active">Cập nhật</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Update Contact</h5>              
            </div>
            <div class="card-body">
                <form action="{{ route('client.update',['id' => $client->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Họ tên Client <span class="text-danger">*</span></label>
                            <input value="{{old('fullname',$client->fullname)}}" type="text" name="fullname" class="form-control" required="" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Phone <span class="text-danger">*</span></label>
                            <input value="{{old('phone',$client->phone)}}" type="number" name="phone" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">                        
                        <div class="col-sm-6">
                            <label class="d-block font-weight-semibold">Khu vực <span class="text-danger">*</span></label>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="khuvuc" value='1' {{$client->khuvuc == 1 ? 'checked' : ''}} >
                                    TP. Hồ Chí Minh 
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="khuvuc" value='2' {{$client->khuvuc == 2 ? 'checked' : ''}}>
                                    Bình Dương
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="khuvuc" value='3' {{$client->khuvuc == 3 ? 'checked' : ''}}>
                                   Đồng Nai 
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="khuvuc" value='4' {{$client->khuvuc == 4 ? 'checked' : ''}}>
                                   Hà Nội
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="khuvuc" value='5' {{$client->khuvuc == 5 ? 'checked' : ''}}>
                                    Nơi khác 
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="d-block font-weight-semibold">Thu nhập <span class="text-danger">*</span></label>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="thunhap" value='1' {{$client->thunhap == 1 ? 'checked' : ''}}>
                                    1 - 2 triệu VNĐ
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="thunhap" value='2' {{$client->thunhap == 2 ? 'checked' : ''}}>
                                    2 - 4 triệu VNĐ
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="thunhap" value='3' {{$client->thunhap == 3 ? 'checked' : ''}}>
                                  4 - 8 triệu VNĐ
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="thunhap" value='4' {{$client->thunhap == 4 ? 'checked' : ''}}>
                                  Trên 8 triệu VNĐ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-3">
                            <label class="lable_edit">Ngày sinh </label>
                                <select name="ngaysinh" class="form-control" style="border-radius: 20px " >
                                    @for($i =1; $i<=31; $i++)
                                       <option value="{{$i}}" {{$client->ngaysinh == $i ? 'selected' : ''}}>{{$i}}</option>
                                    @endfor   
                                </select>
                        </div>
                        <div class="col-sm-4">
                            <label class="lable_edit">Tháng sinh </label>
                                <select name="thangsinh" class="form-control" style="border-radius: 20px ">
                                    @for($i =1; $i<=12; $i++)
                                       <option value="{{$i}}" {{$client->thangsinh == $i ? 'selected' : ''}}>Tháng {{$i}}</option>
                                    @endfor   
                                </select>
                        </div>
                        <div class="col-sm-4">
                            <label class="lable_edit">Năm sinh </label>
                                <select name="namsinh" class="form-control" style="border-radius: 20px ">
                                    @for($i =1933; $i<=2019; $i++)
                                       <option value="{{$i}}" {{$client->namsinh == $i ? 'selected' : ''}}>Năm {{$i}}</option>
                                    @endfor   
                                </select>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Cập nhật
                        </button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection