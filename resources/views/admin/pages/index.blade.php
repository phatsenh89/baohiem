@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                {{-- <a href="#" class="breadcrumb-item">Link</a>
                <span class="breadcrumb-item active">Current</span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content-wrapper">
        <p style="text-align: center">Chào mừng bạn đến với trang quản trị website bảo hiểm nhân thọ.</p>

</div>
@endsection