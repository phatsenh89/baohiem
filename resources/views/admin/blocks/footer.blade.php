<div class="navbar navbar-expand-lg navbar-light">
        <div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-footer">
                <i class="icon-circle-down2"></i>
            </button>

        </div>

        <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text">
               &copy; 2019. <a href="#">Trang Quản Trị</a> Design by <a href="https://cloudone.vn/" target="_blank">CloudOne</a>
            </span>

            <ul class="navbar-nav ml-lg-auto">
                <li class="nav-item"><a class="navbar-nav-link" href="https://cloudone.vn/ve-chung-toi/">About</a></li>
                <li class="nav-item"><a class="navbar-nav-link" href="#">Terms</a></li>
                <li class="nav-item"><a class="navbar-nav-link" href="https://cloudone.vn/lien-he">Contact</a></li>
            </ul>
        </div>
    </div>