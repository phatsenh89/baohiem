<div class="navbar navbar-expand-md navbar-dark">
		<div class="navbar-brand wmin-200">
			<a href="{{route('dashboard')}}" class="d-inline-block">
				<img src="{{ asset('backend/global_assets/images/logoCPM.png') }}" alt="" style="height: 30px">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">

				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						<img src="{{ asset('backend/global_assets/images/fbbc0.jpg') }}" class="rounded-circle mr-2" height="34" alt="">
						<span>{{Auth::user()->username}}</span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a href="{{route('useradmin.change-password')}}" class="dropdown-item"><i class="icon-key"></i>Change Password</a>	
						<div class="dropdown-divider"></div>
							<a href="{{ route('useradmin.edit',[Auth::user()->id]) }}" class="dropdown-item"><i class="icon-cog5"></i>Account settings</a>
							<a href="{{route('getLogout')}}" class="dropdown-item"><i class="icon-switch2"></i>Logout</a>
					</div>
				</li>
			</ul>
		</div>
	</div>