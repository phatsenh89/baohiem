@if(session('alertsuc'))
{{-- <section class="alert alert-success alert-st-one" role="alert">
    <i class="fa fa-check adminpro-checked-pro admin-check-pro" aria-hidden="true"></i>
        <p class="message-mg-rt">
            {{ session('alertsuc') }}
        </p>
</section> --}}
<{{-- div class="alert alert-success" role="alert">
  <p>This is a success alert—check it out!</p>
</div> --}}
<script type="text/javascript">
    $.jGrowl('{{ session('alertsuc') }}', {
        theme: 'bg-success alert-rounded',
        life: 5000
    });
</script>
@endif
@if(session('alerterr'))
{{-- <section class="alert alert-danger alert-mg-b alert-st-four" role="alert">
    <i class="fa fa-window-close adminpro-danger-error admin-check-pro" aria-hidden="true"></i>
        <i class="fa fa-times adminpro-danger-error admin-check-pro" aria-hidden="true"></i>
        <p class="message-mg-rt">
            {{ session('alerterr') }}
        </p>
</section> --}}
<script type="text/javascript">
    $.jGrowl('{{ session('alerterr') }}', {
        theme: 'bg-danger alert-rounded',
        life: 5000
    });
</script>

@endif
@if(session('alerterrv2'))
<section class="alert alert-danger alert-mg-b" role="alert">
    <p style="font-weight: bold;">{{ session('alerterrv2') }}</p>
</section>
@endif
@if($errors->any())
<section class="alert alert-danger alert-mg-b" role="alert">
    @foreach ($errors->all() as $error)
        <span class="message-mg-rt">{{ $error }}</span><br>
    @endforeach
</section>
@endif
@if(session('alertwar'))
<script type="text/javascript">
   
            new Noty({
                layout: 'top',
                text: '{{session('alertwar')}}',
                type: 'success',
                theme: 'limitless',
                timeout: 10000
            }).show();  
    

</script>
@endif
@if(session('alertwar2'))
<div class="alert alert-info alert-success-style2 alert-success-stylenone">
    <i class="fa fa-info-circle adminpro-inform admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
    <p class="message-alert-none">{{ session('alertwar2') }}</p>
</div>
@endif
@if(session('alertsweet'))
<script type="text/javascript">
swal({
    // title: 'Good job!',
    html: '{!! session('alertsweet') !!}',
    type: 'success',
    buttonsStyling: false,
    confirmButtonClass: 'btn btn-primary',
    cancelButtonClass: 'btn btn-light'
});</script>
@endif

@if(session('alerterrbilling'))
<script type="text/javascript">
    swal({
          // title: 'Good job!',
          html: '<strong>{{session('alerterrbilling')}}</strong>',
          type: 'error',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary',
          cancelButtonClass: 'btn btn-light'
      });
</script>
@endif

@if (\Session::has('success'))
<div role="alert" id="flash-message">
    <div class="alert alert-success" >
      <h6 class="card-title">
        <p>Success</a>
      </h6>
    </div>
    {{-- <div id="collapsible-styled-group1" class="collapse show" style="">
      <div class="card-body">
          {!! __(\Session::get('success')) !!}
      </div>
    </div> --}}
</div>
@endif
<!-- --------------------error ----------------- -->
@if (\Session::has('error'))
<div role="alert" id="flash-message">
    <div class="card-header bg-danger">
      <h6 class="card-title">
        <a data-toggle="collapse" class="text-white" aria-expanded="true">Error</a>
      </h6>
    </div>
   {{--  <div id="collapsible-styled-group1" class="collapse show" style="">
      <div class="card-body">
          {!! __(\Session::get('error')) !!}
      </div>
    </div> --}}
</div>
@endif
