<div class="sidebar sidebar-light sidebar-main sidebar-expand-md align-self-start">
    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Main sidebar
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->
    <!-- Sidebar content -->
    <div class="sidebar-content">
        <div class="card card-sidebar-mobile">
            <!-- Header -->
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Main</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <!-- /header -->
            <!-- User menu -->
            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media">
                        <div class="mr-3">
                            <a href="#"><img src="{{ asset('backend/global_assets/images/fbbc0.jpg') }}" width="38" height="38" class="rounded-circle" alt=""></a>
                        </div>
                        <div class="media-body">
                            <div class="media-title font-weight-semibold">{{Auth::user()->fullname}}</div>
                            <div class="font-size-xs opacity-50">
                                <i style="color: green" class="icon-primitive-dot"></i> &nbsp;Online
                                {{-- /*<span style="color: green;font-size: 20px">•</span> Online*/ --}}
                            </div>
                        </div>
                        <div class="ml-3 align-self-center">
                            <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /user menu -->
            <!-- Main navigation -->
            <div class="card-body p-0">
                
                    <ul class="nav nav-sidebar" data-nav-type="accordion">
                        <!-- Main -->
                        <li class="nav-item">
                            <a href="{{ route('dashboard') }}" class="nav-link active">
                                <i class="icon-home4"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        {{-- <li class="nav-item ">
                            <a href="{{route('useradmin.index')}}" class="nav-link"><i class="icon-users"></i> <span>Account management</span></a>
                        </li> --}}
                        <li class="nav-item nav-item-submenu">
                            <a href="#" class="nav-link"><i class="icon-users"></i> <span>Account management</span></a>
                            <ul class="nav nav-group-sub" data-submenu-title="Tài khoản">
                                <li class="nav-item"><a href="{{route('useradmin.index')}}" class="nav-link">Staffs</a></li>
                                <li class="nav-item"><a href="{{route('client.index')}}" class="nav-link">Clients</a></li>
                            </ul>
                        </li>
                        <li class="nav-item ">
                            <a href="{{route('menu.index')}}" class="nav-link"><i class="icon-menu3"></i> <span>Menu</span></a>
                        </li>
                        <li class="nav-item ">
                            <a href="{{route('faq.index')}}" class="nav-link"><i class="icon-question3"></i> <span>FAQ</span></a>
                        </li>
                        <li class="nav-item ">
                            <a href="{{route('news.index')}}" class="nav-link"><i class="icon-stack"></i> <span>Tin Tức</span></a>
                        </li>
                        <li class="nav-item ">
                            <a id="menu_seen" href="{{route('lienhe.index')}}" class="nav-link"><i class="icon-phone"></i> <span>Liên hệ</span></a>
                        </li>   
                        <li class="nav-item"><a href="{{route('cauhinh.editNoId')}}" class="nav-link"><i class="icon-cog5"></i> <span>Setup</span></a></li>
                        <!-- /main -->
                    </ul>
              
            </div>
            <!-- /main navigation -->
        </div>
    </div>
    <!-- /sidebar content -->
</div>