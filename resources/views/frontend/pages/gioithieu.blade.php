@extends('frontend.welcome')
@section('content')
        <div id="et-main-area">
            <div id="main-content">
                <div class="container">
                    <div id="content-area" class="clearfix">
                        <div id="left-area">
                            <article id="post-856" class="post-856 page type-page status-publish hentry">
                                <h1 class="entry-title main_title">Giới thiệu</h1>
                                <div class="entry-content">
                                    <p style="text-align: justify;">{!! $gioithieu->chitiet !!}</p>
                                   
                                    <p style="text-align: justify;">
                                        <p style="text-align: right;"><strong>Ban quản trị website</strong></p>
                                        <p style="text-align: right;"><em>Ngày 10 tháng 12 năm 2019</em></p>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div> <span class="et_pb_scroll_top et-pb-icon"></span>
        </div>
    </div>
    <script type='text/javascript'>
        var kk_star_ratings = {
            "action": "kk-star-ratings",
            "endpoint": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "nonce": "d1f43d2b8a"
        };
    </script>
    <script type='text/javascript'>
        var DIVI = {
            "item_count": "%d Item",
            "items_count": "%d Items"
        };
        var et_shortcodes_strings = {
            "previous": "Previous",
            "next": "Next"
        };
        var et_pb_custom = {
            "ajaxurl": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/images",
            "builder_images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/includes\/builder\/images",
            "et_frontend_nonce": "6677073e6b",
            "subscription_failed": "Please, check the fields below to make sure you entered the correct information.",
            "et_ab_log_nonce": "0014a96523",
            "fill_message": "Please, fill in the following fields:",
            "contact_error_message": "Please, fix the following errors:",
            "invalid": "Invalid email",
            "captcha": "Captcha",
            "prev": "Prev",
            "previous": "Previous",
            "next": "Next",
            "wrong_captcha": "You entered the wrong number in captcha.",
            "ignore_waypoints": "no",
            "is_divi_theme_used": "1",
            "widget_search_selector": ".widget_search",
            "is_ab_testing_active": "",
            "page_id": "856",
            "unique_test_id": "",
            "ab_bounce_rate": "5",
            "is_cache_plugin_active": "no",
            "is_shortcode_tracking": "",
            "tinymce_uri": ""
        };
        var et_pb_box_shadow_elements = [];
    </script>
    <script type="text/javascript" defer src="{{asset('frontend/wp-content/cache/autoptimize/js/autoptimize_490080239d4ee600f5a141052b340c7b.js')}}"></script>
@endsection
