<!DOCTYPE html>
<html lang="en-US">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="pingback" href="../xmlrpc.php" />
    <script>
        var et_site_url = '../index.html';
        var et_post_id = '366';

        function et_core_page_resource_fallback(a, b) {
            "undefined" === typeof b && (b = a.sheet.cssRules && 0 === a.sheet.cssRules.length);
            b && (a.onerror = null, a.onload = null, a.href ? a.href = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id : a.src && (a.src = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id))
        }
    </script>
    <link type="text/css" media="all" href="{{asset('frontend/wp-content/cache/autoptimize/css/autoptimize_4bb63c6e7d996687ade64afedbc1ebf6.css')}}" rel="stylesheet" />
    <title>@yield('title','Tư vấn miễn phí mua bảo hiểm nhân thọ cho con (trẻ em)')</title>
   {{--  <meta name="description" content="Bạn có nhu cầu mua bảo hiểm nhân thọ cho con trẻ ? Chúng tôi tư vấn miễn phí, giúp bạn xác định gói sản phẩm phù hợp nhất với nhu cầu và khả năng tài chính!" />
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <link rel="canonical" href="index.html" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Tư vấn miễn phí mua bảo hiểm nhân thọ cho con (trẻ em)" />
    <meta property="og:description" content="Bạn có nhu cầu mua bảo hiểm nhân thọ cho con trẻ ? Chúng tôi tư vấn miễn phí, giúp bạn xác định gói sản phẩm phù hợp nhất với nhu cầu và khả năng tài chính!" />
    <meta property="og:url" content="https://netbaohiem.com/bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh/" />
    <meta property="og:site_name" content="Net Bảo Hiểm" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Bạn có nhu cầu mua bảo hiểm nhân thọ cho con trẻ ? Chúng tôi tư vấn miễn phí, giúp bạn xác định gói sản phẩm phù hợp nhất với nhu cầu và khả năng tài chính!" />
    <meta name="twitter:title" content="Tư vấn miễn phí mua bảo hiểm nhân thọ cho con (trẻ em)" /> --}}
    {{-- <script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://netbaohiem.com/#website","url":"https://netbaohiem.com/","name":"Net B\u1ea3o Hi\u1ec3m","description":"T\u01b0 V\u1ea5n Mi\u1ec5n Ph\u00ed B\u1ea3o Hi\u1ec3m Nh\u00e2n Th\u1ecd","potentialAction":{"@type":"SearchAction","target":"https://netbaohiem.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://netbaohiem.com/bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh/#webpage","url":"https://netbaohiem.com/bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh/","inLanguage":"en-US","name":"T\u01b0 v\u1ea5n mi\u1ec5n ph\u00ed mua b\u1ea3o hi\u1ec3m nh\u00e2n th\u1ecd cho con (tr\u1ebb em)","isPartOf":{"@id":"https://netbaohiem.com/#website"},"datePublished":"2015-05-11T17:42:25+00:00","dateModified":"2019-09-10T17:45:59+00:00","description":"B\u1ea1n c\u00f3 nhu c\u1ea7u mua b\u1ea3o hi\u1ec3m nh\u00e2n th\u1ecd cho con tr\u1ebb ? Ch\u00fang t\u00f4i t\u01b0 v\u1ea5n mi\u1ec5n ph\u00ed, gi\u00fap b\u1ea1n x\u00e1c \u0111\u1ecbnh g\u00f3i s\u1ea3n ph\u1ea9m ph\u00f9 h\u1ee3p nh\u1ea5t v\u1edbi nhu c\u1ea7u v\u00e0 kh\u1ea3 n\u0103ng t\u00e0i ch\u00ednh!","breadcrumb":{"@id":"https://netbaohiem.com/bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh/#breadcrumb"}},{"@type":"BreadcrumbList","@id":"https://netbaohiem.com/bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh/#breadcrumb","itemListElement":[{"@type":"ListItem","position":1,"item":{"@type":"WebPage","@id":"https://netbaohiem.com/","url":"https://netbaohiem.com/","name":"Home"}},{"@type":"ListItem","position":2,"item":{"@type":"WebPage","@id":"https://netbaohiem.com/bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh/","url":"https://netbaohiem.com/bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh/","name":"BHNT cho tr\u1ebb nh\u1ecf"}}]}]}</script> --}}
    {{-- <link rel='dns-prefetch' href='http://fonts.googleapis.com/' /> --}}
    {{-- <link rel='dns-prefetch' href='http://s.w.org/' /> --}}
    {{-- <link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Feed" href="../feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Comments Feed" href="../comments/feed/index.html" /> --}}
    <meta content="Divi v.3.29.3" name="generator" />
    <link rel='stylesheet' id='et-builder-googlefonts-cached-css' href='https://fonts.googleapis.com/css?family=Merriweather%3A300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic%7CLora%3Aregular%2Citalic%2C700%2C700italic%7COpen+Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&amp;ver=5.3#038;subset=cyrillic,latin,latin-ext,cyrillic-ext,vietnamese,greek,greek-ext' type='text/css' media='all' />
    <script type='text/javascript' src='{{asset("frontend/wp-includes/js/jquery/jquery4a5f.js?ver=1.12.4-wp")}}'></script>
   {{--  <link rel='https://api.w.org/' href='../wp-json/index.html' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="../xmlrpc0db0.php?rsd" />
    <meta name="generator" content="WordPress 5.3" />
    <link rel='shortlink' href='../index7aa0.html?p=366' />
    <link rel="alternate" type="application/json+oembed" href="../wp-json/oembed/1.0/embed7304.json?url=https%3A%2F%2Fnetbaohiem.com%2Fbao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh%2F" />
    <link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embed6c5b?url=https%3A%2F%2Fnetbaohiem.com%2Fbao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh%2F&amp;format=xml" /> --}}
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="shortcut icon" href="{{asset('frontend/wp-content/uploads/2015/05/favicon-1-netbaohiem.png')}}" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-147696565-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>

<body data-rsssl=1 class="page-template-default page page-id-366 et_pb_button_helper_class et_transparent_nav et_fixed_nav et_show_nav et_cover_background et_pb_side_nav_page et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_pb_pagebuilder_layout et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
    <div id="page-container">
        <header id="main-header" data-height-onload="67">
           @include('frontend.blocks.menu')
        </header>
        <div id="et-main-area">
            <div id="main-content">
                <article id="post-366" class="post-366 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div id="et-boc" class="et-boc">
                            <div class="et_builder_inner_content et_pb_gutters3">
                                <div class="et_pb_section et_pb_section_0 et_pb_with_background et_section_regular">
                                    <div class="et_pb_row et_pb_row_0">
                                        <div class="et_pb_column et_pb_column_2_3 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_dark  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                    <p>Tích lũy. Tiết kiệm. Sức khỏe.
                                                        <br />Gói sản phẩm dành riêng cho bé.</p>
                                                </div>
                                            </div>
                                            <div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_dark  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                    <p>Nhập thông tin vào mẫu để được tư vấn miễn phí các sản phẩm BHNT tốt nhất cho con em bạn.</p>
                                                </div>
                                            </div>
                                            <div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_dark  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                    <p>Chi phí học Đại học, kết hôn, mua nhà, khởi nghiệp&#8230;con bạn cần sự hỗ trợ từ gia đình rất nhiều. Bên cạnh đó, các quyền lợi bảo hiểm (tai nạn, viện phí, bệnh tật&#8230;) sẽ đi song song trong quá trình tiết kiệm. Tham gia Bảo hiểm Nhân thọ và lập kế hoạch tài chính ngay từ bây giờ sẽ giúp con bạn có một cuộc sống tốt hơn trong tương lai.</p>
                                                </div>
                                            </div>
                                            <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_right et_pb_module "> <a class="btn btn-primary" href="#two">Tìm hiểu thêm</a></div>

                                        </div>
                                        <div class="et_pb_column et_pb_column_1_3 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_text et_pb_text_3 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                    <div class="frm_forms  with_frm_style frm_style_formidable-style" id="frm_form_10_container">
                                                        <form enctype="multipart/form-data" action="{{route('tu-van-mien-phi')}}" method="post" class="frm-show-form " id="form_mua6uc2222">
                                                            @csrf
                                                            <div class="frm_form_fields ">
                                                                <fieldset>
                                                                    <legend class="frm_hidden">Đăng ký tư vấn BHNT</legend>
                                                                    <h3>Đăng ký tư vấn BHNT</h3>
                                                                    <div class="frm_fields_container">
                                                                        <input type="hidden" name="frm_action" value="create" />
                                                                        <input type="hidden" name="form_id" value="10" />
                                                                        <input type="hidden" name="frm_hide_fields_10" id="frm_hide_fields_10" value="" />
                                                                        <input type="hidden" name="form_key" value="mua6uc2222" />
                                                                        <input type="hidden" name="item_meta" value="" />
                                                                        <input type="hidden" id="frm_submit_entry_10" name="frm_submit_entry_10" value="c0f555c47e" />
                                                                        <input type="hidden" name="_wp_http_referer" value="/bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh/" />
                                                                        <label for="frm_verify_10" class="frm_screen_reader frm_hidden">If you are human, leave this field blank.</label>
                                                                        <input type="text" class="frm_hidden frm_verify" id="frm_verify_10" name="frm_verify" value="" />
                                                                        <div id="frm_field_72_container" class="frm_form_field form-field  frm_top_container horizontal_radio">
                                                                            <label class="frm_primary_label">Nơi bạn đang sinh sống <span class="frm_required"></span> </label>
                                                                            <div class="frm_opt_container">
                                                                                <div class="frm_radio" id="frm_radio_72-0">
                                                                                    <label for="field_ceby2a3-0">
                                                                                        <input type="radio" name="khuvuc" id="field_ceby2a3-0" value="1" data-invmsg="Nơi bạn đang sinh sống is invalid" /> TP. Hồ Chí Minh</label>
                                                                                </div>
                                                                                <div class="frm_radio" id="frm_radio_72-1">
                                                                                    <label for="field_ceby2a3-1">
                                                                                        <input type="radio" name="khuvuc" id="field_ceby2a3-1" value="2" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Bình Dương</label>
                                                                                </div>
                                                                                <div class="frm_radio" id="frm_radio_72-2">
                                                                                    <label for="field_ceby2a3-2">
                                                                                        <input type="radio" name="khuvuc" id="field_ceby2a3-2" value="3" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Đồng Nai</label>
                                                                                </div>
                                                                                <div class="frm_radio" id="frm_radio_72-3">
                                                                                    <label for="field_ceby2a3-3">
                                                                                        <input type="radio" name="khuvuc" id="field_ceby2a3-3" value="4" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Hà Nội</label>
                                                                                </div>
                                                                                <div class="frm_radio" id="frm_radio_72-4">
                                                                                    <label for="field_ceby2a3-4">
                                                                                        <input type="radio" name="khuvuc" id="field_ceby2a3-4" value="5" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Nơi khác</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="frm_field_73_container" class="frm_form_field form-field  frm_required_field frm_left_container frm_inline">
                                                                            <label for="field_kh7lgy33" class="frm_primary_label">Họ và tên <span class="frm_required">*</span> </label>
                                                                            <input type="text" id="field_kh7lgy33" name="fullname" value="" style="width:400px" placeholder="Họ và tên của người tham gia BHNT" data-reqmsg="Không thể để trống thông tin này." aria-required="true" data-invmsg="Họ và tên is invalid" class="auto_width" />
                                                                        </div>
                                                                        <div id="frm_field_74_container" class="frm_form_field form-field  frm_required_field frm_left_container frm_inline">
                                                                            <label for="field_oodfpr33" class="frm_primary_label">Số điện thoại <span class="frm_required">*</span> </label>
                                                                            <input type="text" id="field_oodfpr33" name="phone" value="" style="width:400px" placeholder="Số điện thoại để liên lạc" data-reqmsg="Không thể để trống thông tin này." aria-required="true" data-invmsg="Số điện thoại is invalid" class="auto_width" />
                                                                        </div>
                                                                        <input type="hidden" name="item_key" value="" />
                                                                        <div class="frm_submit">
                                                                            <input type="submit" value="Gửi yêu cầu tư vấn" /> 
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="two" class="et_pb_section et_pb_section_1 et_section_regular section_has_divider et_pb_bottom_divider">
                                    <div class="et_pb_row et_pb_row_1">
                                        <div class="et_pb_column et_pb_column_4_4 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_text et_pb_text_4 et_pb_bg_layout_light  et_pb_text_align_center">
                                                <div class="et_pb_text_inner">
                                                    <p><span style="color: #4a4a4a;">Tham gia Bảo hiểm Nhân Thọ chỉ với 4 bước:</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="et_pb_row et_pb_row_2 et_pb_equal_columns">
                                        <div class="et_pb_column et_pb_column_1_4 et_pb_column_3  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_blurb et_pb_blurb_0 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
                                                <div class="et_pb_blurb_content">
                                                   {{--  <div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_off et-pb-icon">&#x68;</span></span>
                                                    </div> --}}
                                                    <div class="et_pb_blurb_container">
                                                        <h4 class="et_pb_module_header"><span style="color: blue;font-weight: 800;">1. Nhập thông tin</span></h4>
                                                        <div class="et_pb_blurb_description">
                                                            <p>Bạn nhập thông tin theo mẫu có sẵn. <span style="text-decoration: underline;"><span style="color: #3366ff;"><a style="color: #3366ff; text-decoration: underline;" href="{{route('tu-van-mien-phi')}}" rel="nofollow">Nhấn vào đây</a></span></span> để nhập thông tin</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="et_pb_column et_pb_column_1_4 et_pb_column_4  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_blurb et_pb_blurb_1 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
                                                <div class="et_pb_blurb_content">
                                                    {{-- <div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_off et-pb-icon">&#xe0f7;</span></span>
                                                    </div> --}}
                                                    <div class="et_pb_blurb_container">
                                                        <h4 class="et_pb_module_header"><span style="color: blue;font-weight: 800;">2. Phân tích</span></h4>
                                                        <div class="et_pb_blurb_description">
                                                            <p>Các chuyên viên bảo hiểm sẽ phân tích để xác định gói bảo hiểm phù hợp</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="et_pb_column et_pb_column_1_4 et_pb_column_5  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_blurb et_pb_blurb_2 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
                                                <div class="et_pb_blurb_content">
                                                   {{--  <div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_off et-pb-icon">&#xe090;</span></span>
                                                    </div> --}}
                                                    <div class="et_pb_blurb_container">
                                                        <h4 class="et_pb_module_header"><span style="color: blue;font-weight: 800;">3. Liên hệ</span></h4>
                                                        <div class="et_pb_blurb_description">
                                                            <p>Các chuyên viên liên hệ với bạn để tư vấn các gói bảo hiểm phù hợp nhất</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="et_pb_column et_pb_column_1_4 et_pb_column_6  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_blurb et_pb_blurb_3 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
                                                <div class="et_pb_blurb_content">
                                                  {{--   <div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_off et-pb-icon">&#x6e;</span></span>
                                                    </div> --}}
                                                    <div class="et_pb_blurb_container">
                                                        <h4 class="et_pb_module_header"><span style="color: blue;font-weight: 800;">4. Hoàn tất</span></h4>
                                                        <div class="et_pb_blurb_description">
                                                            <p>Chuyên viên tư vấn sẽ hoàn thành toàn bộ thủ tục khi bạn đồng ý tham gia.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="et_pb_bottom_inside_divider et-no-transition"></div>
                                </div>
                                <div class="et_pb_section et_pb_section_2 et_pb_section_parallax et_pb_with_background et_section_regular">
                                    <div class="et_parallax_bg_wrap">
                                        <div class="et_parallax_bg et_pb_parallax_css" style="background-image: url({{asset('frontend/wp-content/uploads/2015/05/mua-bao-hiem-cho-con-loai-nao-tot.jpg);')}}"></div>
                                    </div>
                                    <div class="et_pb_row et_pb_row_3">
                                        <div class="et_pb_column et_pb_column_2_3 et_pb_column_7  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_text et_pb_text_5 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                    <h1>Tạo nền tảng phát triển cho bé</h1>
                                                    <p>Khi con bạn trưởng thành (thường ở độ tuổi từ 18-22) sẽ kéo theo các vấn đề tài chính phát sinh như chi phí học Đại học, kết hôn, mua nhà, khởi nghiệp&#8230;Tuy nhiên, khi con bạn trưởng thành cũng là lúc bạn sắp về hưu, khả năng làm việc và kiếm tiền không còn được như thời trẻ. Do đó, chuẩn bị tài chính ngay từ bây giờ là một quyết định đúng đắn.</p>
                                                    <p>Một câu hỏi rất thường gặp là: mua Bảo hiểm nhân thọ cho con, loại nào tốt? Nên tham gia sản phẩm nào? Thời hạn bao lâu? Phí đóng bao nhiêu?&#8230;Bạn hãy nhập thông tin vào mẫu để được tư vấn các sản phẩm tốt nhất nhé.</p>
                                                    <p>Trên thực tế, việc chọn mua bảo hiểm cho bé (đặc biệt là bảo hiểm cho trẻ sơ sinh) khác biệt khá nhiều so với việc chọn mua bảo hiểm cho người lớn hoặc bảo hiểm hưu trí. Đó là ngoài nhu cầu được bảo vệ về mặt tài chính khi rủi ro xảy ra, các bé còn có nhu cầu học tập và phát triển. Đây là giai đoạn các bé hình thành khả năng và tố chất của bản thân, là giai đoạn quan trọng nhất của đời người. Do đó, khi chọn mua bảo hiểm nhân thọ cho trẻ em, cần chọn những sản phẩm mang lại đúng nhu cầu mong muốn. Đây sẽ là một nền móng vững chắc để tạo tiền đề cho các bé phát triển bền vững trong tương lai.</p>
                                                </div>
                                            </div>
                                            <div class="et_pb_button_module_wrapper et_pb_button_1_wrapper et_pb_button_alignment_right et_pb_module "> <a class="btn btn-primary" href="{{route('tu-van-mien-phi')}}" rel="nofollow">Đăng ký ngay</a></div>
                                        </div>
                                        <div class="et_pb_column et_pb_column_1_3 et_pb_column_8  et_pb_css_mix_blend_mode_passthrough et-last-child et_pb_column_empty"></div>
                                    </div>
                                </div>
                                <div class="et_pb_section et_pb_section_3 et_pb_with_background et_section_regular">
                                    <div class="et_pb_row et_pb_row_4">
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_9  et_pb_css_mix_blend_mode_passthrough et_pb_column_empty"></div>
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_10  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_dark  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                    <h1 style="text-align: right;">Liên kết với giáo dục</h1>
                                                    <p style="text-align: right;">Tham gia Bảo hiểm nhân thọ có liên kết với giáo dục đang là xu hướng hiện nay khi chọn Bảo hiểm nhân thọ cho các bé. Song song với việc bảo vệ khả năng tài chính, các gói Bảo hiểm nhân thọ hiện đại có liên kết với các hệ thống trường Đại học lớn trong nước và quốc tế, phù hợp với nhu cầu du học tại chỗ hoặc du học nước ngoài đang rất phổ biến hiện nay. Chính ưu thế này sẽ tạo cho con em của bạn một cơ sơ vững chắc, một môi trường học tập chuyên nghiệp để các bé phát triển bền vững và trở thành những công dân có ích trong tương lai.</p>
                                                </div>
                                            </div>
                                            <div class="et_pb_button_module_wrapper et_pb_button_2_wrapper et_pb_button_alignment_right et_pb_module "> <a class="btn btn-primary" href="{{route('tu-van-mien-phi')}}" rel="nofollow">Đăng ký ngay</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="et_pb_section et_pb_section_4 et_section_regular">
                                    <div class="et_pb_row et_pb_row_5">
                                        <div class="et_pb_column et_pb_column_4_4 et_pb_column_11  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_text et_pb_text_7 et_pb_bg_layout_light  et_pb_text_align_center">
                                                <div class="et_pb_text_inner">
                                                    <p>Các khách hàng của chúng tôi nói gì?</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="et_pb_row et_pb_row_6">
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_12  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_with_border et_pb_module et_pb_testimonial et_pb_testimonial_0 clearfix et_pb_bg_layout_light  et_pb_text_align_left et_pb_testimonial_no_image">
                                                <div style="background-image:url({{asset('frontend/wp-content/uploads/2015/05/bao-hiem-cho-be.jpg')}})" class="et_pb_testimonial_portrait"></div>
                                                <div class="et_pb_testimonial_description">
                                                    <div class="et_pb_testimonial_description_inner">
                                                        <p>Tôi giờ đã lớn tuổi, không biết cách thức mua bảo hiểm nhân thọ như thế nào, may mà có người quen giới thiệu dịch vụ tư vấn. Thời của tôi không có các loại hình bảo hiểm như thế này, bây giờ xã hội hiện đại cái gì cũng có. Tôi chỉ có một đứa cháu đích tôn duy nhất, nên quyết định tham gia bảo hiểm cho nó, mong muốn nó có một cuộc sống đầy đủ, ấm no.</p> <span class="et_pb_testimonial_author">Chị Như Lan - bà nội bé Thanh Tâm</span>
                                                        <p class="et_pb_testimonial_meta"><span class="et_pb_testimonial_company">Quận 3, TP.HCM</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_13  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_with_border et_pb_module et_pb_testimonial et_pb_testimonial_1 clearfix et_pb_bg_layout_light  et_pb_text_align_left et_pb_testimonial_no_image">
                                                <div style="background-image:url({{asset('frontend/wp-content/uploads/2015/05/bao-hiem-tre-em.jpg')}})" class="et_pb_testimonial_portrait"></div>
                                                <div class="et_pb_testimonial_description">
                                                    <div class="et_pb_testimonial_description_inner">
                                                        <p>Gia đình tôi chỉ có duy nhất bé Xuân Nhi nên tôi rất muốn mua bảo hiểm nhân thọ cho bé, 2 vợ chồng tôi dự định khi bé đủ 18 tuổi sẽ dùng tiền đáo hạn hợp đồng để cho bé học Đại học. Hiện tại 2 vợ chồng còn trẻ nên lo trước cho tương lai, sau này về già không lo được thì có Bảo hiểm nhân thọ lo tiền cho con mình, vừa phòng ngừa rủi ro trong cuộc sống.</p> <span class="et_pb_testimonial_author">Chị Xuân Kiều - mẹ bé Xuân Nhi</span>
                                                        <p class="et_pb_testimonial_meta"><span class="et_pb_testimonial_company">Dĩ An, Bình Dương</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="et_pb_section et_pb_section_5 et_pb_with_background et_section_regular">
                                    <div class="et_pb_row et_pb_row_7 et_pb_gutters1">
                                        <div class="et_pb_column et_pb_column_1_4 et_pb_column_14  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_number_counter et_pb_number_counter_0 et_pb_bg_layout_light  et_pb_text_align_center et_pb_with_title" data-number-value="950+" data-number-separator="">
                                                <div class="percent">
                                                    <p><span class="percent-value"></span><span class="percent-sign"></span></p>
                                                </div>
                                                <h3 class="title">khách hàng</h3></div>
                                        </div>
                                        <div class="et_pb_column et_pb_column_1_4 et_pb_column_15  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_number_counter et_pb_number_counter_1 et_pb_bg_layout_light  et_pb_text_align_center et_pb_with_title" data-number-value="800+" data-number-separator="">
                                                <div class="percent">
                                                    <p><span class="percent-value"></span><span class="percent-sign"></span></p>
                                                </div>
                                                <h3 class="title">hợp đồng</h3></div>
                                        </div>
                                        <div class="et_pb_column et_pb_column_1_4 et_pb_column_16  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_number_counter et_pb_number_counter_2 et_pb_bg_layout_light  et_pb_text_align_center et_pb_with_title" data-number-value="20+" data-number-separator="">
                                                <div class="percent">
                                                    <p><span class="percent-value"></span><span class="percent-sign"></span></p>
                                                </div>
                                                <h3 class="title">tỷ VNĐ</h3></div>
                                        </div>
                                        <div class="et_pb_column et_pb_column_1_4 et_pb_column_17  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_number_counter et_pb_number_counter_3 et_pb_bg_layout_light  et_pb_text_align_center et_pb_with_title" data-number-value="98" data-number-separator="">
                                                <div class="percent">
                                                    <p><span class="percent-value"></span><span class="percent-sign">%</span></p>
                                                </div>
                                                <h3 class="title">hài lòng</h3></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="et_pb_section et_pb_section_6 et_pb_section_parallax et_pb_with_background et_section_regular">
                                    <div class="et_parallax_bg_wrap">
                                        <div class="et_parallax_bg" style="background-image: url({{asset('frontend/wp-content/uploads/2019/09/transportation-services-01.png')}});"></div>
                                    </div>
                                    <div class="et_pb_with_border et_pb_row et_pb_row_8 et_pb_row_fullwidth et_pb_equal_columns et_pb_gutters1">
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_18  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_text et_pb_text_8 et_pb_bg_layout_dark  et_pb_text_align_center">
                                                <div class="et_pb_text_inner">
                                                    <p>Câu hỏi thường gặp</p>
                                                </div>
                                            </div>
                                            <div class="et_pb_module et_pb_accordion et_pb_accordion_0">
                                                <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_0  et_pb_toggle_open">
                                                    <h5 class="et_pb_toggle_title">Tư vấn có tính phí không?</h5>
                                                    <div class="et_pb_toggle_content clearfix">
                                                        <p>Hoàn toàn miễn phí. Bạn chỉ phải đóng phí cho hợp đồng của bạn nếu tham gia BHNT.</p>
                                                    </div>
                                                </div>
                                                <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_1  et_pb_toggle_close">
                                                    <h5 class="et_pb_toggle_title">Sau bao lâu thì nhân viên liên hệ?</h5>
                                                    <div class="et_pb_toggle_content clearfix">
                                                        <p>Từ 1-3 ngày tùy số lượng khách hàng đăng ký tư vấn. Thông thường là 1 ngày.</p>
                                                    </div>
                                                </div>
                                                <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_2  et_pb_toggle_close">
                                                    <h5 class="et_pb_toggle_title">Nếu tham gia thì cần chuẩn bị giấy tờ gì?</h5>
                                                    <div class="et_pb_toggle_content clearfix">
                                                        <p>Chứng minh nhân dân hoặc hộ chiếu. Nếu người tham gia chưa đủ 18 tuổi thì cần giấy khai sinh. Bản gốc hoặc photo đều được, không cần công chứng.</p>
                                                    </div>
                                                </div>
                                                <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_3  et_pb_toggle_close">
                                                    <h5 class="et_pb_toggle_title">Ký hợp đồng có lâu không?</h5>
                                                    <div class="et_pb_toggle_content clearfix">
                                                        <p>Quá trình ký hợp đồng chỉ khoảng 10 đến 15 phút.</p>
                                                    </div>
                                                </div>
                                                <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_4  et_pb_toggle_close">
                                                    <h5 class="et_pb_toggle_title">Ký hợp đồng tại nhà được không?</h5>
                                                    <div class="et_pb_toggle_content clearfix">
                                                        <p>Bạn có thể lựa chọn địa điểm để ký hợp đồng ở bất kỳ đâu (tại công ty, tại nhà&#8230;).</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_19  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_text et_pb_text_9 et_pb_bg_layout_light  et_pb_text_align_center">
                                                <div class="et_pb_text_inner">
                                                    <p>Đăng ký tham gia</p>
                                                </div>
                                            </div>
                                            <div class="et_pb_module et_pb_text et_pb_text_10 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                    <div class="frm_forms  with_frm_style frm_style_formidable-style" id="frm_form_12_container">
                                                        <form enctype="multipart/form-data" action="{{route('tu-van-mien-phi')}}" method="post" class="frm-show-form " id="form_mua6uc223">
                                                            @csrf
                                                            <div class="frm_form_fields ">
                                                                <fieldset>
                                                                    <legend class="frm_hidden">Form homepage bottom</legend>
                                                                    <div class="frm_fields_container">
                                                                        <input type="hidden" name="frm_action" value="create" />
                                                                        <input type="hidden" name="form_id" value="12" />
                                                                        <input type="hidden" name="frm_hide_fields_12" id="frm_hide_fields_12" value="" />
                                                                        <input type="hidden" name="form_key" value="mua6uc223" />
                                                                        <input type="hidden" name="item_meta" value="" />
                                                                        <input type="hidden" id="frm_submit_entry_12" name="frm_submit_entry_12" value="c0f555c47e" />
                                                                        <input type="hidden" name="_wp_http_referer" value="/bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh/" />
                                                                        <label for="frm_verify_12" class="frm_screen_reader frm_hidden">If you are human, leave this field blank.</label>
                                                                        <input type="text" class="frm_hidden frm_verify" id="frm_verify_12" name="frm_verify" value="" />
                                                                        <div id="frm_field_78_container" class="frm_form_field form-field  frm_top_container horizontal_radio">
                                                                            <label class="frm_primary_label">Nơi bạn đang sinh sống <span class="frm_required"></span> </label>
                                                                            <div class="frm_opt_container">
                                                                                <div class="frm_radio" id="frm_radio_78-0">
                                                                                    <label for="field_ceby2a4-0">
                                                                                        <input type="radio" name="khuvuc" id="field_ceby2a4-0" value="1" data-invmsg="Nơi bạn đang sinh sống is invalid" /> TP. Hồ Chí Minh</label>
                                                                                </div>
                                                                                <div class="frm_radio" id="frm_radio_78-1">
                                                                                    <label for="field_ceby2a4-1">
                                                                                        <input type="radio" name="khuvuc" id="field_ceby2a4-1" value="2" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Bình Dương</label>
                                                                                </div>
                                                                                <div class="frm_radio" id="frm_radio_78-2">
                                                                                    <label for="field_ceby2a4-2">
                                                                                        <input type="radio" name="khuvuc" id="field_ceby2a4-2" value="3" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Đồng Nai</label>
                                                                                </div>
                                                                                <div class="frm_radio" id="frm_radio_78-3">
                                                                                    <label for="field_ceby2a4-3">
                                                                                        <input type="radio" name="khuvuc" id="field_ceby2a4-3" value="4" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Hà Nội</label>
                                                                                </div>
                                                                                <div class="frm_radio" id="frm_radio_78-4">
                                                                                    <label for="field_ceby2a4-4">
                                                                                        <input type="radio" name="khuvuc" id="field_ceby2a4-4" value="5" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Nơi khác</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="frm_field_79_container" class="frm_form_field form-field  frm_top_container horizontal_radio">
                                                                            <label class="frm_primary_label">Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) <span class="frm_required"></span> </label>
                                                                            <div class="frm_opt_container">
                                                                                <div class="frm_radio" id="frm_radio_79-0">
                                                                                    <label for="field_cbrfgd2-0">
                                                                                        <input type="radio" name="thunhap" id="field_cbrfgd2-0" value="1" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> 1 - 2 triệu VNĐ</label>
                                                                                </div>
                                                                                <div class="frm_radio" id="frm_radio_79-1">
                                                                                    <label for="field_cbrfgd2-1">
                                                                                        <input type="radio" name="thunhap" id="field_cbrfgd2-1" value="2" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> 2 - 4 triệu VNĐ</label>
                                                                                </div>
                                                                                <div class="frm_radio" id="frm_radio_79-2">
                                                                                    <label for="field_cbrfgd2-2">
                                                                                        <input type="radio" name="thunhap" id="field_cbrfgd2-2" value="3" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> 4 - 8 triệu VNĐ</label>
                                                                                </div>
                                                                                <div class="frm_radio" id="frm_radio_79-3">
                                                                                    <label for="field_cbrfgd2-3">
                                                                                        <input type="radio" name="thunhap" id="field_cbrfgd2-3" value="4" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> Trên 8 triệu VNĐ</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="frm_field_80_container" class="frm_form_field form-field  frm_required_field frm_left_container frm_full">
                                                                            <label for="field_kh7lgy34" class="frm_primary_label">Họ và tên <span class="frm_required">*</span> </label>
                                                                            <input type="text" id="field_kh7lgy34" name="fullname" value="" style="width:400px" placeholder="Họ và tên của người tham gia BHNT" data-reqmsg="Không thể để trống thông tin này." aria-required="true" data-invmsg="Họ và tên is invalid" class="auto_width" />
                                                                        </div>
                                                                        <div id="frm_field_81_container" class="frm_form_field form-field  frm_required_field frm_left_container frm_full">
                                                                            <label for="field_oodfpr34" class="frm_primary_label">Số điện thoại <span class="frm_required">*</span> </label>
                                                                            <input type="text" id="field_oodfpr34" name="phone" value="" style="width:400px" placeholder="Số điện thoại để liên lạc" data-reqmsg="Không thể để trống thông tin này." aria-required="true" data-invmsg="Số điện thoại is invalid" class="auto_width" />
                                                                        </div>
                                                                        <div id="frm_field_83_container" class="frm_form_field form-field  frm_left_container frm_total">
                                                                            <label for="field_luq362" id="field_luq362_label" class="frm_primary_label">Năm sinh của người tham gia BHNT <span class="frm_required"></span> </label>
                                                                            <select name="namsinh" id="field_luq362" data-invmsg="Năm sinh của người tham gia BHNT is invalid" class="auto_width">
                                                                                @for($i =1933; $i<=2019; $i++)
                                                                                   <option value="{{$i}}" selected >Năm {{$i}}</option>
                                                                                @endfor
                                                                            </select>
                                                                        </div>
                                                                        <input type="hidden" name="item_key" value="" />
                                                                        <div class="frm_submit">
                                                                            <input type="submit" value="Gửi yêu cầu tư vấn" />
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
             @include('frontend.blocks.footer')
        </div>
    </div>
    <script type='text/javascript'>
        var kk_star_ratings = {
            "action": "kk-star-ratings",
            "endpoint": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "nonce": "ab5ffef0a5"
        };
    </script>
    <script type='text/javascript'>
        var DIVI = {
            "item_count": "%d Item",
            "items_count": "%d Items"
        };
        var et_shortcodes_strings = {
            "previous": "Previous",
            "next": "Next"
        };
        var et_pb_custom = {
            "ajaxurl": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/images",
            "builder_images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/includes\/builder\/images",
            "et_frontend_nonce": "fbc068d605",
            "subscription_failed": "Please, check the fields below to make sure you entered the correct information.",
            "et_ab_log_nonce": "43b4f535e7",
            "fill_message": "Please, fill in the following fields:",
            "contact_error_message": "Please, fix the following errors:",
            "invalid": "Invalid email",
            "captcha": "Captcha",
            "prev": "Prev",
            "previous": "Previous",
            "next": "Next",
            "wrong_captcha": "You entered the wrong number in captcha.",
            "ignore_waypoints": "no",
            "is_divi_theme_used": "1",
            "widget_search_selector": ".widget_search",
            "is_ab_testing_active": "",
            "page_id": "366",
            "unique_test_id": "",
            "ab_bounce_rate": "5",
            "is_cache_plugin_active": "no",
            "is_shortcode_tracking": "",
            "tinymce_uri": ""
        };
        var et_pb_box_shadow_elements = [];
    </script>
    <script type='text/javascript'>
        var frm_js = {
            "ajax_url": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "images_url": "https:\/\/netbaohiem.com\/wp-content\/plugins\/formidable\/images",
            "loading": "Loading\u2026",
            "remove": "Remove",
            "offset": "4",
            "nonce": "c68f150b16",
            "id": "ID",
            "no_results": "No results match",
            "file_spam": "That file looks like Spam.",
            "calc_error": "There is an error in the calculation in the field with key",
            "empty_fields": "Please complete the preceding required fields before uploading a file."
        };
    </script>
    <style id="et-core-unified-cached-inline-styles-2">
        div.et_pb_section.et_pb_section_0 {
            background-blend-mode: multiply;
            background-image: url({{asset('frontend/wp-content/uploads/2015/06/bao-hiem-cho-tre-so-sinh-1.jpg')}})!important
        }
        
        .et_pb_text_7 {
            font-family: 'Verdana', Helvetica, Arial, Lucida, sans-serif;
            font-size: 30px;
            line-height: 1.3em;
            padding-top: 0px!important;
            margin-bottom: 0px!important
        }
        
        .et_pb_number_counter_0.et_pb_number_counter h3,
        .et_pb_number_counter_0.et_pb_number_counter h1.title,
        .et_pb_number_counter_0.et_pb_number_counter h2.title,
        .et_pb_number_counter_0.et_pb_number_counter h4.title,
        .et_pb_number_counter_0.et_pb_number_counter h5.title,
        .et_pb_number_counter_0.et_pb_number_counter h6.title {
            font-family: 'Merriweather', Georgia, "Times New Roman", serif;
            font-size: 15px;
            text-align: center
        }
        
        .et_pb_section_5.et_pb_section {
            background-color: #fafafa!important
        }
        
        .et_pb_testimonial_1 .et_pb_testimonial_portrait {
            border-width: 2px
        }
        
        .et_pb_testimonial_1.et_pb_testimonial {
            background-color: rgba(0, 0, 0, 0);
            background-color: rgba(0, 0, 0, 0)
        }
        
        .et_pb_testimonial_0 .et_pb_testimonial_portrait {
            border-width: 2px;
            border-color: #000000
        }
        
        .et_pb_testimonial_0.et_pb_testimonial {
            background-color: rgba(0, 0, 0, 0);
            background-color: rgba(0, 0, 0, 0)
        }
        
        .et_pb_text_7 p {
            line-height: 1.3em
        }
        
        .et_pb_number_counter_1.et_pb_number_counter h3,
        .et_pb_number_counter_1.et_pb_number_counter h1.title,
        .et_pb_number_counter_1.et_pb_number_counter h2.title,
        .et_pb_number_counter_1.et_pb_number_counter h4.title,
        .et_pb_number_counter_1.et_pb_number_counter h5.title,
        .et_pb_number_counter_1.et_pb_number_counter h6.title {
            font-family: 'Merriweather', Georgia, "Times New Roman", serif;
            font-size: 15px;
            text-align: center
        }
        
        .et_pb_row_5.et_pb_row {
            margin-top: -14px!important;
            margin-bottom: -6px!important
        }
        
        .et_pb_button_2,
        .et_pb_button_2:after {
            transition: all 300ms ease 0ms
        }
        
        body #page-container .et_pb_section .et_pb_button_2:hover:after {
            color:
        }
        
        body #page-container .et_pb_section .et_pb_button_2 {
            color: #ffffff!important
        }
        
        .et_pb_section_0.et_pb_section {
            background-color: rgba(0, 0, 0, 0.75)!important
        }
        
        .et_pb_text_6 h2 {
            font-family: 'Verdana', Helvetica, Arial, Lucida, sans-serif;
            font-weight: 300
        }
        
        .et_pb_number_counter_0.et_pb_number_counter .percent p {
            color: #0044cc!important;
            text-align: center
        }
        
        .et_pb_number_counter_1.et_pb_number_counter .percent p {
            color: #0044cc!important;
            text-align: center
        }
        
        div.et_pb_section.et_pb_section_3 {
            background-image: url(https://netbaohiem.com/wp-content/uploads/2015/05/bao-hiem-nhan-tho-cho-be.jpg)!important
        }
        
        .et_pb_accordion_0.et_pb_accordion h5.et_pb_toggle_title,
        .et_pb_accordion_0.et_pb_accordion h1.et_pb_toggle_title,
        .et_pb_accordion_0.et_pb_accordion h2.et_pb_toggle_title,
        .et_pb_accordion_0.et_pb_accordion h3.et_pb_toggle_title,
        .et_pb_accordion_0.et_pb_accordion h4.et_pb_toggle_title,
        .et_pb_accordion_0.et_pb_accordion h6.et_pb_toggle_title {
            color: #ffffff!important
        }
        
        .et_pb_text_9 {
            font-family: 'Open Sans', Helvetica, Arial, Lucida, sans-serif;
            font-weight: 800;
            font-size: 30px;
            margin-top: -40px!important
        }
        
        .et_pb_accordion_0.et_pb_accordion .et_pb_toggle_open h5.et_pb_toggle_title,
        .et_pb_accordion_0.et_pb_accordion .et_pb_toggle_open h1.et_pb_toggle_title,
        .et_pb_accordion_0.et_pb_accordion .et_pb_toggle_open h2.et_pb_toggle_title,
        .et_pb_accordion_0.et_pb_accordion .et_pb_toggle_open h3.et_pb_toggle_title,
        .et_pb_accordion_0.et_pb_accordion .et_pb_toggle_open h4.et_pb_toggle_title,
        .et_pb_accordion_0.et_pb_accordion .et_pb_toggle_open h6.et_pb_toggle_title {
            color: #ffffff!important
        }
        
        .et_pb_accordion_0 .et_pb_toggle_close {
            background-color: rgba(0, 0, 0, 0)
        }
        
        .et_pb_accordion_0 .et_pb_toggle_open {
            background-color: rgba(0, 0, 0, 0)
        }
        
        .et_pb_accordion_0.et_pb_accordion .et_pb_accordion_item {
            border-width: 0px 0px 1px 0px
        }
        
        .et_pb_accordion_0.et_pb_accordion .et_pb_toggle_content {
            color: #ffffff!important
        }
        
        .et_pb_text_8 {
            font-family: 'Open Sans', Helvetica, Arial, Lucida, sans-serif;
            font-weight: 800;
            font-size: 30px;
            line-height: 1.3em;
            margin-top: -40px!important
        }
        
        .et_pb_number_counter_2.et_pb_number_counter h3,
        .et_pb_number_counter_2.et_pb_number_counter h1.title,
        .et_pb_number_counter_2.et_pb_number_counter h2.title,
        .et_pb_number_counter_2.et_pb_number_counter h4.title,
        .et_pb_number_counter_2.et_pb_number_counter h5.title,
        .et_pb_number_counter_2.et_pb_number_counter h6.title {
            font-family: 'Merriweather', Georgia, "Times New Roman", serif;
            font-size: 15px;
            text-align: center
        }
        
        .et_pb_text_8 p {
            line-height: 1.3em
        }
        
        .et_pb_row_8.et_pb_row {
            padding-top: 0px!important;
            padding-bottom: 0px!important;
            padding-top: 0px;
            padding-bottom: 0px
        }
        
        .et_pb_row_8 {
            background-color: #212121;
            border-width: 5px;
            border-color: #212121
        }
        
        .et_pb_section_6.et_pb_section {
            padding-top: 0px;
            padding-bottom: 0px
        }
        
        .et_pb_number_counter_3.et_pb_number_counter .percent p {
            color: #0044cc!important;
            text-align: center
        }
        
        .et_pb_number_counter_3.et_pb_number_counter h3,
        .et_pb_number_counter_3.et_pb_number_counter h1.title,
        .et_pb_number_counter_3.et_pb_number_counter h2.title,
        .et_pb_number_counter_3.et_pb_number_counter h4.title,
        .et_pb_number_counter_3.et_pb_number_counter h5.title,
        .et_pb_number_counter_3.et_pb_number_counter h6.title {
            font-family: 'Merriweather', Georgia, "Times New Roman", serif;
            font-size: 15px;
            text-align: center
        }
        
        .et_pb_number_counter_2.et_pb_number_counter .percent p {
            color: #0044cc!important;
            text-align: center
        }
        
        .et_pb_text_6 h1 {
            font-family: 'Verdana', Helvetica, Arial, Lucida, sans-serif;
            font-weight: 300;
            color: #ffffff!important;
            line-height: 1.3em;
            text-align: left
        }
        
        .et_pb_button_2_wrapper {
            margin-top: 10px!important;
            margin-bottom: 10px!important
        }
        
        .et_pb_text_10 {
            margin-top: 15px!important
        }
        
        .et_pb_text_4 p {
            line-height: 1.3em
        }
        
        .et_pb_row_1,
        body #page-container .et-db #et-boc .et_pb_row_1.et_pb_row,
        body.et_pb_pagebuilder_layout.single #page-container #et-boc .et_pb_row_1.et_pb_row,
        body.et_pb_pagebuilder_layout.single.et_full_width_page #page-container #et-boc .et_pb_row_1.et_pb_row {
            width: 90%
        }
        
        .et_pb_blurb_0.et_pb_blurb h4,
        .et_pb_blurb_0.et_pb_blurb h4 a,
        .et_pb_blurb_0.et_pb_blurb h1.et_pb_module_header,
        .et_pb_blurb_0.et_pb_blurb h1.et_pb_module_header a,
        .et_pb_blurb_0.et_pb_blurb h2.et_pb_module_header,
        .et_pb_blurb_0.et_pb_blurb h2.et_pb_module_header a,
        .et_pb_blurb_0.et_pb_blurb h3.et_pb_module_header,
        .et_pb_blurb_0.et_pb_blurb h3.et_pb_module_header a,
        .et_pb_blurb_0.et_pb_blurb h5.et_pb_module_header,
        .et_pb_blurb_0.et_pb_blurb h5.et_pb_module_header a,
        .et_pb_blurb_0.et_pb_blurb h6.et_pb_module_header,
        .et_pb_blurb_0.et_pb_blurb h6.et_pb_module_header a {
            color: #4a4a4a!important
        }
        
        .et_pb_row_1.et_pb_row {
            margin-top: -45px!important
        }
        
        .et_pb_blurb_0 .et-pb-icon {
            color: #0044cc
        }
        
        .et_pb_section_1.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider {
            background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmYWZhZmEiPjwvZz48L3N2Zz4=);
            background-size: 100% 100px;
            bottom: 0;
            height: 100px;
            z-index: 1
        }
        
        .et_pb_blurb_1.et_pb_blurb h4,
        .et_pb_blurb_1.et_pb_blurb h4 a,
        .et_pb_blurb_1.et_pb_blurb h1.et_pb_module_header,
        .et_pb_blurb_1.et_pb_blurb h1.et_pb_module_header a,
        .et_pb_blurb_1.et_pb_blurb h2.et_pb_module_header,
        .et_pb_blurb_1.et_pb_blurb h2.et_pb_module_header a,
        .et_pb_blurb_1.et_pb_blurb h3.et_pb_module_header,
        .et_pb_blurb_1.et_pb_blurb h3.et_pb_module_header a,
        .et_pb_blurb_1.et_pb_blurb h5.et_pb_module_header,
        .et_pb_blurb_1.et_pb_blurb h5.et_pb_module_header a,
        .et_pb_blurb_1.et_pb_blurb h6.et_pb_module_header,
        .et_pb_blurb_1.et_pb_blurb h6.et_pb_module_header a {
            color: #4a4a4a!important
        }
        
        .et_pb_blurb_1 .et-pb-icon {
            color: #0044cc
        }
        
        .et_pb_text_3 {
            background-color: rgba(255, 255, 255, 0.75)
        }
        
        .et_pb_blurb_2.et_pb_blurb h4,
        .et_pb_blurb_2.et_pb_blurb h4 a,
        .et_pb_blurb_2.et_pb_blurb h1.et_pb_module_header,
        .et_pb_blurb_2.et_pb_blurb h1.et_pb_module_header a,
        .et_pb_blurb_2.et_pb_blurb h2.et_pb_module_header,
        .et_pb_blurb_2.et_pb_blurb h2.et_pb_module_header a,
        .et_pb_blurb_2.et_pb_blurb h3.et_pb_module_header,
        .et_pb_blurb_2.et_pb_blurb h3.et_pb_module_header a,
        .et_pb_blurb_2.et_pb_blurb h5.et_pb_module_header,
        .et_pb_blurb_2.et_pb_blurb h5.et_pb_module_header a,
        .et_pb_blurb_2.et_pb_blurb h6.et_pb_module_header,
        .et_pb_blurb_2.et_pb_blurb h6.et_pb_module_header a {
            color: #4a4a4a!important
        }
        
        .et_pb_blurb_2 .et-pb-icon {
            color: #0044cc
        }
        
        .et_pb_button_0,
        .et_pb_button_0:after {
            transition: all 300ms ease 0ms
        }
        
        .et_pb_text_1 {
            font-weight: 600;
            font-size: 19px;
            line-height: 1.3em
        }
        
        .et_pb_blurb_3.et_pb_blurb h4,
        .et_pb_blurb_3.et_pb_blurb h4 a,
        .et_pb_blurb_3.et_pb_blurb h1.et_pb_module_header,
        .et_pb_blurb_3.et_pb_blurb h1.et_pb_module_header a,
        .et_pb_blurb_3.et_pb_blurb h2.et_pb_module_header,
        .et_pb_blurb_3.et_pb_blurb h2.et_pb_module_header a,
        .et_pb_blurb_3.et_pb_blurb h3.et_pb_module_header,
        .et_pb_blurb_3.et_pb_blurb h3.et_pb_module_header a,
        .et_pb_blurb_3.et_pb_blurb h5.et_pb_module_header,
        .et_pb_blurb_3.et_pb_blurb h5.et_pb_module_header a,
        .et_pb_blurb_3.et_pb_blurb h6.et_pb_module_header,
        .et_pb_blurb_3.et_pb_blurb h6.et_pb_module_header a {
            color: #4a4a4a!important
        }
        
        .et_pb_text_1 p {
            line-height: 1.3em
        }
        
        .et_pb_button_1,
        .et_pb_button_1:after {
            transition: all 300ms ease 0ms
        }
        
        body #page-container .et_pb_section .et_pb_button_1:hover:after {
            color:
        }
        
        body #page-container .et_pb_section .et_pb_button_1 {
            color: #000000!important
        }
        
        .et_pb_button_1_wrapper {
            margin-top: 10px!important;
            margin-bottom: 10px!important
        }
        
        .et_pb_text_5 h2 {
            font-family: 'Verdana', Helvetica, Arial, Lucida, sans-serif;
            font-weight: 300
        }
        
        .et_pb_text_5 h1 {
            font-family: 'Verdana', Helvetica, Arial, Lucida, sans-serif;
            font-weight: 300;
            line-height: 1.3em;
            text-align: left
        }
        
        .et_pb_text_0 p {
            line-height: 1.3em
        }
        
        .et_pb_text_0 {
            font-family: 'Open Sans', Helvetica, Arial, Lucida, sans-serif;
            font-weight: 600;
            text-transform: uppercase;
            font-size: 30px;
            line-height: 1.3em
        }
        
        .et_pb_blurb_3 .et-pb-icon {
            color: #0044cc
        }
        
        .et_pb_text_4 {
            font-family: 'Verdana', Helvetica, Arial, Lucida, sans-serif;
            font-size: 30px;
            line-height: 1.3em
        }
        
        .et_pb_column_19 {
            background-color: #ffffff;
            padding-top: 80px;
            padding-right: 6%;
            padding-bottom: 80px;
            padding-left: 6%;
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_1 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_18 {
            background-color: #212121;
            padding-top: 80px;
            padding-right: 6%;
            padding-bottom: 80px;
            padding-left: 6%;
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_0 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_2 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_8 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_17 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_3 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_16 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_4 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_9 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_14 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_5 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_13 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_6 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_12 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_11 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_7 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_10 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_column_15 {
            z-index: 9;
            position: relative
        }
        
        .et_pb_row_3.et_pb_row {
            margin-top: 0px!important;
            margin-left: auto!important;
            margin-right: auto!important
        }
        
        @media only screen and (min-width:981px) {
            .et_pb_row_8,
            body #page-container .et-db #et-boc .et_pb_row_8.et_pb_row,
            body.et_pb_pagebuilder_layout.single #page-container #et-boc .et_pb_row_8.et_pb_row,
            body.et_pb_pagebuilder_layout.single.et_full_width_page #page-container #et-boc .et_pb_row_8.et_pb_row {
                width: 100%;
                max-width: 100%
            }
        }
        
        @media only screen and (max-width:980px) {
            body #page-container .et_pb_section .et_pb_button_1 {
                padding-left: 1em;
                padding-right: 1em
            }
            body #page-container .et_pb_section .et_pb_button_1:hover {
                padding-left: 0.7em;
                padding-right: 2em
            }
            body #page-container .et_pb_section .et_pb_button_1:after {
                display: inline-block;
                opacity: 0
            }
            body #page-container .et_pb_section .et_pb_button_1:hover:after {
                opacity: 1
            }
            body #page-container .et_pb_section .et_pb_button_2 {
                padding-left: 1em;
                padding-right: 1em
            }
            body #page-container .et_pb_section .et_pb_button_2:hover {
                padding-left: 0.7em;
                padding-right: 2em
            }
            body #page-container .et_pb_section .et_pb_button_2:after {
                display: inline-block;
                opacity: 0
            }
            body #page-container .et_pb_section .et_pb_button_2:hover:after {
                opacity: 1
            }
            .et_pb_row_8,
            body #page-container .et-db #et-boc .et_pb_row_8.et_pb_row,
            body.et_pb_pagebuilder_layout.single #page-container #et-boc .et_pb_row_8.et_pb_row,
            body.et_pb_pagebuilder_layout.single.et_full_width_page #page-container #et-boc .et_pb_row_8.et_pb_row {
                width: 100%;
                max-width: 100%
            }
            .et_pb_accordion_0.et_pb_accordion .et_pb_accordion_item {
                border-bottom-width: 1px
            }
            .et_pb_column_18 {
                padding-right: 10%;
                padding-left: 10%
            }
            .et_pb_column_19 {
                padding-right: 10%;
                padding-left: 10%
            }
        }
        
        @media only screen and (min-width:768px) and (max-width:980px) {
            .et_pb_text_2 {
                display: none!important
            }
            .et_pb_button_0 {
                display: none!important
            }
        }
        
        @media only screen and (max-width:767px) {
            .et_pb_text_2 {
                display: none!important
            }
            .et_pb_button_0 {
                display: none!important
            }
            body #page-container .et_pb_section .et_pb_button_1 {
                padding-left: 1em;
                padding-right: 1em
            }
            body #page-container .et_pb_section .et_pb_button_1:hover {
                padding-left: 0.7em;
                padding-right: 2em
            }
            body #page-container .et_pb_section .et_pb_button_1:after {
                display: inline-block;
                opacity: 0
            }
            body #page-container .et_pb_section .et_pb_button_1:hover:after {
                opacity: 1
            }
            body #page-container .et_pb_section .et_pb_button_2 {
                padding-left: 1em;
                padding-right: 1em
            }
            body #page-container .et_pb_section .et_pb_button_2:hover {
                padding-left: 0.7em;
                padding-right: 2em
            }
            body #page-container .et_pb_section .et_pb_button_2:after {
                display: inline-block;
                opacity: 0
            }
            body #page-container .et_pb_section .et_pb_button_2:hover:after {
                opacity: 1
            }
            .et_pb_accordion_0.et_pb_accordion .et_pb_accordion_item {
                border-bottom-width: 1px
            }
        }
    </style>
    <script type="text/javascript" defer src="{{asset('frontend/wp-content/cache/autoptimize/js/autoptimize_48ad72b6e79a198cbc6afba3170d7159.js')}}"></script>
</body>

</html>
