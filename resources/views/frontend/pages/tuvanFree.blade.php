@extends('frontend.welcome')
@section('content')

    <div id="page-container">
       
        <div id="et-main-area">
            <div id="main-content">
                <div class="container">
                    <div id="content-area" class="clearfix">
                        <div id="left-area">
                            <article id="post-71" class="post-71 page type-page status-publish hentry">
                                <h1 class="entry-title main_title">Tư vấn miễn phí</h1>
                                <div class="entry-content">
                                    <p><em>Bạn vui lòng điền đầy đủ thông tin của <strong>người có nguyện vọng tham gia Bảo hiểm nhân thọ.</strong></em></p>
                                    <p style="text-align: left;">
                                        <div class="frm_forms  with_frm_style frm_style_formidable-style" id="frm_form_2_container">
                                            <form enctype="multipart/form-data" action="{{route('tu-van-mien-phi')}}" method="post" class="frm-show-form " id="form_mua6uc">
                                                @csrf
                                                @include('admin.blocks.alert')
                                                <div class="frm_form_fields ">
                                                    <fieldset>
                                                        <legend class="frm_hidden">Form Tư Vấn Miễn Phí</legend>
                                                        <div class="frm_fields_container">
                                                            <input type="hidden" name="frm_action" value="create" />
                                                            <input type="hidden" name="form_id" value="2" />
                                                            <input type="hidden" name="frm_hide_fields_2" id="frm_hide_fields_2" value="" />
                                                            <input type="hidden" name="form_key" value="mua6uc" />
                                                            <input type="hidden" name="item_meta" value="" />
                                                            <input type="hidden" id="frm_submit_entry_2" name="frm_submit_entry_2" value="c0f555c47e" />
                                                            <input type="hidden" name="_wp_http_referer" value="/tu-van-mien-phi/" />
                                                            <label for="frm_verify_2" class="frm_screen_reader frm_hidden">If you are human, leave this field blank.</label>
                                                            <input type="text" class="frm_hidden frm_verify" id="frm_verify_2" name="frm_verify" value="" />
                                                            <div id="frm_field_45_container" class="frm_form_field form-field  frm_top_container horizontal_radio">
                                                                <label class="frm_primary_label">Nơi bạn đang sinh sống <span class="frm_required"></span> </label>
                                                                <div class="frm_opt_container">
                                                                    <div class="frm_radio" id="frm_radio_45-0">
                                                                        <label for="field_xg77lb-0">
                                                                            <input type="radio" name="khuvuc" id="field_xg77lb-0" value="1" data-invmsg="Nơi bạn đang sinh sống is invalid" /> TP. Hồ Chí Minh</label>
                                                                    </div>
                                                                    <div class="frm_radio" id="frm_radio_45-1">
                                                                        <label for="field_xg77lb-1">
                                                                            <input type="radio" name="khuvuc" id="field_xg77lb-1" value="2" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Bình Dương</label>
                                                                    </div>
                                                                    <div class="frm_radio" id="frm_radio_45-2">
                                                                        <label for="field_xg77lb-2">
                                                                            <input type="radio" name="khuvuc" id="field_xg77lb-2" value="3" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Đồng Nai</label>
                                                                    </div>
                                                                    <div class="frm_radio" id="frm_radio_45-3">
                                                                        <label for="field_xg77lb-3">
                                                                            <input type="radio" name="khuvuc" id="field_xg77lb-3" value="4" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Hà Nội</label>
                                                                    </div>
                                                                    <div class="frm_radio" id="frm_radio_45-4">
                                                                        <label for="field_xg77lb-4">
                                                                            <input type="radio" name="khuvuc" id="field_xg77lb-4" value="5" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Nơi khác</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="frm_field_46_container" class="frm_form_field form-field  frm_top_container horizontal_radio">
                                                                <label class="frm_primary_label">Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) <span class="frm_required"></span> </label>
                                                                <div class="frm_opt_container">
                                                                    <div class="frm_radio" id="frm_radio_46-0">
                                                                        <label for="field_5yckce-0">
                                                                            <input type="radio" name="thunhap" id="field_5yckce-0" value="1" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> 1 - 2 triệu VNĐ</label>
                                                                    </div>
                                                                    <div class="frm_radio" id="frm_radio_46-1">
                                                                        <label for="field_5yckce-1">
                                                                            <input type="radio" name="thunhap" id="field_5yckce-1" value="2" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> 2 - 4 triệu VNĐ</label>
                                                                    </div>
                                                                    <div class="frm_radio" id="frm_radio_46-2">
                                                                        <label for="field_5yckce-2">
                                                                            <input type="radio" name="thunhap" id="field_5yckce-2" value="3" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> 4 - 8 triệu VNĐ</label>
                                                                    </div>
                                                                    <div class="frm_radio" id="frm_radio_46-3">
                                                                        <label for="field_5yckce-3">
                                                                            <input type="radio" name="thunhap" id="field_5yckce-3" value="4" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> Trên 8 triệu VNĐ</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="frm_field_21_container" class="frm_form_field form-field  frm_top_container frm_fifth">
                                                                <label for="field_po9xcm" class="frm_primary_label">Ngày sinh <span class="frm_required"></span> </label>
                                                                <select name="ngaysinh" id="field_po9xcm" data-frmval="1" data-invmsg="Ngày sinh is invalid">
                                                                    @for($i =1; $i<=31; $i++)
                                                                       <option value="{{$i}}" >{{$i}}</option>
                                                                    @endfor
                                                                   
                                                                </select>
                                                            </div>
                                                            <div id="frm_field_22_container" class="frm_form_field form-field  frm_top_container frm_fifth">
                                                                <label for="field_oefcqu" class="frm_primary_label">Tháng sinh <span class="frm_required"></span> </label>
                                                                <select name="thangsinh" id="field_oefcqu" data-frmval="Tháng 1" data-invmsg="Tháng sinh is invalid">
                                                                    @for($i =1; $i<=12; $i++)
                                                                       <option value="{{$i}}" >Tháng {{$i}}</option>
                                                                    @endfor 
                                                                   
                                                                </select>
                                                            </div>
                                                            <div id="frm_field_23_container" class="frm_form_field form-field  frm_top_container frm_fifth">
                                                                <label for="field_hh6syp" class="frm_primary_label">Năm sinh <span class="frm_required"></span> </label>
                                                                <select name="namsinh" id="field_hh6syp" data-frmval="2019" data-invmsg="Năm sinh is invalid">
                                                                    @for($i =1933; $i<=2019; $i++)
                                                                       <option value="{{$i}}" selected >Năm {{$i}}</option>
                                                                    @endfor
                                                                   
                                                                </select>
                                                            </div>
                                                            <div id="frm_field_17_container" class="frm_form_field form-field  frm_required_field frm_left_container frm_inline">
                                                                <label for="field_kh7lgy" class="frm_primary_label">Họ và tên <span class="frm_required">*</span> </label>
                                                                <input type="text" id="field_kh7lgy" name="fullname" value="" style="width:400px" placeholder="Họ và tên của người tham gia BHNT" data-reqmsg="Không thể để trống thông tin này." aria-required="true" data-invmsg="Họ và tên is invalid" class="auto_width" />
                                                            </div>
                                                            <div id="frm_field_18_container" class="frm_form_field form-field  frm_required_field frm_left_container frm_inline">
                                                                <label for="field_oodfpr" class="frm_primary_label">Số điện thoại <span class="frm_required">*</span> </label>
                                                                <input type="text" id="field_oodfpr" name="phone" value="" style="width:400px" placeholder="Số điện thoại để liên lạc" data-reqmsg="Không thể để trống thông tin này." aria-required="true" data-invmsg="Số điện thoại is invalid" class="auto_width" />
                                                            </div>
                                                           
                                                           {{--  <input type="hidden" id="field_i4dxa" name="item_meta[84]" value="" data-invmsg="Hidden Field is invalid" /> --}}
                                                            <input type="hidden" name="item_key" value="" />
                                                            <div class="frm_submit">
                                                                <input type="submit" value="Gửi yêu cầu tư vấn" />  
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </form>
                                        </div>
                                    </p>
                                    <p><span style="color: #ff0000;"><span style="color: #036380;"><strong>Chúng tôi cam kết giữ bí mật tuyệt đối thông tin của khách hàng.</strong></span> </span>
                                    </p>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div> <span class="et_pb_scroll_top et-pb-icon"></span>
            
        </div>
    </div>
    <script type='text/javascript'>
        var kk_star_ratings = {
            "action": "kk-star-ratings",
            "endpoint": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "nonce": "ab5ffef0a5"
        };
    </script>
    <script type='text/javascript'>
        var DIVI = {
            "item_count": "%d Item",
            "items_count": "%d Items"
        };
        var et_shortcodes_strings = {
            "previous": "Previous",
            "next": "Next"
        };
        var et_pb_custom = {
            "ajaxurl": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/images",
            "builder_images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/includes\/builder\/images",
            "et_frontend_nonce": "fbc068d605",
            "subscription_failed": "Please, check the fields below to make sure you entered the correct information.",
            "et_ab_log_nonce": "43b4f535e7",
            "fill_message": "Please, fill in the following fields:",
            "contact_error_message": "Please, fix the following errors:",
            "invalid": "Invalid email",
            "captcha": "Captcha",
            "prev": "Prev",
            "previous": "Previous",
            "next": "Next",
            "wrong_captcha": "You entered the wrong number in captcha.",
            "ignore_waypoints": "no",
            "is_divi_theme_used": "1",
            "widget_search_selector": ".widget_search",
            "is_ab_testing_active": "",
            "page_id": "71",
            "unique_test_id": "",
            "ab_bounce_rate": "5",
            "is_cache_plugin_active": "no",
            "is_shortcode_tracking": "",
            "tinymce_uri": ""
        };
        var et_pb_box_shadow_elements = [];
    </script>
    <script type='text/javascript'>
        var frm_js = {
            "ajax_url": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "images_url": "https:\/\/netbaohiem.com\/wp-content\/plugins\/formidable\/images",
            "loading": "Loading\u2026",
            "remove": "Remove",
            "offset": "4",
            "nonce": "c68f150b16",
            "id": "ID",
            "no_results": "No results match",
            "file_spam": "That file looks like Spam.",
            "calc_error": "There is an error in the calculation in the field with key",
            "empty_fields": "Please complete the preceding required fields before uploading a file."
        };
    </script>

    <script type="text/javascript" defer src="{{asset('frontend/wp-content/cache/autoptimize/js/autoptimize_08ecde37adebf8ded47f2479a4bca2b3.js')}}"></script>
@endsection
