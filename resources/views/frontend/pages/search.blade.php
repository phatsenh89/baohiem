<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="pingback" href="../xmlrpc.php" />
    <script>
        var et_site_url = '../index.html';
        var et_post_id = '73';

        function et_core_page_resource_fallback(a, b) {
            "undefined" === typeof b && (b = a.sheet.cssRules && 0 === a.sheet.cssRules.length);
            b && (a.onerror = null, a.onload = null, a.href ? a.href = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id : a.src && (a.src = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id))
        }
    </script>
    <link type="text/css" media="all" href="{{asset('frontend/wp-content/cache/autoptimize/css/autoptimize_998867243f785db33b6a5a20f9554970.css')}}" rel="stylesheet" />
    <title>@yield('title','Bài viết - Bảo Hiểm')</title>
    <meta name="robots" content="noindex,follow" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Bài viết - Net Bảo Hiểm" />
    <meta property="og:url" content="https://netbaohiem.com/bai-viet/" />
    <meta property="og:site_name" content="Net Bảo Hiểm" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="Bài viết - Net Bảo Hiểm" />
    <script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://netbaohiem.com/#website","url":"https://netbaohiem.com/","name":"Net B\u1ea3o Hi\u1ec3m","description":"T\u01b0 V\u1ea5n Mi\u1ec5n Ph\u00ed B\u1ea3o Hi\u1ec3m Nh\u00e2n Th\u1ecd","potentialAction":{"@type":"SearchAction","target":"https://netbaohiem.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://netbaohiem.com/bai-viet/#webpage","url":"https://netbaohiem.com/bai-viet/","inLanguage":"en-US","name":"B\u00e0i vi\u1ebft - Net B\u1ea3o Hi\u1ec3m","isPartOf":{"@id":"https://netbaohiem.com/#website"},"datePublished":"2015-04-24T15:48:03+00:00","dateModified":"2019-09-21T02:03:10+00:00","breadcrumb":{"@id":"https://netbaohiem.com/bai-viet/#breadcrumb"}},{"@type":"BreadcrumbList","@id":"https://netbaohiem.com/bai-viet/#breadcrumb","itemListElement":[{"@type":"ListItem","position":1,"item":{"@type":"WebPage","@id":"https://netbaohiem.com/","url":"https://netbaohiem.com/","name":"Home"}},{"@type":"ListItem","position":2,"item":{"@type":"WebPage","@id":"https://netbaohiem.com/bai-viet/","url":"https://netbaohiem.com/bai-viet/","name":"B\u00e0i vi\u1ebft"}}]}]}</script>
    <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
    <link rel='dns-prefetch' href='http://s.w.org/' />
    <link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Feed" href="../feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Comments Feed" href="../comments/feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Bài viết Comments Feed" href="feed/index.html" />
    <meta content="Divi v.3.29.3" name="generator" />
    <link rel='stylesheet' id='et-builder-googlefonts-cached-css' href='https://fonts.googleapis.com/css?family=Merriweather%3A300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic%7CLora%3Aregular%2Citalic%2C700%2C700italic&amp;ver=5.3#038;subset=cyrillic,latin,latin-ext,cyrillic-ext,vietnamese' type='text/css' media='all' />
    <script type='text/javascript' src='{{asset('frontend/wp-includes/js/jquery/jquery4a5f.js?ver=1.12.4-wp')}}'></script>
    <link rel='https://api.w.org/' href='../wp-json/index.html' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="../xmlrpc0db0.php?rsd" />
    <meta name="generator" content="WordPress 5.3" />
    <link rel='shortlink' href='../index8cae.html?p=73' />
    <link rel="alternate" type="application/json+oembed" href="../wp-json/oembed/1.0/embedb8da.json?url=https%3A%2F%2Fnetbaohiem.com%2Fbai-viet%2F" />
    <link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embedbfcd?url=https%3A%2F%2Fnetbaohiem.com%2Fbai-viet%2F&amp;format=xml" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="shortcut icon" href="{{asset('frontend/wp-content/uploads/2015/05/favicon-1-netbaohiem.png')}}" />
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-147696565-1', 'auto');
        ga('send', 'pageview');
    </script>
    <link rel="stylesheet" id="et-core-unified-cached-inline-styles" href="{{asset('frontend/wp-content/cache/et/73/et-core-unified-15752158660702.min.css')}}" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />
</head>

<body data-rsssl=1 class="page-template-default page page-id-73 et_pb_button_helper_class et_transparent_nav et_fixed_nav et_show_nav et_cover_background et_pb_gutter osx et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_pb_pagebuilder_layout et_full_width_page et_divi_theme et-db et_minified_js et_minified_css">
    <div id="page-container">
        <header id="main-header" data-height-onload="67">
           @include('frontend.blocks.menu')
        </header>
        <div id="et-main-area">
            <div id="main-content">
                <article id="post-73" class="post-73 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div id="et-boc" class="et-boc">
                            <div class="et_builder_inner_content et_pb_gutters3">
                                <div class="et_pb_section et_pb_section_0 et_section_regular">
                                    <div class="et_pb_row et_pb_row_0">
                                        <div class="et_pb_column et_pb_column_4_4 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_cta_0 et_pb_promo et_pb_bg_layout_light  et_pb_text_align_center">
                                                <div class="et_pb_promo_description">
                                                    <h2 class="et_pb_module_header">Khó khăn khi lựa chọn Bảo Hiểm Nhân Thọ ?</h2>
                                                    <div>Chúng tôi sẽ tư vấn cho bạn mọi vấn đề liên quan. Nhấn vào nút &#8220;Tư vấn miễn phí &#8221; ngay.</div>
                                                </div>
                                                <div class="et_pb_button_wrapper"><a class="btn btn-primary" href="{{route('tu-van-mien-phi')}}">Tư vấn miễn phí</a></div>
                                            </div>
                                            <div class="et_pb_module et_pb_blog_0 et_pb_blog_grid_wrapper">
                                                <div class="et_pb_blog_grid clearfix et_pb_bg_layout_light ">
                                                    <div class="et_pb_ajax_pagination_container">
                                                        <div class="et_pb_salvattore_content" data-columns>
                                                            @foreach($news as $item)
                                                                <article id="post-2108" class="et_pb_post clearfix et_pb_no_thumb post-2108 post type-post status-publish format-standard hentry category-bai-viet">
                                                                    <h2 class="entry-title">
                                                                        <a href="{{route('bai-viet-detail',[$item->slug])}}">{{$item->title}}</a>
                                                                    </h2>
                                                                    <p class="post-meta"></p>
                                                                    <div class="post-content">
                                                                        <div class="post-content-inner">
                                                                            <p>{{$item->intro}}</p>
                                                                        </div>
                                                                    </div>
                                                                </article>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div> <span class="et_pb_scroll_top et-pb-icon"></span>
           @include('frontend.blocks.footer')
        </div>
    </div>
    <script type='text/javascript'>
        var kk_star_ratings = {
            "action": "kk-star-ratings",
            "endpoint": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "nonce": "ab5ffef0a5"
        };
    </script>
    <script type='text/javascript'>
        var DIVI = {
            "item_count": "%d Item",
            "items_count": "%d Items"
        };
        var et_shortcodes_strings = {
            "previous": "Previous",
            "next": "Next"
        };
        var et_pb_custom = {
            "ajaxurl": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/images",
            "builder_images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/includes\/builder\/images",
            "et_frontend_nonce": "fbc068d605",
            "subscription_failed": "Please, check the fields below to make sure you entered the correct information.",
            "et_ab_log_nonce": "43b4f535e7",
            "fill_message": "Please, fill in the following fields:",
            "contact_error_message": "Please, fix the following errors:",
            "invalid": "Invalid email",
            "captcha": "Captcha",
            "prev": "Prev",
            "previous": "Previous",
            "next": "Next",
            "wrong_captcha": "You entered the wrong number in captcha.",
            "ignore_waypoints": "no",
            "is_divi_theme_used": "1",
            "widget_search_selector": ".widget_search",
            "is_ab_testing_active": "",
            "page_id": "73",
            "unique_test_id": "",
            "ab_bounce_rate": "5",
            "is_cache_plugin_active": "no",
            "is_shortcode_tracking": "",
            "tinymce_uri": ""
        };
        var et_pb_box_shadow_elements = [];
    </script>
    <script type="text/javascript" defer src="{{asset('frontend/wp-content/cache/autoptimize/js/autoptimize_248223b3062b995f830c22b95fd0ced2.js')}}"></script>
</body>
</html>
