@extends('frontend.welcome')
@section('content')
<article id="post-69" class="post-69 page type-page status-publish hentry">
            <div class="entry-content">
                <div id="et-boc" class="et-boc">
                    <div class="et_builder_inner_content et_pb_gutters3">
                        <div class="et_pb_section et_pb_section_0 et_pb_with_background et_section_regular">
                            <div class="et_pb_row et_pb_row_0">
                                <div class="et_pb_column et_pb_column_2_3 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_dark  et_pb_text_align_left">
                                        <div class="et_pb_text_inner">
                                            <p>Tích lũy. Tiết kiệm. Sức khỏe. Viện phí.
                                                <br />Gói bảo hiểm tất cả trong một.</p>
                                        </div>
                                    </div>
                                    <div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_dark  et_pb_text_align_left">
                                        <div class="et_pb_text_inner">
                                            <p>Nhập thông tin vào mẫu để được tư vấn miễn phí các sản phẩm BHNT tốt nhất hiện nay.</p>
                                        </div>
                                    </div>
                                    <div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_dark  et_pb_text_align_left">
                                        <div class="et_pb_text_inner">
                                            <p><strong>Bạn có biết:</strong> tỉ lệ người dân tham gia Bảo hiểm nhân thọ ở Úc là 52%, ở Mỹ là ~60%, ở Canada là ~70% và ở Việt Nam chỉ là 8%. Số liệu này dựa trên thống kê năm 2018 bởi Hiệp hội Nghiên cứu và tiếp thị bảo hiểm nhân thọ LIMRA &#8211; Life Insurance Marketing Research Association.</p>
                                        </div>
                                    </div>
                                    <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_right et_pb_module "> <a class="btn btn-primary" href="#two">Tìm hiểu thêm</a>
                                    </div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_3 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_text et_pb_text_3 et_pb_bg_layout_light  et_pb_text_align_left">
                                        <div class="et_pb_text_inner">
                                            <div class="frm_forms  with_frm_style frm_style_formidable-style" id="frm_form_10_container">
                                                <form enctype="multipart/form-data" action="{{route('tu-van-mien-phi')}}" method="post" class="frm-show-form " id="form_mua6uc2222">
                                                    @csrf
                                                    <div class="frm_form_fields ">
                                                        <fieldset>
                                                            <legend class="frm_hidden">Đăng ký tư vấn BHNT</legend>
                                                            <h3>Đăng ký tư vấn BHNT</h3>
                                                            <div class="frm_fields_container">
                                                                <input type="hidden" name="frm_action" value="create" />
                                                                <input type="hidden" name="form_id" value="10" />
                                                                <input type="hidden" name="frm_hide_fields_10" id="frm_hide_fields_10" value="" />
                                                                <input type="hidden" name="form_key" value="mua6uc2222" />
                                                                <input type="hidden" name="item_meta" value="" />
                                                                <input type="hidden" id="frm_submit_entry_10" name="frm_submit_entry_10" value="626c203fea" />
                                                                <input type="hidden" name="_wp_http_referer" value="/" />
                                                                <label for="frm_verify_10" class="frm_screen_reader frm_hidden">If you are human, leave this field blank.</label>
                                                                <input type="text" class="frm_hidden frm_verify" id="frm_verify_10" name="frm_verify" value="" />
                                                                <div id="frm_field_72_container" class="frm_form_field form-field  frm_top_container horizontal_radio">
                                                                    <label class="frm_primary_label">Nơi bạn đang sinh sống <span class="frm_required"></span> </label>
                                                                    <div class="frm_opt_container">
                                                                        <div class="frm_radio" id="frm_radio_72-0">
                                                                            <label for="field_ceby2a3-0">
                                                                                <input type="radio" name="khuvuc" id="field_ceby2a3-0" value="1" data-invmsg="Nơi bạn đang sinh sống is invalid" /> TP. Hồ Chí Minh</label>
                                                                        </div>
                                                                        <div class="frm_radio" id="frm_radio_72-1">
                                                                            <label for="field_ceby2a3-1">
                                                                                <input type="radio" name="khuvuc" id="field_ceby2a3-1" value="2" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Bình Dương</label>
                                                                        </div>
                                                                        <div class="frm_radio" id="frm_radio_72-2">
                                                                            <label for="field_ceby2a3-2">
                                                                                <input type="radio" name="khuvuc" id="field_ceby2a3-2" value="3" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Đồng Nai</label>
                                                                        </div>
                                                                        <div class="frm_radio" id="frm_radio_72-3">
                                                                            <label for="field_ceby2a3-3">
                                                                                <input type="radio" name="khuvuc" id="field_ceby2a3-3" value="4" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Hà Nội</label>
                                                                        </div>
                                                                        <div class="frm_radio" id="frm_radio_72-4">
                                                                            <label for="field_ceby2a3-4">
                                                                                <input type="radio" name="khuvuc" id="field_ceby2a3-4" value="5" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Nơi khác</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="frm_field_73_container" class="frm_form_field form-field  frm_required_field frm_left_container frm_inline">
                                                                    <label for="field_kh7lgy33" class="frm_primary_label">Họ và tên <span class="frm_required">*</span> </label>
                                                                    <input type="text" id="field_kh7lgy33" name="fullname" value="" style="width:400px" placeholder="Họ và tên của người tham gia BHNT" data-reqmsg="Không thể để trống thông tin này." aria-required="true" data-invmsg="Họ và tên is invalid" class="auto_width" />
                                                                </div>
                                                                <div id="frm_field_74_container" class="frm_form_field form-field  frm_required_field frm_left_container frm_inline">
                                                                    <label for="field_oodfpr33" class="frm_primary_label">Số điện thoại <span class="frm_required">*</span> </label>
                                                                    <input type="text" id="field_oodfpr33" name="phone" value="" style="width:400px" placeholder="Số điện thoại để liên lạc" data-reqmsg="Không thể để trống thông tin này." aria-required="true" data-invmsg="Số điện thoại is invalid" class="auto_width" />
                                                                </div>
                                                                <input type="hidden" name="item_key" value="" />
                                                                <div class="frm_submit">
                                                                    <input type="submit" value="Gửi yêu cầu tư vấn" />
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="two" class="et_pb_section et_pb_section_1 et_section_regular section_has_divider et_pb_bottom_divider">
                            <div class="et_pb_row et_pb_row_1">
                                <div class="et_pb_column et_pb_column_4_4 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_text et_pb_text_4 et_pb_bg_layout_light  et_pb_text_align_center">
                                        <div class="et_pb_text_inner">
                                            <p><span style="color: #4a4a4a;">Tham gia Bảo hiểm Nhân Thọ chỉ với 4 bước:</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="et_pb_row et_pb_row_2 et_pb_equal_columns">
                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_3  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_blurb et_pb_blurb_0 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
                                        <div class="et_pb_blurb_content">
                                          {{--   <div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_off et-pb-icon">&#x68;</span></span>
                                            </div> --}}
                                            <div class="et_pb_blurb_container">
                                                <h4 class="et_pb_module_header"><span style="color: blue;font-weight: 800;">1. Nhập thông tin</span></h4>
                                                <div class="et_pb_blurb_description">
                                                    <p>Bạn nhập thông tin theo mẫu có sẵn. <span style="text-decoration: underline;"><span style="color: #3366ff;"><a style="color: #3366ff; text-decoration: underline;" href="{{route('tu-van-mien-phi')}}" rel="nofollow">Nhấn vào đây</a></span></span> để nhập thông tin</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_4  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_blurb et_pb_blurb_1 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
                                        <div class="et_pb_blurb_content">
                                            {{-- <div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_off et-pb-icon">&#xe0f7;</span></span>
                                            </div> --}}
                                            <div class="et_pb_blurb_container">
                                                <h4 class="et_pb_module_header"><span style="color: blue;font-weight: 800;">2. Phân tích</span></h4>
                                                <div class="et_pb_blurb_description">
                                                    <p>Các chuyên viên bảo hiểm sẽ phân tích để xác định gói bảo hiểm phù hợp</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_5  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_blurb et_pb_blurb_2 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
                                        <div class="et_pb_blurb_content">
                                            {{-- <div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_off et-pb-icon">&#xe090;</span></span>
                                            </div> --}}
                                            <div class="et_pb_blurb_container">
                                                <h4 class="et_pb_module_header"><span style="color: blue;font-weight: 800;">3. Liên hệ</span></h4>
                                                <div class="et_pb_blurb_description">
                                                    <p>Các chuyên viên liên hệ với bạn để tư vấn các gói bảo hiểm phù hợp nhất</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_6  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_blurb et_pb_blurb_3 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
                                        <div class="et_pb_blurb_content">
                                           {{--  <div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_off et-pb-icon">&#x6e;</span></span>
                                            </div> --}}
                                            <div class="et_pb_blurb_container">
                                                <h4 class="et_pb_module_header"><span style="color: blue;font-weight: 800;">4. Hoàn tất</span></h4>
                                                <div class="et_pb_blurb_description">
                                                    <p>Chuyên viên tư vấn sẽ hoàn thành toàn bộ thủ tục khi bạn đồng ý tham gia.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="et_pb_bottom_inside_divider et-no-transition"></div>
                        </div>
                        <div class="et_pb_section et_pb_section_2 et_section_regular section_has_divider et_pb_bottom_divider et_pb_top_divider">
                            <div class="et_pb_top_inside_divider et-no-transition"></div>
                            <div class="et_pb_row et_pb_row_3 et_pb_gutters1">
                                <div class="et_pb_column et_pb_column_4_4 et_pb_column_7  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_text et_pb_text_5 et_pb_bg_layout_light  et_pb_text_align_right et_pb_text_align_center-tablet">
                                        <div class="et_pb_text_inner">
                                            <p><span style="color: #4a4a4a;">Tích lũy, tiết kiệm một cách kỷ luật</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="et_pb_row et_pb_row_4 et_pb_equal_columns et_pb_gutters1">
                                <div class="et_pb_column et_pb_column_1_2 et_pb_column_8  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_image et_pb_image_0 et_animated et-waypoint"> <span class="et_pb_image_wrap "><img  data-srcset="https://netbaohiem.com/wp-content/uploads/2019/09/3-netbaohiem.com-net-bao-hiem-bhnt-tiet-kiem-co-ky-luat.jpg 357w, https://netbaohiem.com/wp-content/uploads/2019/09/3-netbaohiem.com-net-bao-hiem-bhnt-tiet-kiem-co-ky-luat-183x300.jpg 183w" sizes="(max-width: 357px) 100vw, 357px" data-src="https://netbaohiem.com/wp-content/uploads/2019/09/3-netbaohiem.com-net-bao-hiem-bhnt-tiet-kiem-co-ky-luat.jpg" class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/2019/09/3-netbaohiem.com-net-bao-hiem-bhnt-tiet-kiem-co-ky-luat.jpg" srcset="https://netbaohiem.com/wp-content/uploads/2019/09/3-netbaohiem.com-net-bao-hiem-bhnt-tiet-kiem-co-ky-luat.jpg 357w, https://netbaohiem.com/wp-content/uploads/2019/09/3-netbaohiem.com-net-bao-hiem-bhnt-tiet-kiem-co-ky-luat-183x300.jpg 183w" sizes="(max-width: 357px) 100vw, 357px" /></noscript></span></div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_2 et_pb_column_9  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_light  et_pb_text_align_left">
                                        <div class="et_pb_text_inner">
                                            <p>Đối với các loại hợp đồng bảo hiểm phi nhân thọ, khi bạn đóng phí thì số tiền đó sẽ không lấy lại được.</p>
                                            <p>Tuy nhiên, hợp đồng Bảo hiểm Nhân thọ lại có tính chất tích lũy, hay còn gọi là có Giá Trị Hoàn Lại (GTHL). Tiền bạn đóng vào hàng năm vẫn nằm trong tài khoản hợp đồng. Khi hợp đồng đáo hạn (kết thúc hợp đồng), bạn sẽ nhận lại toàn bộ tiền gốc và lãi trong suốt thời gian đóng phí.</p>
                                            <p>Phần tiền tiết kiệm này sẽ rất có giá trị trong tương lai, khi gia đình bạn có thêm các thành viên mới. Giả sử khi con bạn đủ 18 tuổi và bé cần tiền để học Đại học; hoặc trong tương lai xa hơn, con bạn cần tài chính để khởi nghiệp, kết hôn, mua nhà&#8230;Tiết kiệm tài chính ngay từ bây giờ sẽ giúp bạn chuẩn bị cho tương lai của bé được tốt hơn.</p>
                                            <p>Một lợi ích nữa của tiết kiệm tiền là để khi bạn đã về hưu, lúc này sức lao động không còn được như thời trẻ, thì số tiền tiết kiệm này sẽ giúp bạn có một tuổi hưu an nhàn hơn, tự chủ được tài chính khi về già.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="et_pb_row et_pb_row_5">
                                <div class="et_pb_column et_pb_column_4_4 et_pb_column_10  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_text et_pb_text_7 et_pb_bg_layout_light  et_pb_text_align_left et_pb_text_align_center-tablet">
                                        <div class="et_pb_text_inner">
                                            <p><span style="color: #4a4a4a;">Được bảo hiểm trong quá trình tiết kiệm</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="et_pb_row et_pb_row_6 et_pb_equal_columns et_pb_gutters1">
                                <div class="et_pb_column et_pb_column_1_2 et_pb_column_11  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_text et_pb_text_8 et_pb_bg_layout_light  et_pb_text_align_left">
                                        <div class="et_pb_text_inner">
                                            <p>Trong quá trình bạn tiết kiệm tiền cho tương lai của con cái, cho tuổi hưu của bản thân&#8230;luôn có khả năng rủi ro sẽ xảy ra bất kì lúc nào và đôi khi để lại hậu quả khôn lường về mặt tài chính cũng như tinh thần/thể xác.</p>
                                            <p>Bảo hiểm Nhân thọ sẽ bồi thường cho bạn một số tiền rất lớn (gấp khoảng 100 lần phí đóng) nếu không may rủi ro xảy ra, và có hiệu lực ngay vào ngày bạn tham gia. Các loại rủi ro mà BHNT thường hỗ trợ:</p>
                                            <ul>
                                                <li>Tai nạn</li>
                                                <li>Bệnh hiểm nghèo</li>
                                                <li>Viện phí</li>
                                            </ul>
                                            <p>Chi phí để chữa trị một ca bệnh ung thư dao động từ 500 triệu &#8211; 2 tỷ đồng; vấn đề tai nạn giao thông tại Việt Nam đang có xu hướng tăng lên rất nhanh trong những năm gần đây; tiền viện phí cho một ca chấn thương chỉnh hình dao động từ 200 triệu &#8211; 1 tỷ đồng tùy mức độ.</p>
                                            <p>Tham gia BHNT ít nhất giúp cho gia đình và người thân không phải lo lắng về mặt tài chính nếu chuyện không may xảy ra.</p>
                                        </div>
                                    </div>
                                    <div class="et_pb_button_module_wrapper et_pb_button_1_wrapper et_pb_button_alignment_right et_pb_module "> <a class="btn btn-primary" href="{{route('tu-van-mien-phi')}}" rel="nofollow">Đăng ký ngay</a></div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_2 et_pb_column_12  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_image et_pb_image_1 et_animated et-waypoint"> <span class="et_pb_image_wrap "><img  data-srcset="https://netbaohiem.com/wp-content/uploads/2019/09/5-netbaohiem-net-bao-hiem-netbaohiem.com-bao-hiem-nhan-tho-bhnt.jpg 398w, https://netbaohiem.com/wp-content/uploads/2019/09/5-netbaohiem-net-bao-hiem-netbaohiem.com-bao-hiem-nhan-tho-bhnt-196x300.jpg 196w" sizes="(max-width: 398px) 100vw, 398px" data-src="https://netbaohiem.com/wp-content/uploads/2019/09/5-netbaohiem-net-bao-hiem-netbaohiem.com-bao-hiem-nhan-tho-bhnt.jpg" class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/2019/09/5-netbaohiem-net-bao-hiem-netbaohiem.com-bao-hiem-nhan-tho-bhnt.jpg" srcset="https://netbaohiem.com/wp-content/uploads/2019/09/5-netbaohiem-net-bao-hiem-netbaohiem.com-bao-hiem-nhan-tho-bhnt.jpg 398w, https://netbaohiem.com/wp-content/uploads/2019/09/5-netbaohiem-net-bao-hiem-netbaohiem.com-bao-hiem-nhan-tho-bhnt-196x300.jpg 196w" sizes="(max-width: 398px) 100vw, 398px" /></noscript></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="et_pb_section et_pb_section_3 et_pb_section_parallax et_pb_with_background et_section_regular">
                            <div class="et_parallax_bg_wrap">
                                <div class="et_parallax_bg et_pb_parallax_css" style="background-image: url(frontend/wp-content/uploads/2015/05/toi-muon-mua-bao-hiem-nhan-tho.jpg);"></div>
                            </div>
                            <div class="et_pb_row et_pb_row_7">
                                <div class="et_pb_column et_pb_column_4_4 et_pb_column_13  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_text et_pb_text_9 et_pb_bg_layout_light  et_pb_text_align_left">
                                        <div class="et_pb_text_inner">
                                            <h1>Vì sao bạn cần một tư vấn viên đáng tin cậy?</h1></div>
                                    </div>
                                </div>
                            </div>
                            <div class="et_pb_row et_pb_row_8">
                                <div class="et_pb_column et_pb_column_2_3 et_pb_column_14  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_text et_pb_text_10 et_pb_bg_layout_light  et_pb_text_align_left">
                                        <div class="et_pb_text_inner">
                                            <h2>Nhiều sản phẩm, khó lựa chọn</h2>
                                            <p>Ở Việt Nam hiện nay có rất nhiều công ty Bảo hiểm nhân thọ, mỗi công ty lại có rất nhiều sản phẩm khác nhau, dẫn đến khó khăn trong việc chọn lựa gói Bảo hiểm nhân thọ tốt nhất. Trên thực tế, mỗi sản phẩm Bảo hiểm đều có các chức năng hoàn toàn khác nhau, và việc xác định gói Bảo hiểm tốt nhất phụ thuộc vào các yếu tố như độ tuổi, giới tính, thu nhập…của bản thân bạn. Do đó, bạn cần một người cố vấn với kiến thức rộng và kỹ năng chuyên môn cao để xác định đúng nhu cầu của bản thân.</p>
                                            <h2>Hợp đồng dài hạn</h2>
                                            <p>Vì việc mua bảo hiểm nhân thọ </a>mang tính dài hạn, nên vấn đề CSKH là yếu tố tiên quyết tạo nên một công ty BHNT uy tín.</p>
                                            <p>Nếu không may rủi ro xảy ra, những vấn đề thủ tục, giấy tờ, hồ sơ&#8230;để giải quyết yêu cầu bồi thường đôi khi làm bạn lúng túng. Nếu bạn tham gia BHNT với một công ty uy tín, có chế độ CSKH tốt, nhân viên sẽ hỗ trợ bạn 90% thời gian và công sức.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_3 et_pb_column_15  et_pb_css_mix_blend_mode_passthrough et-last-child et_pb_column_empty"></div>
                            </div>
                        </div>
                        <div class="et_pb_section et_pb_section_4 et_pb_with_background et_section_regular">
                            <div class="et_pb_row et_pb_row_9 et_pb_gutters1">
                                <div class="et_pb_column et_pb_column_2_5 et_pb_column_16  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_image et_pb_image_2 et_animated et-waypoint"> <span class="et_pb_image_wrap "><img data-src="https://netbaohiem.com/wp-content/uploads/2015/04/bao-hiem-nhan-tho-nao-tot-nhat.jpg" class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="frontend/wp-content/uploads/2015/04/bao-hiem-nhan-tho-nao-tot-nhat.jpg" /></noscript></span></div>
                                </div>
                                <div class="et_pb_column et_pb_column_3_5 et_pb_column_17  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_text et_pb_text_11 et_pb_bg_layout_light  et_pb_text_align_left">
                                        <div class="et_pb_text_inner">
                                            <h1 class="">Chúng tôi là những nhà cố vấn tận tâm</h1>
                                            <ul class="">
                                                <li class=""><strong>Kinh nghiệm chuyên môn</strong></li>
                                            </ul>
                                            <p class="">Chúng tôi là một đội ngũ tư vấn viên với nhiều năm kinh nghiệm trong ngành Bảo hiểm nhân thọ, liên tục cập nhật và nắm bắt tất cả các gói sản phẩm trên thị trường. Do đó, chúng tôi có đủ tự tin để tư vấn cho khách hàng những gói sản phẩm tốt nhất.</p>
                                            <ul class="">
                                                <li class=""><strong class="">Đạo đức của người tư vấn</strong></li>
                                            </ul>
                                            <p class="">Với châm ngôn “chỉ tư vấn cho khách hàng những gói sản phẩm tốt nhất”, chúng tôi đặc biệt không chạy theo lợi nhuận. Netbaohiem.com hoạt động hoàn toàn độc lập !</p>
                                            <ul>
                                                <li> <strong>Phí tư vấn = 0 đồng<br /></strong></li>
                                            </ul>
                                            <p>Chúng tôi tư vấn hoàn toàn miễn phí cho bạn !</p>
                                        </div>
                                    </div>
                                    <div class="et_pb_button_module_wrapper et_pb_button_2_wrapper et_pb_button_alignment_right et_pb_module "> <a class="btn btn-primary" href="{{route('tu-van-mien-phi')}}" rel="nofollow">Đăng ký ngay</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="et_pb_section et_pb_section_5 et_section_regular">
                            <div class="et_pb_row et_pb_row_10">
                                <div class="et_pb_column et_pb_column_4_4 et_pb_column_18  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_text et_pb_text_12 et_pb_bg_layout_light  et_pb_text_align_center">
                                        <div class="et_pb_text_inner">
                                            <p>Các khách hàng của chúng tôi nói gì?</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="et_pb_row et_pb_row_11">
                                <div class="et_pb_column et_pb_column_1_2 et_pb_column_19  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_with_border et_pb_module et_pb_testimonial et_pb_testimonial_0 clearfix et_pb_bg_layout_light  et_pb_text_align_left et_pb_testimonial_no_image">
                                        <div style="background-image:url(frontend/wp-content/uploads/2015/04/b%e1%ba%a3o-hi%e1%bb%83m-nh%c3%a2n-th%e1%bb%8d.jpg)" class="et_pb_testimonial_portrait"></div>
                                        <div class="et_pb_testimonial_description">
                                            <div class="et_pb_testimonial_description_inner">
                                                <p>Ban đầu chúng tôi thực sự rất phân vân về việc có nên mua bảo hiểm nhân thọ không, nhưng nhận thấy các rủi ro trong cuộc sống có thể xảy ra với mình bất kỳ lúc nào, đặc biệt là tai nạn giao thông đang là một vấn nạn của Việt Nam. Do đó, chúng tôi quyết định nhờ sự hỗ trợ của các tư vấn viên và đã chọn ra được sản phẩm phù hợp nhất. Giờ đây 2 vợ chồng tôi có thể an tâm phần nào, nếu có bất trắc xảy ra, các con của chúng tôi cũng không phải chịu quá nhiều thiệt thòi.</p> <span class="et_pb_testimonial_author">Vợ chồng anh Tuấn - chị Thảo</span>
                                                <p class="et_pb_testimonial_meta"><span class="et_pb_testimonial_company">Quận 1, TP.HCM</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_2 et_pb_column_20  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_with_border et_pb_module et_pb_testimonial et_pb_testimonial_1 clearfix et_pb_bg_layout_light  et_pb_text_align_left et_pb_testimonial_no_image">
                                        <div style="background-image:url(frontend/wp-content/uploads/2015/04/can-mua-bao-hiem-nhan-tho.jpg)" class="et_pb_testimonial_portrait"></div>
                                        <div class="et_pb_testimonial_description">
                                            <div class="et_pb_testimonial_description_inner">
                                                <p>Do tuổi đã cao và là trụ cột trong gia đình, từ lâu tôi đã định mua bảo hiểm nhân thọ cho bản thân để phòng khi tình huống xấu xảy ra thì vợ và 2 con tôi vẫn tiếp tục cuộc sống. Do không biết nên mua bảo hiểm của công ty nào, tôi đã tìm đến các tư vấn viên và được tư vấn rất kỹ lưỡng về các gói sản phẩm. Cuối cùng tôi cũng chọn ra cho mình được một sản phẩm phù hợp với nhu cầu và khả năng tài chính của bản thân. Xin cảm ơn các bạn đã hỗ trợ tôi.</p> <span class="et_pb_testimonial_author">Anh Trần Đăng Thành</span>
                                                <p class="et_pb_testimonial_meta"><span class="et_pb_testimonial_company">Thuận An, Bình Dương</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="et_pb_section et_pb_section_6 et_pb_with_background et_section_regular">
                            <div class="et_pb_row et_pb_row_12 et_pb_gutters1">
                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_21  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_number_counter et_pb_number_counter_0 et_pb_bg_layout_light  et_pb_text_align_center et_pb_with_title" data-number-value="950+" data-number-separator="">
                                        <div class="percent">
                                            <p><span class="percent-value"></span><span class="percent-sign"></span></p>
                                        </div>
                                        <h3 class="title">khách hàng</h3></div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_22  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_number_counter et_pb_number_counter_1 et_pb_bg_layout_light  et_pb_text_align_center et_pb_with_title" data-number-value="800+" data-number-separator="">
                                        <div class="percent">
                                            <p><span class="percent-value"></span><span class="percent-sign"></span></p>
                                        </div>
                                        <h3 class="title">hợp đồng</h3></div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_23  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_number_counter et_pb_number_counter_2 et_pb_bg_layout_light  et_pb_text_align_center et_pb_with_title" data-number-value="20+" data-number-separator="">
                                        <div class="percent">
                                            <p><span class="percent-value"></span><span class="percent-sign"></span></p>
                                        </div>
                                        <h3 class="title">tỷ VNĐ</h3></div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_24  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_number_counter et_pb_number_counter_3 et_pb_bg_layout_light  et_pb_text_align_center et_pb_with_title" data-number-value="98" data-number-separator="">
                                        <div class="percent">
                                            <p><span class="percent-value"></span><span class="percent-sign">%</span></p>
                                        </div>
                                        <h3 class="title">hài lòng</h3></div>
                                </div>
                            </div>
                        </div>
                        <div class="et_pb_section et_pb_section_7 et_pb_section_parallax et_pb_with_background et_section_regular">
                            <div class="et_parallax_bg_wrap">
                                <div class="et_parallax_bg" style="background-image: url(frontend/wp-content/uploads/2019/09/transportation-services-01.png);"></div>
                            </div>
                            <div class="et_pb_with_border et_pb_row et_pb_row_13 et_pb_row_fullwidth et_pb_equal_columns et_pb_gutters1">
                                <div class="et_pb_column et_pb_column_1_2 et_pb_column_25  et_pb_css_mix_blend_mode_passthrough">
                                    <div class="et_pb_module et_pb_text et_pb_text_13 et_pb_bg_layout_dark  et_pb_text_align_center">
                                        <div class="et_pb_text_inner">
                                            <p>Câu hỏi thường gặp</p>
                                        </div>
                                    </div>
                                    <div class="et_pb_module et_pb_accordion et_pb_accordion_0">
                                        <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_0  et_pb_toggle_open">
                                            <h5 class="et_pb_toggle_title">Tư vấn có tính phí không?</h5>
                                            <div class="et_pb_toggle_content clearfix">
                                                <p>Hoàn toàn miễn phí. Bạn chỉ phải đóng phí cho hợp đồng của bạn nếu tham gia BHNT.</p>
                                            </div>
                                        </div>
                                        <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_1  et_pb_toggle_close">
                                            <h5 class="et_pb_toggle_title">Sau bao lâu thì nhân viên liên hệ?</h5>
                                            <div class="et_pb_toggle_content clearfix">
                                                <p>Từ 1-3 ngày tùy số lượng khách hàng đăng ký tư vấn. Thông thường là 1 ngày.</p>
                                            </div>
                                        </div>
                                        <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_2  et_pb_toggle_close">
                                            <h5 class="et_pb_toggle_title">Nếu tham gia thì cần chuẩn bị giấy tờ gì?</h5>
                                            <div class="et_pb_toggle_content clearfix">
                                                <p>Chứng minh nhân dân hoặc hộ chiếu. Nếu người tham gia chưa đủ 18 tuổi thì cần giấy khai sinh. Bản gốc hoặc photo đều được, không cần công chứng.</p>
                                            </div>
                                        </div>
                                        <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_3  et_pb_toggle_close">
                                            <h5 class="et_pb_toggle_title">Ký hợp đồng có lâu không?</h5>
                                            <div class="et_pb_toggle_content clearfix">
                                                <p>Quá trình ký hợp đồng chỉ khoảng 10 đến 15 phút.</p>
                                            </div>
                                        </div>
                                        <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_4  et_pb_toggle_close">
                                            <h5 class="et_pb_toggle_title">Ký hợp đồng tại nhà được không?</h5>
                                            <div class="et_pb_toggle_content clearfix">
                                                <p>Bạn có thể lựa chọn địa điểm để ký hợp đồng ở bất kỳ đâu (tại công ty, tại nhà&#8230;).</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="et_pb_column et_pb_column_1_2 et_pb_column_26  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                    <div class="et_pb_module et_pb_text et_pb_text_14 et_pb_bg_layout_light  et_pb_text_align_center">
                                        <div class="et_pb_text_inner">
                                            <p>Đăng ký tham gia</p>
                                        </div>
                                    </div>
                                    <div class="et_pb_module et_pb_text et_pb_text_15 et_pb_bg_layout_light  et_pb_text_align_left">
                                        <div class="et_pb_text_inner">
                                            <div class="frm_forms  with_frm_style frm_style_formidable-style" id="frm_form_12_container">
                                                <form enctype="multipart/form-data" action="{{route('tu-van-mien-phi')}}" method="post" class="frm-show-form " id="form_mua6uc223">
                                                    @csrf
                                                    <div class="frm_form_fields ">
                                                        <fieldset>
                                                            <legend class="frm_hidden">Form homepage bottom</legend>
                                                            <div class="frm_fields_container">
                                                                <input type="hidden" name="frm_action" value="create" />
                                                                <input type="hidden" name="form_id" value="12" />
                                                                <input type="hidden" name="frm_hide_fields_12" id="frm_hide_fields_12" value="" />
                                                                <input type="hidden" name="form_key" value="mua6uc223" />
                                                                <input type="hidden" name="item_meta" value="" />
                                                                <input type="hidden" id="frm_submit_entry_12" name="frm_submit_entry_12" value="626c203fea" />
                                                                <input type="hidden" name="_wp_http_referer" value="/" />
                                                                <label for="frm_verify_12" class="frm_screen_reader frm_hidden">If you are human, leave this field blank.</label>
                                                                <input type="text" class="frm_hidden frm_verify" id="frm_verify_12" name="frm_verify" value="" />
                                                                <div id="frm_field_78_container" class="frm_form_field form-field  frm_top_container horizontal_radio">
                                                                    <label class="frm_primary_label">Nơi bạn đang sinh sống <span class="frm_required"></span> </label>
                                                                    <div class="frm_opt_container">
                                                                        <div class="frm_radio" id="frm_radio_78-0">
                                                                            <label for="field_ceby2a4-0">
                                                                                <input type="radio" name="khuvuc" id="field_ceby2a4-0" value="1" data-invmsg="Nơi bạn đang sinh sống is invalid" /> TP. Hồ Chí Minh</label>
                                                                        </div>
                                                                        <div class="frm_radio" id="frm_radio_78-1">
                                                                            <label for="field_ceby2a4-1">
                                                                                <input type="radio" name="khuvuc" id="field_ceby2a4-1" value="2" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Bình Dương</label>
                                                                        </div>
                                                                        <div class="frm_radio" id="frm_radio_78-2">
                                                                            <label for="field_ceby2a4-2">
                                                                                <input type="radio" name="khuvuc" id="field_ceby2a4-2" value="3" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Đồng Nai</label>
                                                                        </div>
                                                                        <div class="frm_radio" id="frm_radio_78-3">
                                                                            <label for="field_ceby2a4-3">
                                                                                <input type="radio" name="khuvuc" id="field_ceby2a4-3" value="4" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Hà Nội</label>
                                                                        </div>
                                                                        <div class="frm_radio" id="frm_radio_78-4">
                                                                            <label for="field_ceby2a4-4">
                                                                                <input type="radio" name="khuvuc" id="field_ceby2a4-4" value="5" data-invmsg="Nơi bạn đang sinh sống is invalid" /> Nơi khác</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="frm_field_79_container" class="frm_form_field form-field  frm_top_container horizontal_radio">
                                                                    <label class="frm_primary_label">Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) <span class="frm_required"></span> </label>
                                                                    <div class="frm_opt_container">
                                                                        <div class="frm_radio" id="frm_radio_79-0">
                                                                            <label for="field_cbrfgd2-0">
                                                                                <input type="radio" name="thunhap" id="field_cbrfgd2-0" value="1" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> 1 - 2 triệu VNĐ</label>
                                                                        </div>
                                                                        <div class="frm_radio" id="frm_radio_79-1">
                                                                            <label for="field_cbrfgd2-1">
                                                                                <input type="radio" name="thunhap" id="field_cbrfgd2-1" value="2" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> 2 - 4 triệu VNĐ</label>
                                                                        </div>
                                                                        <div class="frm_radio" id="frm_radio_79-2">
                                                                            <label for="field_cbrfgd2-2">
                                                                                <input type="radio" name="thunhap" id="field_cbrfgd2-2" value="3" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> 4 - 8 triệu VNĐ</label>
                                                                        </div>
                                                                        <div class="frm_radio" id="frm_radio_79-3">
                                                                            <label for="field_cbrfgd2-3">
                                                                                <input type="radio" name="thunhap" id="field_cbrfgd2-3" value="4" data-invmsg="Số tiền nhàn rỗi mỗi tháng của bạn (Tiền nhàn rỗi = Tổng thu nhập - Tổng chi phí) is invalid" /> Trên 8 triệu VNĐ</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="frm_field_80_container" class="frm_form_field form-field  frm_required_field frm_left_container frm_full">
                                                                    <label for="field_kh7lgy34" class="frm_primary_label">Họ và tên <span class="frm_required">*</span> </label>
                                                                    <input type="text" id="field_kh7lgy34" name="fullname" value="" style="width:400px" placeholder="Họ và tên của người tham gia BHNT" data-reqmsg="Không thể để trống thông tin này." aria-required="true" data-invmsg="Họ và tên is invalid" class="auto_width" />
                                                                </div>
                                                                <div id="frm_field_81_container" class="frm_form_field form-field  frm_required_field frm_left_container frm_full">
                                                                    <label for="field_oodfpr34" class="frm_primary_label">Số điện thoại <span class="frm_required">*</span> </label>
                                                                    <input type="text" id="field_oodfpr34" name="phone" value="" style="width:400px" placeholder="Số điện thoại để liên lạc" data-reqmsg="Không thể để trống thông tin này." aria-required="true" data-invmsg="Số điện thoại is invalid" class="auto_width" />
                                                                </div>
                                                                <div id="frm_field_83_container" class="frm_form_field form-field  frm_left_container frm_total">
                                                                    <label for="field_luq362" id="field_luq362_label" class="frm_primary_label">Năm sinh của người tham gia BHNT <span class="frm_required"></span> </label>
                                                                    <select name="namsinh" id="field_luq362" data-invmsg="Năm sinh của người tham gia BHNT is invalid" class="auto_width">
                                                                        @for($i =1933; $i<=2019; $i++)
                                                                           <option value="{{$i}}" selected >Năm {{$i}}</option>
                                                                        @endfor
                                                                    </select>
                                                                </div>
                                                                <input type="hidden" name="item_key" value="" />
                                                                <div class="frm_submit">
                                                                    <input type="submit" value="Gửi yêu cầu tư vấn" />
                                                                    </noscript>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
@endsection