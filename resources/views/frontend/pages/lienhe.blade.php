<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="pingback" href="../xmlrpc.php" />
    <script>
        var et_site_url = '../index.html';
        var et_post_id = '76';

        function et_core_page_resource_fallback(a, b) {
            "undefined" === typeof b && (b = a.sheet.cssRules && 0 === a.sheet.cssRules.length);
            b && (a.onerror = null, a.onload = null, a.href ? a.href = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id : a.src && (a.src = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id))
        }
    </script>
    <link type="text/css" media="all" href="{{asset('frontend/wp-content/cache/autoptimize/css/autoptimize_123690c73ba7d7d73ba06ddd913c3a77.css')}}" rel="stylesheet" />
    <title>@yield('title','Liên hệ - Bảo Hiểm')</title>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <link rel="canonical" href="index.html" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Liên hệ - Net Bảo Hiểm" />
    <meta property="og:url" content="https://netbaohiem.com/lien-he/" />
    <meta property="og:site_name" content="Net Bảo Hiểm" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="Liên hệ - Net Bảo Hiểm" />
    <script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://netbaohiem.com/#website","url":"https://netbaohiem.com/","name":"Net B\u1ea3o Hi\u1ec3m","description":"T\u01b0 V\u1ea5n Mi\u1ec5n Ph\u00ed B\u1ea3o Hi\u1ec3m Nh\u00e2n Th\u1ecd","potentialAction":{"@type":"SearchAction","target":"https://netbaohiem.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://netbaohiem.com/lien-he/#webpage","url":"https://netbaohiem.com/lien-he/","inLanguage":"en-US","name":"Li\u00ean h\u1ec7 - Net B\u1ea3o Hi\u1ec3m","isPartOf":{"@id":"https://netbaohiem.com/#website"},"datePublished":"2015-04-24T15:49:21+00:00","dateModified":"2019-09-02T09:35:31+00:00","breadcrumb":{"@id":"https://netbaohiem.com/lien-he/#breadcrumb"}},{"@type":"BreadcrumbList","@id":"https://netbaohiem.com/lien-he/#breadcrumb","itemListElement":[{"@type":"ListItem","position":1,"item":{"@type":"WebPage","@id":"https://netbaohiem.com/","url":"https://netbaohiem.com/","name":"Home"}},{"@type":"ListItem","position":2,"item":{"@type":"WebPage","@id":"https://netbaohiem.com/lien-he/","url":"https://netbaohiem.com/lien-he/","name":"Li\u00ean h\u1ec7"}}]}]}</script>
    <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
    <link rel='dns-prefetch' href='http://s.w.org/' />
    <link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Feed" href="../feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Comments Feed" href="../comments/feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Liên hệ Comments Feed" href="feed/index.html" />
    <meta content="Divi v.3.29.3" name="generator" />
    <link rel='stylesheet' id='et-builder-googlefonts-cached-css' href='https://fonts.googleapis.com/css?family=Merriweather%3A300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic%7CLora%3Aregular%2Citalic%2C700%2C700italic&amp;ver=5.3#038;subset=cyrillic,latin,latin-ext,cyrillic-ext,vietnamese' type='text/css' media='all' />
    <script type='text/javascript' src='{{asset('frontend/wp-includes/js/jquery/jquery4a5f.js?ver=1.12.4-wp')}}'></script>
    <link rel='https://api.w.org/' href='../wp-json/index.html' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="../xmlrpc0db0.php?rsd" />
    <meta name="generator" content="WordPress 5.3" />
    <link rel='shortlink' href='../index04cd.html?p=76' />
    <link rel="alternate" type="application/json+oembed" href="../wp-json/oembed/1.0/embedc155.json?url=https%3A%2F%2Fnetbaohiem.com%2Flien-he%2F" />
    <link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embedcf73?url=https%3A%2F%2Fnetbaohiem.com%2Flien-he%2F&amp;format=xml" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="shortcut icon" href="../wp-content/uploads/2015/05/favicon-1-netbaohiem.png" />
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-147696565-1', 'auto');
        ga('send', 'pageview');
    </script>
    <link rel="stylesheet" id="et-core-unified-cached-inline-styles" href="{{asset('frontend/wp-content/cache/et/76/et-core-unified-1575219638103.min.css')}}" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
     <script >
    $(document).ready(function() {
         $('#flash-message').delay(1000).slideUp(300);
      });
</script> 
</head>

<body data-rsssl=1 class="page-template-default page page-id-76 et_pb_button_helper_class et_transparent_nav et_fixed_nav et_show_nav et_cover_background et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_pb_pagebuilder_layout et_full_width_page et_divi_theme et-db et_minified_js et_minified_css">
    <div id="page-container">
        <header id="main-header" data-height-onload="67">
     @include('frontend.blocks.menu')
        </header>
        <div id="et-main-area">
            <div id="main-content">
                <article id="post-76" class="post-76 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div id="et-boc" class="et-boc">
                            <div class="et_builder_inner_content et_pb_gutters3">
                                <div class="et_pb_section et_pb_section_0 et_section_regular">
                                    <div class="et_pb_row et_pb_row_0">
                                        <div class="et_pb_column et_pb_column_2_3 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough">
                                            <div id="et_pb_contact_form_0" class="et_pb_module et_pb_contact_form_0 et_pb_contact_form_container clearfix" data-form_unique_num="0">
                                                <h1 class="et_pb_contact_main_title">Liên hệ</h1>
                                                <div class="et-pb-contact-message">
                                                    @include('admin.blocks.alert')
                                                </div>
                                                 
                                                <div class="et_pb_contact">
                                                    <form class="et_pb_contact_form clearfix" method="post" action="{{route('lien-he')}}">
                                                        @csrf
                                                        <p class="et_pb_contact_field et_pb_contact_field_0 et_pb_contact_field_half" data-id="name" data-type="input">
                                                            <label for="et_pb_contact_name_0" class="et_pb_contact_form_label">Name</label>
                                                            <input type="text" id="et_pb_contact_name_0" class="input" value="" name="name" data-required_mark="required" data-field_type="input" data-original_id="name" placeholder="Name">
                                                        </p>
                                                        <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half et_pb_contact_field_last" data-id="email" data-type="email">
                                                            <label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Phone</label>
                                                            <input type="number" id="et_pb_contact_email_0" class="input" value="" name="phone" data-required_mark="required"  placeholder="Phone">
                                                        </p>
                                                        <p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_last" data-id="message" data-type="text">
                                                            <label for="et_pb_contact_message_0" class="et_pb_contact_form_label">Message</label>
                                                            <textarea name="message" id="et_pb_contact_message_0" class="et_pb_contact_message input" data-required_mark="required" data-field_type="text" data-original_id="message" placeholder="Message"></textarea>
                                                        </p>
                                                        <div class="et_contact_bottom_container">
                                                            <button type="submit" name="et_builder_submit_button" class="btn btn-primary" >Gửi</button>
                                                        </div>
                                                        <input type="hidden" id="_wpnonce-et-pb-contact-form-submitted-0" name="_wpnonce-et-pb-contact-form-submitted-0" value="f962ac44a8" />
                                                        <input type="hidden" name="_wp_http_referer" value="/lien-he/" />
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="et_pb_column et_pb_column_1_3 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">Nhập tên, email và nội dung để được giải đáp các thắc mắc. Chúng tôi sẽ phản hồi qua số điện thoại trong thời gian sớm nhất.</p>
                                                    <p><strong>Địa chỉ liên hệ:</strong> Lầu 80, tòa nhà Landmark 81, 720A Điện Biên Phủ, Phường 22, Quận Bình Thạnh, TP.HCM.</p>
                                                    <p><strong>Số điện thoại:</strong> +84981649545</p>
                                                    <p><strong>Email:</strong> support@netbaohiem.com</p>
                                                    <p>&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div> 
            @include('frontend.blocks.footer')
        </div>
    </div>
    <script type='text/javascript'>
        var kk_star_ratings = {
            "action": "kk-star-ratings",
            "endpoint": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "nonce": "ab5ffef0a5"
        };
    </script>
    <script type='text/javascript'>
        var DIVI = {
            "item_count": "%d Item",
            "items_count": "%d Items"
        };
        var et_shortcodes_strings = {
            "previous": "Previous",
            "next": "Next"
        };
        var et_pb_custom = {
            "ajaxurl": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/images",
            "builder_images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/includes\/builder\/images",
            "et_frontend_nonce": "fbc068d605",
            "subscription_failed": "Please, check the fields below to make sure you entered the correct information.",
            "et_ab_log_nonce": "43b4f535e7",
            "fill_message": "Please, fill in the following fields:",
            "contact_error_message": "Please, fix the following errors:",
            "invalid": "Invalid email",
            "captcha": "Captcha",
            "prev": "Prev",
            "previous": "Previous",
            "next": "Next",
            "wrong_captcha": "You entered the wrong number in captcha.",
            "ignore_waypoints": "no",
            "is_divi_theme_used": "1",
            "widget_search_selector": ".widget_search",
            "is_ab_testing_active": "",
            "page_id": "76",
            "unique_test_id": "",
            "ab_bounce_rate": "5",
            "is_cache_plugin_active": "no",
            "is_shortcode_tracking": "",
            "tinymce_uri": ""
        };
        var et_pb_box_shadow_elements = [];
    </script>
    <script type="application/ld+json">{ "@context": "http://schema.org", "@type": "LocalBusiness", "name": "Net B\u1ea3o Hi\u1ec3m", "description": "Website chuy\u00ean cung c\u1ea5p c\u00e1c th\u00f4ng tin li\u00ean quan \u0111\u1ebfn th\u1ecb tr\u01b0\u1eddng B\u1ea3o Hi\u1ec3m Nh\u00e2n Th\u1ecd. Ngo\u00e0i ra, website c\u00f2n t\u01b0 v\u1ea5n mi\u1ec5n ph\u00ed c\u00e1c g\u00f3i s\u1ea3n ph\u1ea9m BHNT t\u1ed1t nh\u1ea5t.", "image": "https://netbaohiem.com/wp-content/uploads/2015/04/logo-1-netbaohiem.png", "priceRange": "500.000-5.000.000", "address": { "@type": "PostalAddress", "addressLocality": "Ho Chi Minh city", "addressRegion": "\u0110\u00f4ng Nam B\u1ed9", "postalCode": "700000", "streetAddress": "L\u1ea7u 80, t\u00f2a nh\u00e0 Landmark 81, 720A \u0110i\u1ec7n Bi\u00ean Ph\u1ee7, Ph\u01b0\u1eddng 22, Qu\u1eadn B\u00ecnh Th\u1ea1nh, TP.HCM" }, "telephone": "+84981649545" }
    </script>
    <script type="text/javascript" defer src="{{asset('frontend/wp-content/cache/autoptimize/js/autoptimize_9a6017058e62156a066cd8dd84cd3fa9.js')}}"></script>
</body>
</html>
