<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="pingback" href="../xmlrpc.php" />
    <script>
        var et_site_url = '../index.html';
        var et_post_id = '2108';

        function et_core_page_resource_fallback(a, b) {
            "undefined" === typeof b && (b = a.sheet.cssRules && 0 === a.sheet.cssRules.length);
            b && (a.onerror = null, a.onload = null, a.href ? a.href = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id : a.src && (a.src = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id))
        }
    </script>
    <link type="text/css" media="all" href="{{asset('frontend/wp-content/cache/autoptimize/css/autoptimize_123690c73ba7d7d73ba06ddd913c3a77.css')}}" rel="stylesheet" />
    <title>@yield('title','Bài viết chi tiết')</title>
    <meta name="description" content="Rủi ro hoàn toàn có thể xảy ra với bất kỳ ai: dù bạn có giàu hay nghèo, tốt hay xấu...rủi ro luôn có khả năng xảy ra với bạn" />
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <link rel="canonical" href="index.html" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="7 Trường Hợp Thương Tâm Của Người Nổi Tiếng Trong Showbiz Việt" />
    <meta property="og:description" content="Rủi ro hoàn toàn có thể xảy ra với bất kỳ ai: dù bạn có giàu hay nghèo, tốt hay xấu...rủi ro luôn có khả năng xảy ra với bạn" />
    <meta property="og:url" content="https://netbaohiem.com/truong-hop-thuong-tam-cua-nguoi-noi-tieng-trong-showbiz-viet/" />
    <meta property="og:site_name" content="Net Bảo Hiểm" />
    <meta property="article:section" content="Bài viết" />
    <meta property="article:published_time" content="2019-12-01T17:10:26+00:00" />
    <meta property="article:modified_time" content="2019-12-01T17:56:36+00:00" />
    <meta property="og:updated_time" content="2019-12-01T17:56:36+00:00" />
    <meta property="og:image" content="https://netbaohiem.com/wp-content/uploads/2019/12/netbaohiem.com-wanbi-tuan-anh-min.jpg" />
    <meta property="og:image:secure_url" content="https://netbaohiem.com/wp-content/uploads/2019/12/netbaohiem.com-wanbi-tuan-anh-min.jpg" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Rủi ro hoàn toàn có thể xảy ra với bất kỳ ai: dù bạn có giàu hay nghèo, tốt hay xấu...rủi ro luôn có khả năng xảy ra với bạn" />
    <meta name="twitter:title" content="7 Trường Hợp Thương Tâm Của Người Nổi Tiếng Trong Showbiz Việt" />
    <meta name="twitter:image" content="https://netbaohiem.com/wp-content/uploads/2019/12/netbaohiem.com-wanbi-tuan-anh-min.jpg" />
    <script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://netbaohiem.com/#website","url":"https://netbaohiem.com/","name":"Net B\u1ea3o Hi\u1ec3m","description":"T\u01b0 V\u1ea5n Mi\u1ec5n Ph\u00ed B\u1ea3o Hi\u1ec3m Nh\u00e2n Th\u1ecd","potentialAction":{"@type":"SearchAction","target":"https://netbaohiem.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"ImageObject","@id":"https://netbaohiem.com/truong-hop-thuong-tam-cua-nguoi-noi-tieng-trong-showbiz-viet/#primaryimage","url":"https://netbaohiem.com/wp-content/uploads/2019/12/netbaohiem.com-wanbi-tuan-anh-min.jpg","width":450,"height":300,"caption":"netbaohiem.com wanbi tuan anh"},{"@type":"WebPage","@id":"https://netbaohiem.com/truong-hop-thuong-tam-cua-nguoi-noi-tieng-trong-showbiz-viet/#webpage","url":"https://netbaohiem.com/truong-hop-thuong-tam-cua-nguoi-noi-tieng-trong-showbiz-viet/","inLanguage":"en-US","name":"7 Tr\u01b0\u1eddng H\u1ee3p Th\u01b0\u01a1ng T\u00e2m C\u1ee7a Ng\u01b0\u1eddi N\u1ed5i Ti\u1ebfng Trong Showbiz Vi\u1ec7t","isPartOf":{"@id":"https://netbaohiem.com/#website"},"primaryImageOfPage":{"@id":"https://netbaohiem.com/truong-hop-thuong-tam-cua-nguoi-noi-tieng-trong-showbiz-viet/#primaryimage"},"datePublished":"2019-12-01T17:10:26+00:00","dateModified":"2019-12-01T17:56:36+00:00","author":{"@id":"https://netbaohiem.com/#/schema/person/f7a7a8f3ef3df57c291ec2d7495460a4"},"description":"R\u1ee7i ro ho\u00e0n to\u00e0n c\u00f3 th\u1ec3 x\u1ea3y ra v\u1edbi b\u1ea5t k\u1ef3 ai: d\u00f9 b\u1ea1n c\u00f3 gi\u00e0u hay ngh\u00e8o, t\u1ed1t hay x\u1ea5u...r\u1ee7i ro lu\u00f4n c\u00f3 kh\u1ea3 n\u0103ng x\u1ea3y ra v\u1edbi b\u1ea1n","breadcrumb":{"@id":"https://netbaohiem.com/truong-hop-thuong-tam-cua-nguoi-noi-tieng-trong-showbiz-viet/#breadcrumb"}},{"@type":"BreadcrumbList","@id":"https://netbaohiem.com/truong-hop-thuong-tam-cua-nguoi-noi-tieng-trong-showbiz-viet/#breadcrumb","itemListElement":[{"@type":"ListItem","position":1,"item":{"@type":"WebPage","@id":"https://netbaohiem.com/","url":"https://netbaohiem.com/","name":"Home"}},{"@type":"ListItem","position":2,"item":{"@type":"WebPage","@id":"https://netbaohiem.com/category/bai-viet/","url":"https://netbaohiem.com/category/bai-viet/","name":"B\u00e0i vi\u1ebft"}},{"@type":"ListItem","position":3,"item":{"@type":"WebPage","@id":"https://netbaohiem.com/truong-hop-thuong-tam-cua-nguoi-noi-tieng-trong-showbiz-viet/","url":"https://netbaohiem.com/truong-hop-thuong-tam-cua-nguoi-noi-tieng-trong-showbiz-viet/","name":"7 Tr\u01b0\u1eddng H\u1ee3p Th\u01b0\u01a1ng T\u00e2m C\u1ee7a Ng\u01b0\u1eddi N\u1ed5i Ti\u1ebfng Trong Showbiz Vi\u1ec7t"}}]},{"@type":["Person"],"@id":"https://netbaohiem.com/#/schema/person/f7a7a8f3ef3df57c291ec2d7495460a4","name":"NBH","image":{"@type":"ImageObject","@id":"https://netbaohiem.com/#authorlogo","url":"https://secure.gravatar.com/avatar/bdfe5e2162b79b6933ab01e10ca55f85?s=96&d=blank&r=g","caption":"NBH"},"sameAs":[]}]}</script>
    <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
    <link rel='dns-prefetch' href='http://s.w.org/' />
    <link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Feed" href="../feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Comments Feed" href="../comments/feed/index.html" />
    <meta content="Divi v.3.29.3" name="generator" />
    <link rel='stylesheet' id='et-builder-googlefonts-cached-css' href='https://fonts.googleapis.com/css?family=Lora%3Aregular%2Citalic%2C700%2C700italic%7CMerriweather%3A300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic&amp;ver=5.3#038;subset=cyrillic,vietnamese,latin,latin-ext,cyrillic-ext' type='text/css' media='all' />
    <script type='text/javascript' src='{{asset("frontend/wp-includes/js/jquery/jquery4a5f.js?ver=1.12.4-wp")}}'></script>
    <link rel='https://api.w.org/' href='../wp-json/index.html' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="../xmlrpc0db0.php?rsd" />
    <meta name="generator" content="WordPress 5.3" />
    <link rel='shortlink' href='../index8f7c.html?p=2108' />
    <link rel="alternate" type="application/json+oembed" href="../wp-json/oembed/1.0/embedcdbc.json?url=https%3A%2F%2Fnetbaohiem.com%2Ftruong-hop-thuong-tam-cua-nguoi-noi-tieng-trong-showbiz-viet%2F" />
    <link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embed9ec0?url=https%3A%2F%2Fnetbaohiem.com%2Ftruong-hop-thuong-tam-cua-nguoi-noi-tieng-trong-showbiz-viet%2F&amp;format=xml" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="shortcut icon" href="{{asset('frontend/wp-content/uploads/2015/05/favicon-1-netbaohiem.png')}}" />
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-147696565-1', 'auto');
        ga('send', 'pageview');
    </script>
    <link rel="stylesheet" id="et-divi-customizer-global-cached-inline-styles" href="{{asset('frontend/wp-content/cache/et/global/et-divi-customizer-global-1575216429521.min.css')}}" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />
</head>

<body data-rsssl=1 class="post-template-default single single-post postid-2108 single-format-standard et_pb_button_helper_class et_transparent_nav et_fixed_nav et_show_nav et_cover_background et_pb_gutter linux et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
    <div id="page-container">
        <header id="main-header" data-height-onload="67">
          @include('frontend.blocks.menu')
        </header>
        <div id="et-main-area">
            <div id="main-content">
                <div class="container">
                    <div id="content-area" class="clearfix">
                        <div id="left-area">
                            <article id="post-2108" class="et_pb_post post-2108 post type-post status-publish format-standard hentry category-bai-viet">
                                <div class="et_post_meta_wrapper">
                                    <h1 class="entry-title">{{$news_detail->title}}</h1></div>
                                <div class="entry-content">
                                    <p>{!! $news_detail->content !!}</p>

                                    <div style="display: none;" class="kk-star-ratings kksr-valign-bottom kksr-align-right " data-id="2108" data-slug="">
                                        <div class="kksr-stars">
                                            <div class="kksr-stars-inactive">
                                                <div class="kksr-star" data-star="1">
                                                    <div class="kksr-icon" style="width: 22px; height: 22px;"></div>
                                                </div>
                                                <div class="kksr-star" data-star="2">
                                                    <div class="kksr-icon" style="width: 22px; height: 22px;"></div>
                                                </div>
                                                <div class="kksr-star" data-star="3">
                                                    <div class="kksr-icon" style="width: 22px; height: 22px;"></div>
                                                </div>
                                                <div class="kksr-star" data-star="4">
                                                    <div class="kksr-icon" style="width: 22px; height: 22px;"></div>
                                                </div>
                                                <div class="kksr-star" data-star="5">
                                                    <div class="kksr-icon" style="width: 22px; height: 22px;"></div>
                                                </div>
                                            </div>
                                            <div class="kksr-stars-active" style="width: 0px;">
                                                <div class="kksr-star">
                                                    <div class="kksr-icon" style="width: 22px; height: 22px;"></div>
                                                </div>
                                                <div class="kksr-star">
                                                    <div class="kksr-icon" style="width: 22px; height: 22px;"></div>
                                                </div>
                                                <div class="kksr-star">
                                                    <div class="kksr-icon" style="width: 22px; height: 22px;"></div>
                                                </div>
                                                <div class="kksr-star">
                                                    <div class="kksr-icon" style="width: 22px; height: 22px;"></div>
                                                </div>
                                                <div class="kksr-star">
                                                    <div class="kksr-icon" style="width: 22px; height: 22px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kksr-legend"> <span class="kksr-muted"></span></div>
                                    </div>
                                </div>
                                <div class="et_post_meta_wrapper"></div>
                            </article>
                        </div>
                        <div id="sidebar">
                            <div id="search-2" class="et_pb_widget widget_search">
                                <h4 class="widgettitle">Tìm kiếm bài viết</h4>
                                <form role="search" method="POST" id="searchform" class="searchform" action="{{url('tim-kiem')}}">
                                     @csrf
                                    <div>
                                        <label class="screen-reader-text" for="s">Search for:</label>
                                        <input type="text" value="" name="tukhoa" id="s" />
                                        <input type="submit" id="searchsubmit" value="Search" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <span class="et_pb_scroll_top et-pb-icon"></span>
           @include('frontend.blocks.footer')
        </div>
    </div>
    <script type='text/javascript'>
        var kk_star_ratings = {
            "action": "kk-star-ratings",
            "endpoint": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "nonce": "d1f43d2b8a"
        };
    </script>
    <script type='text/javascript'>
        var DIVI = {
            "item_count": "%d Item",
            "items_count": "%d Items"
        };
        var et_shortcodes_strings = {
            "previous": "Previous",
            "next": "Next"
        };
        var et_pb_custom = {
            "ajaxurl": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php",
            "images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/images",
            "builder_images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/includes\/builder\/images",
            "et_frontend_nonce": "6677073e6b",
            "subscription_failed": "Please, check the fields below to make sure you entered the correct information.",
            "et_ab_log_nonce": "0014a96523",
            "fill_message": "Please, fill in the following fields:",
            "contact_error_message": "Please, fix the following errors:",
            "invalid": "Invalid email",
            "captcha": "Captcha",
            "prev": "Prev",
            "previous": "Previous",
            "next": "Next",
            "wrong_captcha": "You entered the wrong number in captcha.",
            "ignore_waypoints": "no",
            "is_divi_theme_used": "1",
            "widget_search_selector": ".widget_search",
            "is_ab_testing_active": "",
            "page_id": "2108",
            "unique_test_id": "",
            "ab_bounce_rate": "5",
            "is_cache_plugin_active": "no",
            "is_shortcode_tracking": "",
            "tinymce_uri": ""
        };
        var et_pb_box_shadow_elements = [];
    </script>
    <script type="text/javascript" defer src="{{asset('frontend/wp-content/cache/autoptimize/js/autoptimize_490080239d4ee600f5a141052b340c7b.js')}}"></script>
</body>
</html>
