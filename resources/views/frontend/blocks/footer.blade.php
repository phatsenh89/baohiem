 <footer id="main-footer">
        <div class="container">
            <div id="footer-widgets" class="clearfix">
                <div class="footer-widget">
                    <div id="text-13" class="fwidget et_pb_widget widget_text">
                        <h4 class="title">Hotline:</h4>
                        <div class="textwidget">
                            <p>+84981649545</p>
                        </div>
                    </div>
                </div>
                <div class="footer-widget">
                    <div id="text-12" class="fwidget et_pb_widget widget_text">
                        <h4 class="title">Địa chỉ:</h4>
                        <div class="textwidget">
                            <p>Lầu 80, tòa nhà Landmark 81, 720A Điện Biên Phủ, Phường 22, Quận Bình Thạnh, TP.HCM</p>
                        </div>
                    </div>
                </div>
                <div class="footer-widget">
                    <div id="text-14" class="fwidget et_pb_widget widget_text">
                        <h4 class="title">Email:</h4>
                        <div class="textwidget">
                            <p>support@netbaohiem.com</p>
                        </div>
                    </div>
                </div>
                <div class="footer-widget">
                    <div id="text-15" class="fwidget et_pb_widget widget_text">
                        <h4 class="title">Thời gian làm việc:</h4>
                        <div class="textwidget">
                            <p><?php echo getCauhinh('thoigianlamviec'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="et-footer-nav">
            <div class="container">
                <ul id="menu-footer-menu" class="bottom-nav">
                    <li id="menu-item-869" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-869"><a href="{{route('gioi-thieu')}}">Giới thiệu</a></li>
                    <li id="menu-item-870" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-870"><a href="{{route('lien-he')}}">Liên hệ</a></li>
                    <li id="menu-item-880" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-880"><a href="{{route('dieu-khoan-dich-vu')}}">Điều khoản dịch vụ</a></li>
                    <li id="menu-item-887" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-887"><a href="{{route('chinh-sach-bao-mat')}}">Chính sách bảo mật</a></li>
                </ul>
            </div>
        </div>
        <div id="footer-bottom">
            <div class="container clearfix">
                <p id="footer-info">&copy;2019 Design by  <a href="https://cloudone.vn" target="_blank">CloudOne</a></p>
            </div>
        </div>
    </footer>