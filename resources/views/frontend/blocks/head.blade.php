<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="pingback" href="xmlrpc.php" />
<script>
    var et_site_url = 'index.html';
    var et_post_id = '69';

    function et_core_page_resource_fallback(a, b) {
        "undefined" === typeof b && (b = a.sheet.cssRules && 0 === a.sheet.cssRules.length);
        b && (a.onerror = null, a.onload = null, a.href ? a.href = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id : a.src && (a.src = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id))
    }
</script>
<link type="text/css" media="all" href="{{asset('frontend/wp-content/cache/autoptimize/css/autoptimize_123690c73ba7d7d73ba06ddd913c3a77.css')}}" rel="stylesheet" />
<title>@yield('title','Net Bảo Hiểm - Tư vấn miễn phí mua bảo hiểm nhân thọ tốt nhất hiện nay')</title>
{{-- <meta name="description" content="Tư vấn từ A-Z các vấn đề liên quan đến mua bảo hiểm nhân thọ. Các câu hỏi thường gặp như: mua gói nào, phí đóng bao nhiêu, cách tham gia, cách đóng phí..." />
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
<link rel="canonical" href="index.html" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Net Bảo Hiểm - Tư vấn miễn phí mua bảo hiểm nhân thọ tốt nhất hiện nay" />
<meta property="og:description" content="Tư vấn từ A-Z các vấn đề liên quan đến mua bảo hiểm nhân thọ. Các câu hỏi thường gặp như: mua gói nào, phí đóng bao nhiêu, cách tham gia, cách đóng phí..." />
<meta property="og:url" content="https://netbaohiem.com/" />
<meta property="og:site_name" content="Net Bảo Hiểm" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="Tư vấn từ A-Z các vấn đề liên quan đến mua bảo hiểm nhân thọ. Các câu hỏi thường gặp như: mua gói nào, phí đóng bao nhiêu, cách tham gia, cách đóng phí..." />
<meta name="twitter:title" content="Net Bảo Hiểm - Tư vấn miễn phí mua bảo hiểm nhân thọ tốt nhất hiện nay" />
<meta name="p:domain_verify" content="3f7fc8dee83cf5f137f83146bc10e3ac" /> --}}
	   {{-- <script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://netbaohiem.com/#website","url":"https://netbaohiem.com/","name":"Net B\u1ea3o Hi\u1ec3m","description":"T\u01b0 V\u1ea5n Mi\u1ec5n Ph\u00ed B\u1ea3o Hi\u1ec3m Nh\u00e2n Th\u1ecd","potentialAction":{"@type":"SearchAction","target":"https://netbaohiem.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://netbaohiem.com/#webpage","url":"https://netbaohiem.com/","inLanguage":"en-US","name":"Net B\u1ea3o Hi\u1ec3m - T\u01b0 v\u1ea5n mi\u1ec5n ph\u00ed mua b\u1ea3o hi\u1ec3m nh\u00e2n th\u1ecd t\u1ed1t nh\u1ea5t hi\u1ec7n nay","isPartOf":{"@id":"https://netbaohiem.com/#website"},"datePublished":"2015-04-24T15:46:45+00:00","dateModified":"2019-09-21T04:01:39+00:00","description":"T\u01b0 v\u1ea5n t\u1eeb A-Z c\u00e1c v\u1ea5n \u0111\u1ec1 li\u00ean quan \u0111\u1ebfn mua b\u1ea3o hi\u1ec3m nh\u00e2n th\u1ecd. C\u00e1c c\u00e2u h\u1ecfi th\u01b0\u1eddng g\u1eb7p nh\u01b0: mua g\u00f3i n\u00e0o, ph\u00ed \u0111\u00f3ng bao nhi\u00eau, c\u00e1ch tham gia, c\u00e1ch \u0111\u00f3ng ph\u00ed..."}]}
    </script>  --}}{{-- 
<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />
<link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Feed" href="feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Comments Feed" href="comments/feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="Net Bảo Hiểm &raquo; Trang chủ Comments Feed" href="trang-chu/feed/index.html" /> --}}
<meta content="Divi v.3.29.3" name="generator" />
<link rel='stylesheet' id='et-builder-googlefonts-cached-css' href='https://fonts.googleapis.com/css?family=Lora%3Aregular%2Citalic%2C700%2C700italic%7CMerriweather%3A300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic%7COpen+Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&amp;ver=5.3#038;subset=cyrillic,vietnamese,latin,latin-ext,cyrillic-ext,greek,greek-ext' type='text/css' media='all' />
<script type='text/javascript' src='{{asset('frontend/wp-includes/js/jquery/jquery4a5f.js?ver=1.12.4-wp')}}'></script>
{{-- <link rel='https://api.w.org/' href='wp-json/index.html' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
<meta name="generator" content="WordPress 5.3" />
<link rel='shortlink' href='index.html' /> --}}
<link rel="alternate" type="application/json+oembed" href="{{asset('frontend/wp-json/oembed/1.0/embed24d3.json?url=https%3A%2F%2Fnetbaohiem.com%2F')}}" />
{{-- <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embede2ce?url=https%3A%2F%2Fnetbaohiem.com%2F&amp;format=xml" /> --}}
{{-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" /> --}}
<link rel="shortcut icon" href="{{asset('frontend/wp-content/uploads/2015/05/favicon-1-netbaohiem.png')}}" />
	<script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-147696565-1', 'auto');
        ga('send', 'pageview');
    </script> 
	<link rel="stylesheet" id="et-core-unified-cached-inline-styles" href="{{asset('frontend/wp-content/cache/et/69/et-core-unified-15752155197663.min.css')}}" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" /> 
     <link rel="stylesheet" id="et-divi-customizer-global-cached-inline-styles" href="{{asset('frontend/wp-content/cache/et/global/et-divi-customizer-global-1575216429521.min.css')}}" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />
       <link rel="stylesheet" id="et-core-unified-cached-inline-styles" href="{{asset('frontend/wp-content/cache/et/73/et-core-unified-15752158660702.min.css')}}" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />

   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
 <script >
    $(document).ready(function() {
         $('#flash-message').delay(1000).slideUp(300);
      });
</script> 