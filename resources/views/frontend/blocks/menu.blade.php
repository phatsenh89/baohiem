<div class="container clearfix et_menu_container">
        <div class="logo_container">
            <span class="logo_helper"></span>
            <a href="{{route('home')}}"> 
                <img alt="Net Bảo Hiểm" id="logo" data-height-percentage="54" data-src="https://netbaohiem.com/wp-content/uploads/2015/04/logo-1-netbaohiem.png" class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" />
                <noscript>
                    <img src="{{asset('frontend/wp-content/uploads/2015/04/logo-1-netbaohiem.png')}}" alt="Net Bảo Hiểm" id="logo" data-height-percentage="54" />
                </noscript>
            </a>
        </div>
        <div id="et-top-navigation" data-height="67" data-fixed-height="40">
            <nav id="top-menu-nav">
                <ul id="top-menu" class="nav">
                    <li id="menu-item-80" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-69 current_page_item menu-item-80"><a href="{{route('home')}}" aria-current="page">Trang chủ</a></li>
                    <li id="menu-item-376" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-376"><a href="{{route('bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh')}}">BHNT cho trẻ nhỏ</a></li>
                    <li id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a href="{{route('tu-van-mien-phi')}}">Tư vấn miễn phí</a></li>
                    <li id="menu-item-81" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-81"><a href="{{route('bai-viet')}}">Bài viết</a></li>
                </ul>
            </nav>
            <div id="et_mobile_nav_menu">
                <div class="mobile_nav closed"> <span class="select_page">Select Page</span> <span class="mobile_menu_bar mobile_menu_bar_toggle"></span></div>
            </div>
        </div>
    </div>