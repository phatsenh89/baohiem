<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
   @include('frontend.blocks.head')
</head>
<body data-rsssl=1 class="home page-template-default page page-id-69 et_pb_button_helper_class et_transparent_nav et_fixed_nav et_show_nav et_cover_background et_pb_side_nav_page et_pb_gutter et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_pb_pagebuilder_layout et_full_width_page et_divi_theme et-db et_minified_js et_minified_css">
	<div id="page-container">
		<header id="main-header" data-height-onload="67">
            @include('frontend.blocks.menu')
            <div class="et_search_outer">
                <div class="container et_search_form_container">
                    <form role="search" method="get" class="et-search-form" action="https://netbaohiem.com/">
                        <input type="search" class="et-search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:" />
                    </form> <span class="et_close_search_field"></span></div>
            </div>
        </header>
            <div id="et-main-area">
                <div id="main-content">
                    @yield('content')
                </div> 
               
    @include('frontend.blocks.footer')
            </div>
    </div>
<script type='text/javascript'>var kk_star_ratings= {
     "action": "kk-star-ratings", "endpoint": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php", "nonce": "d1f43d2b8a"
 };
 </script> <script type='text/javascript'>var DIVI= {
     "item_count": "%d Item", "items_count": "%d Items"
 };
 var et_shortcodes_strings= {
     "previous": "Previous", "next": "Next"
 };
 var et_pb_custom= {
     "ajaxurl": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php", "images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/images", "builder_images_uri": "https:\/\/netbaohiem.com\/wp-content\/themes\/Divi\/includes\/builder\/images", "et_frontend_nonce": "6677073e6b", "subscription_failed": "Please, check the fields below to make sure you entered the correct information.", "et_ab_log_nonce": "0014a96523", "fill_message": "Please, fill in the following fields:", "contact_error_message": "Please, fix the following errors:", "invalid": "Invalid email", "captcha": "Captcha", "prev": "Prev", "previous": "Previous", "next": "Next", "wrong_captcha": "You entered the wrong number in captcha.", "ignore_waypoints": "no", "is_divi_theme_used": "1", "widget_search_selector": ".widget_search", "is_ab_testing_active": "", "page_id": "69", "unique_test_id": "", "ab_bounce_rate": "5", "is_cache_plugin_active": "no", "is_shortcode_tracking": "", "tinymce_uri": ""
 };
 var et_pb_box_shadow_elements=[];
 </script> 
 <script type='text/javascript'>var frm_js= {
     "ajax_url": "https:\/\/netbaohiem.com\/wp-admin\/admin-ajax.php", "images_url": "https:\/\/netbaohiem.com\/wp-content\/plugins\/formidable\/images", "loading": "Loading\u2026", "remove": "Remove", "offset": "4", "nonce": "972b711fa5", "id": "ID", "no_results": "No results match", "file_spam": "That file looks like Spam.", "calc_error": "There is an error in the calculation in the field with key", "empty_fields": "Please complete the preceding required fields before uploading a file."
 };
 </script> 
 <script type="application/ld+json"> {
     "@context": "http://schema.org/",
     "@type": "WebSite",
     "url": "https://netbaohiem.com",
     "potentialAction": {
         "@type": "SearchAction",
         "target": "https://netbaohiem.com/?s={query}",
         "query-input": "required name=query"
     }
 }
 
 </script>
 <script type="application/ld+json"> {
     "@context": "http://schema.org",
     "@type": "InsuranceAgency",
     "image": "https://netbaohiem.com/wp-content/uploads/2015/04/mua-bao-hiem-nhan-tho.jpg",
     "priceRange": "500.000-5.000.000",
     "telephone": "+84981649545",
     "name": "Net B\u1ea3o Hi\u1ec3m",
     "logo": "https://netbaohiem.com/wp-content/uploads/2015/05/logo-netbaohiem.com_.jpg",
     "description": "Website chuy\u00ean cung c\u1ea5p c\u00e1c th\u00f4ng tin li\u00ean quan \u0111\u1ebfn th\u1ecb tr\u01b0\u1eddng B\u1ea3o Hi\u1ec3m Nh\u00e2n Th\u1ecd. Ngo\u00e0i ra, website c\u00f2n t\u01b0 v\u1ea5n mi\u1ec5n ph\u00ed c\u00e1c g\u00f3i s\u1ea3n ph\u1ea9m BHNT t\u1ed1t nh\u1ea5t.",
     "openingHours": [ "Mo-Su 8:00-20:00",
     ""],
     "geo": {
         "@type": "GeoCoordinates", "latitude": null, "longitude": null
     }
     ,
     "url": "https://netbaohiem.com",
     "sameAs": [ "https://youtu.be/Hb1CigsNYcY"],
     "contactPoint": {
         "@type": "ContactPoint", "telephone": "+84981649545", "contactType": "customer service", "email": "support@netbaohiem.com", "contactOption": "TollFree", "areaServed": [ "VN"], "availableLanguage": [ "Vietnamese"]
     }
     ,
     "address": {
         "@type": "PostalAddress", "addressCountry": "Viet Nam", "addressLocality": "Ho Chi Minh city", "addressRegion": "\u0110\u00f4ng Nam B\u1ed9", "postalCode": "700000", "streetAddress": "Landmark 81, 720A \u0110i\u1ec7n Bi\u00ean Ph\u1ee7, Ph\u01b0\u1eddng 22, Qu\u1eadn B\u00ecnh Th\u1ea1nh"
     }
 }
 
 </script> 

 <script type="text/javascript" defer src="{{asset('frontend/wp-content/cache/autoptimize/js/autoptimize_f741c9bed8a613d2aa902ea18248b4f3.js')}}"></script>
</body> 
</html>
