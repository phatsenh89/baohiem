<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Lienhe extends Model
{
    public $timestamps = true;
    protected $table = 'bh_lienhe';

    public function dbTable()
    {
        return DB::table('bh_lienhe');
    }
    public function ListLienhe()
    {
    	return $this->dbTable()->paginate(10);
    }
    public function storeLienhe($data)
    {
    	return $this->dbTable()->insert($data);
    }
    public function editLienhe($id)
    {
    	return $this->dbTable()->find($id);
    }
    public function updateLienhe($data,$id)
    {
    	return $this->dbTable()->whereId($id)->update($data);
    }
    public function delLienhe($data)
    {
        return $this->dbTable()->delete($data);
    }
}
