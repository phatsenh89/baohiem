<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Menu extends Model
{
    public $timestamps = true;
    protected $table = 'bh_menu';

    public function dbTable()
    {
        return DB::table('bh_menu');
    }
    public function ListMenu()
    {
    	return $this->dbTable()->paginate(10);
    }
    public function storeMenu($data)
    {
    	return $this->dbTable()->insert($data);
    }
    public function editMenu($id)
    {
    	return $this->dbTable()->find($id);
    }
    public function updateMenu($data,$id)
    {
    	return $this->dbTable()->whereId($id)->update($data);
    }
    public function delMenu($data)
    {
        return $this->dbTable()->delete($data);
    }
}
