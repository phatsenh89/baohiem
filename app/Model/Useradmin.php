<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Useradmin extends Model
{
    public $timestamps = true;
    protected $table = 'bh_admin';

    public function dbTable()
    {
        return DB::table('bh_admin');
    }
    public function ListAdmin()
    {
    	return $this->dbTable()
                    ->paginate(10);
    }
    public function storeAdmin($data)
    {
    	return $this->dbTable()
    				->insert($data);
    }
    public function getUser($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function updateUser($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    
    public function getUserAdmin()
    {
        return $this->dbTable()
                    ->get()
                    ->toArray();
    }

     public function delAdmin($id)
    {
        $check = $this->getUser($id);
        if($check->role != 0){
            $this->dbTable()->delete($id);
            return true;
        } else {
            return false;
        }
    }

    public function getAdminEmail($email)
    {
        return $this->dbTable()
                    ->where('email',$email)
                    ->first();
    }
    public function updataAdminEmailToken($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function checkUpdataAdminEmailPass($token)
    {
        return $this->dbTable()
                    ->where(
                      
                        'token',$token
                    )
                    ->first();
    }
    public function updataAdminEmailPass($data,$token)
    {
        return $this->dbTable()
                    ->where(
                       
                        'token',$token
                    )
                    ->update($data);
    }
}
