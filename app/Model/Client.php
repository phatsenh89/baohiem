<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Client extends Model
{
    public $timestamps = true;
    protected $table = 'bh_client';

    public function dbTable()
    {
        return DB::table('bh_client');
    }
    public function ListClient()
    {
    	return $this->dbTable()->paginate(10);
    }
    public function storeClient($data)
    {
    	return $this->dbTable()->insert($data);
    }
    public function editClient($id)
    {
    	return $this->dbTable()->find($id);
    }
    public function updateClient($data,$id)
    {
    	return $this->dbTable()->whereId($id)->update($data);
    }
    public function delClient($data)
    {
        return $this->dbTable()->delete($data);
    }
}
