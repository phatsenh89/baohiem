<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Cauhinh extends Model
{
    public $timestamps = true;
    protected $table = 'bh_config';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('bh_config');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->get()
                    ->toArray();
    }
    public function updateData($name,$data)
    {
        return $this->dbTable()
                    ->whereId($name)
                    ->update($data);
    }
    public function getData($name)
    {
        return $this->dbTable()
                    ->select('chitiet')
                    ->whereId($name)
                    ->get()
                    ->first();
    }
    public function destroyImg($name)
    {
        $path = 'image/';
        $filename = $path.$name;
        if(!empty($name))
        {
                $data = File::delete($filename);
        }
    }

     public function findName($id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->first();
    }
}
