<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class FAQ extends Model
{
    public $timestamps = true;
    protected $table = 'bh_faq';

    public function dbTable()
    {
        return DB::table('bh_faq');
    }
    public function ListFAQ()
    {
    	return $this->dbTable()->paginate(10);
    }
    public function storeFAQ($data)
    {
    	return $this->dbTable()->insert($data);
    }
    public function editFAQ($id)
    {
    	return $this->dbTable()->find($id);
    }
    public function updateFAQ($data,$id)
    {
    	return $this->dbTable()->whereId($id)->update($data);
    }
    public function delFAQ($data)
    {
        return $this->dbTable()->delete($data);
    }
}
