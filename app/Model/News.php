<?php

namespace App\Model;
use DB;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $timestamps = true;
    protected $table = 'bh_tintuc';

    public function dbTable()
    {
        return DB::table('bh_tintuc');
    }
    public function ListNews()
    {
    	return $this->dbTable()->paginate(10);
    }
    public function storeNews($data)
    {
    	return $this->dbTable()->insert($data);
    }
    public function editNews($id)
    {
    	return $this->dbTable()->find($id);
    }
    public function updateNews($data,$id)
    {
    	return $this->dbTable()->whereId($id)->update($data);
    }
    public function delNews($data)
    {
        return $this->dbTable()->delete($data);
    }
    //
    //
    //
    public function GetNewsList()
    {
        return $this->dbTable()->where('status',1)->get()->toArray();
    }
}
