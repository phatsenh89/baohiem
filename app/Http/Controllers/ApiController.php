<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart,Auth,DataTime;
use App\Model\Cauhinh;
use App\Model\Client;

class ApiController extends Controller
{
    public function change_status_news(Request $request)
    {
        
        $id = $request->id;
        $status = $request->status =='true' ? 1 : 0;
        // $msg = $request->status;
        $data = DB::table('bh_tintuc')->where('id',$id)->limit(1);
        if(is_null($data)) $msg = 'ID không hợp lệ hoặc không tồn tại trong cơ sở dữ liệu';
        else {
            DB::table('bh_tintuc')->where('id',$id)->update(['status'=>$status]);
            $msg = 'Cập nhật trạng thái thành công!!!';
        }
        return response()->json(array('msg'=>$msg), 200);
    }
}
