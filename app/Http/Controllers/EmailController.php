<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\SMTP;
// use PHPMailer\PHPMailer\Exception;
use Mail;
class EmailController extends Controller
{
    public function sendMail($arr)
    {
        // $mail = new PHPMailer(true);                              
        try {
            Mail::send([], [], function($message) use ($arr){
                $message->subject($arr['subject']);
                $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                $message->to($arr['req']);
                $message->setBody($arr['body'], 'text/html');
            });
            return 1;
        } catch (Exception $e) {
            return "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }

    }
}
