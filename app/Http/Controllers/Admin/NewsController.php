<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNews;
use App\Model\News;
use DateTime,DB;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $news;
    public function __construct(News $news)
    {
        $this->news = $news;
    }
    public function index()
    {
        $data['news'] = $this->news->ListNews();

        return view('admin.module.news.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.module.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNews $req)
    {
        try {
            $data  = array(
                'title' => $req->title,
                'slug'      => (empty($req->slug))? str_slug($req->title,"-"):$req->slug,
                'intro' => $req->intro, 
                'content' => $req->content,
                'status' => $req->status,
                'created_at'    => new DateTime,
            );
        $this->news->storeNews($data);
        } catch (Exception $e) {
            return back()->with('alerterr','Wrong format');
        }
            return redirect()->back()->with('alertsuc','Thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['news'] = $this->news->editNews($id);

        return view('admin.module.news.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        try {
            $data  = array(
                'title' => $req->title,
                'slug'      => (empty($req->slug))? str_slug($req->name,"-"):$req->slug,
                'intro' => $req->intro, 
                'content' => $req->content,
                'status' => $req->status,
                'updated_at'    => new DateTime,
            );
        $this->news->updateNews($data,$id);
        } catch (Exception $e) {
            return back()->with('alerterr','Wrong format');
        }
            return redirect()->back()->with('alertsuc','Thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        $data = $req->input('checked',[]);

        for ($i=0; $i < count($data) ; $i++) { 
             $this->news->delNews($data[$i]);
        }
        return redirect()->route('news.index')->with('alertsuc','Xóa thành công');
    }
}
