<?php

namespace App\Http\Controllers\Admin;

// use Illuminate\Http\Request;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Http\Controllers\EmailController;
use App\Model\Useradmin;
use App\Model\Cauhinh;
use Carbon\Carbon;

class ResetPasswordController extends Controller
{
	private $useradmin;
    private $cauhinh;
    private $email;

    public function __construct(Useradmin $useradmin, Cauhinh $cauhinh, EmailController $email)
    {
        $this->useradmin = $useradmin;
        $this->cauhinh   = $cauhinh;
        $this->email     = $email;
    }
	public function getPassword()
    {
        return view('admin.auth.admin.admin-resetpassword');
    }
    public function sendEmail (Request $req) 
    {
  	
        if (isset($_POST['sendMailAdmin']))
        {
            $email = $this->useradmin->getAdminEmail($req->email);
            // dd($email);
            if(isset($email))
            {
                $token = bcrypt(Carbon::now('Asia/Ho_Chi_Minh').$email->email);
                
                $data  = array(
                            'token' => $token,
                        );

                $baseUrl        = $this->cauhinh->findName('linkwebsite')->chitiet;
                $body_gmail     = "<a href='".$baseUrl."change-new-password?token=".$token." '>Click here if you want to recover your password.</a>";
                $arr            = [];
                $arr['req']     = $req->email;
                $arr['subject'] = 'Reset Password';
                $arr['body']    = $body_gmail;
                
                $res = $this->email->sendMail($arr);
                if($res == 1)
                {

                    $this->useradmin->updataAdminEmailToken($data,$email->id);
                }else{
                    return redirect()->back()->with('alerterr','Send failed'); 
                }
                return redirect()->back()->with('alertsuc','Please check your email');
                   

            }
            else
            {
                return redirect()->back()->with('alerterr','No email exists '.$req->email);
            }
                
        }
  	}
  	public function getNewPassword()
    {
		   return view('admin.auth.admin.admin-newpassword');        
	}


	public function postNewPassword(Request $req)
    {
        if (isset($_POST['updatePassAdmin']))
        {
        	
        	$token = $req->token;
            // dd($token);
        	$check = $this->useradmin->checkUpdataAdminEmailPass($token);
            // dd($check);
        	if(isset($check))
        	{
                if($req->renewpassword == $req->newpassword)
                {
    	        	$data = array(
    	        						'password' => bcrypt($req->newpassword),
    	        						'token' => Null,
    	        					);
    	        	$this->useradmin->updataAdminEmailPass($data,$token);
    	        	return redirect()->route('admin.getLogin')->with('alertsuc',"Thay đổi mật khẩu thành công");
                }
                else
                {

                    return redirect()->back()->with('alerterr',"Mật khẩu nhập lại không chính xác");
                }

        	}
        	else
        	{
        		return redirect()->back()->with('alerterr',"Không thể thay đổi mật khẩu. Xin vui lòng gửi lại email");
        	}
        }
        else
        {
        	return redirect()->back()->with('alerterr','Lỗi');
        }
	}

}
