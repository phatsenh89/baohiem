<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
   
    public function getLogin()
    {
        return view('admin.login');
    }
    public function postLogin(Request $request)
    {

        if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'status' => 1])) {
         
            return redirect()->route('dashboard');

        } else {

          return redirect()->back()->with('alerterr',"Your password is incorrect");
        }
    }
    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('getLogin');
    }
}
