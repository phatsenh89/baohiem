<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Lienhe;
use DateTime;

class LienheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $lienhe;
    public function __construct(Lienhe $lienhe)
    {
        $this->lienhe = $lienhe;
    }
    public function index()
    {
        $data['lienhe'] = $this->lienhe->ListLienhe();

        return view('admin.module.lienhe.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.module.lienhe.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['lienhe'] = $this->lienhe->editLienhe($id);

        return view('admin.module.lienhe.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
         try {
             $data = array(
                        'name' => $req->name,
                        'phone' => $req->phone,
                        'message' => $req->message,
                        'updated_at'    => new DateTime,
                    ); 
           
            $this->lienhe->updateLienhe($data,$id);
            
        } catch (Exception $e) {
            
            return back()->with('alerterr','Wrong format');
        }
            return redirect()->back()->with('alertsuc','Thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        $data = $req->input('checked',[]);

        for ($i=0; $i < count($data) ; $i++) { 
             $this->lienhe->delLienhe($data[$i]);
        }
        return redirect()->route('lienhe.index')->with('alertsuc','Xóa thành công');
    }
}
