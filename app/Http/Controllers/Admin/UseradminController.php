<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Useradmins\StoreRequest;
use App\Http\Controllers\Controller;
use App\Model\Useradmin;
use Input;
use DB,Hash,Auth;
use DateTime;

class UseradminController extends Controller
{
    private $useradmin;


    public function __construct(Useradmin $useradmin)
    {
        $this->useradmin          = $useradmin;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Getchangepass()
    {
        return view('admin.module.useradmin.user-changepass');
    }
     public function Postchangepass(Request $request)
    {
        if (!(Hash::check($request->currentpassword, Auth::user()->password))) {
            // Mật khẩu trùng khớp
            return redirect()->back()->with("error","Mật khẩu hiện tại của bạn không khớp với mật khẩu bạn cung cấp. Vui lòng thử lại.");
        }
        if($request->newpassword != $request->newpassword_confirmation) {
            //Mật khẩu hiện tại và mật khẩu mới giống nhau
            return redirect()->back()->with("error","Mật khẩu mới không thể giống như mật khẩu hiện tại của bạn. Vui lòng chọn một mật khẩu khác nhau.");
        }

        $validatedData = $request->validate([
            'currentpassword' => 'required',
            'newpassword' => 'required|min:6|confirmed',
        ]);
        // Đổi mật khẩu
        
        $data =  array('password' =>bcrypt($request->newpassword));
         
         // dd($data);
        
         DB::table('bh_admin')->whereId(Auth::user()->id)->update($data);

         echo "<script>
                alert('Mật khẩu của bạn đã được thay đổi thành công.');
                window.location = '".url('/admin/dashboard')."'
              </script>";
    }

    public function index()
    {
        $data['useradmin']   = $this->useradmin->ListAdmin();
        return view('admin.module.useradmin.user-list',$data);    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.module.useradmin.user-create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $req)
    {
        try {
            $data = array(
                        'username' => $req->username,
                        'password' => bcrypt($req->password),
                        'role' => $req->role,
                        'fullname' => $req->fullname,
                        'email' =>$req->email,
                        'created_at'    => new DateTime,
                    ); 
                
        $this->useradmin->storeAdmin($data);
        } catch (Exception $e) {
            return back()->with('alerterr','Wrong format');
        }
        
            return redirect()->back()->with('alertsuc','Thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['useradmin'] = $this->useradmin->getUser($id);
        return view('admin.module.useradmin.user-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        try {
            $data = array(
                        'role' => $req->role,
                        'fullname' => $req->fullname,
                        'username'  => $req->username,
                        'email' =>$req->email,
                        'password' => bcrypt($req->password),
                        'updated_at'    => new DateTime,
                    ); 
                
        $this->useradmin->updateUser($data,$id);
        } catch (Exception $e) {
             return back()->with('alerterr','Wrong format');
        }
            return redirect()->back()->with('alertsuc','Thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $result = $this->useradmin->delAdmin($id);

        if($result){
            return redirect()->route('useradmin.index')->with('alertsuc','Xóa user thành công');
         } else{
             return redirect()->route('useradmin.index')->with('alerterr','Vui lòng không xóa admin tổng');
         }    
    }
}
