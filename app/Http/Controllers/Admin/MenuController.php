<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMenu;
use App\Model\Menu;
use Input,Auth;
use DateTime,DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $menu;
    public function __construct(Menu $menu)
    {
        $this->menu = $menu;
    }
    public function index()
    {
        $data['menu'] = $this->menu->ListMenu();

        return view('admin.module.menu.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view('admin.module.menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMenu $req)
    {
         try {
             $data = array(
                        'name' => $req->name,
                        'slug'      => (empty($req->slug))? str_slug($req->name,"-"):$req->slug,
                        'created_at'    => new DateTime,
                    ); 
                
            $this->menu->storeMenu($data);
            
        } catch (Exception $e) {
            
            return back()->with('alerterr','Wrong format');
        }
            return redirect()->back()->with('alertsuc','Thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = $this->menu->editMenu($id);

        return view('admin.module.menu.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        try {
             $data = array(
                        'name' => $req->name,
                        'slug'      => (empty($req->slug))? str_slug($req->name,"-"):$req->slug,
                        'updated_at'    => new DateTime,
                    ); 
                
            $this->menu->updateMenu($data,$id);
            
        } catch (Exception $e) {
            return back()->with('alerterr','Wrong format');
        }
            return redirect()->back()->with('alertsuc','Thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        $data = $req->input('checked',[]);

        for ($i=0; $i < count($data) ; $i++) { 
             $this->menu->delMenu($data[$i]);
        }
        return redirect()->route('menu.index')->with('alertsuc','Xóa thành công');
    }
}
