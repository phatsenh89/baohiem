<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cauhinh;
use Illuminate\Support\Facades\Auth;
use DB;
use Input;
use File;;

class CauhinhController extends Controller
{
    private $cauhinh;

    public function __construct(
                                Cauhinh $cauhinh
                                )
    {
        $this->cauhinh          = $cauhinh;
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {

    }
    public function laydulieu(){
        $name = [
                    'email',
                    'title',
                    'dienthoai',
                    'tenwebsite',
                    'linkwebsite',
                    'googlemap',
                    'lienhe',
                    'gioithieu',
                    'footer',
                    'faceBook',
                    'youtube',
                    'instagram',
                    'vipmoney',
                    'email_gmail',
                    'password_gmail',
                    'dieukhoandichvu',
                    'chinhsachbaomat',
                    'thoigianlamviec',
                ];
        $arrData = [];
        // dd($arrData[0]->chitiet);
        for($i = 0 ; $i<count($name) ; $i++)
        {
            $data = $this->cauhinh->getData($name[$i]);
            array_push($arrData, $data);
        }
        return $arrData; 
    }

    public function editNoId()
    {

        $data['cauhinh'] = $this->laydulieu();
        // dd($data['cauhinh'][0]->chitiet);
        return view('admin.module.cauhinhs.cauhinh-update',$data);
    }

    public function updateNoId(Request $req)
    {
        $filename = "";
        if(is_numeric($req->vipmoney) && $req->vipmoney >= 0)
        {
            $vipmoney = $req->vipmoney;
        }
        else
        {
            return redirect()->back()->with('alerterr','Số tiền khách hàng thân thiết phải là số');
        }
        $name = [
                    'email',
                    'title',
                    'dienthoai',
                    'tenwebsite',
                    'linkwebsite',
                    'googlemap',
                    'lienhe',
                    'gioithieu',
                    'footer',
                    'facebook',
                    'youtube',
                    'instagram',
                    'vipmoney',
                    'email_gmail',
                    'password_gmail',
                    'dieukhoandichvu',
                    'chinhsachbaomat',
                    'thoigianlamviec',
                ];
        $data = [
                    array('chitiet' => $req->email),
                    array('chitiet' => $req->title),
                    array('chitiet' => $req->dienthoai),
                    array('chitiet' => $req->tenwebsite),
                    array('chitiet' => $req->linkwebsite),
                    array('chitiet' => $req->googlemap),
                    array('chitiet' => $req->lienhe),
                    array('chitiet' => $req->gioithieu),
                    array('chitiet' => $req->footer),
                    array('chitiet' => $req->facebook),
                    array('chitiet' => $req->youtube),
                    array('chitiet' => $req->instagram),
                    array('chitiet' => $req->vipmoney),
                    array('chitiet' => $req->email_gmail),
                    array('chitiet' => $req->password_gmail),
                    array('chitiet' => $req->dieukhoandichvu),
                    array('chitiet' => $req->chinhsachbaomat),
                    array('chitiet' => $req->thoigianlamviec),
                ];

        if($req->hasFile('photos')) 
        {
            //Quy dinh duoi
            $allowedfileExtension=['jpeg', 'jpg', 'png', 'gif'];
            //Lay image
            $file = $req->file('photos');
            //Lay duoi image
            $extension = strtolower($file->getClientOriginalExtension());
            //Kiem tra duoi co trong quy dinh khong
            $check=in_array($extension,$allowedfileExtension);
            if($check) {
                //upload tung image
                $filename = $req->photos->store('logo');
                $imgName = array('chitiet' => $filename);
                array_push($data, $imgName);
                
                for($i = 0 ; $i<count($name) ; $i++)
                {
                    for($j = 0 ; $j<count($data) ; $j++)
                    {
                        if($i==$j)
                        {
                            $this->cauhinh->destroyImg($req->hinhcu);
                            $this->cauhinh->updateData($name[$i],$data[$j]);
                        }
                    }
                }
                return redirect()->back()->with('alertsuc','Sửa thành công');
            }else{
                return redirect()->back()->with('alerterr','Vui lòng chọn đúng định dạng ảnh');
            }
        }
        else
        {
            for($i = 0 ; $i<count($name) ; $i++)
            {
                for($j = 0 ; $j<count($data) ; $j++)
                {
                    if($i==$j)
                    {
                        $this->cauhinh->updateData($name[$i],$data[$j]);
                    }
                }
            }
            return redirect()->back()->with('alertsuc','Sửa thành công');
        }
           
    }
}