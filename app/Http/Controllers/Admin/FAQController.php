<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFAQ;
use App\Model\FAQ;
use DateTime,DB;

class FAQController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $faq;
    public function __construct(FAQ $faq)
    {
        $this->faq = $faq;
    }
    public function index()
    {
        $data['faq'] = $this->faq->ListFAQ();

        return view('admin.module.faq.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.module.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFAQ $req)
    {
        try {
             $data = array(
                        'title' => $req->title,
                        'content' => $req->content,
                        'created_at'    => new DateTime,
                    ); 
           
            $this->faq->storeFAQ($data);
            
        } catch (Exception $e) {
            
            return back()->with('alerterr','Wrong format');
        }
            return redirect()->back()->with('alertsuc','Thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['faq'] = $this->faq->editFAQ($id);

        return view('admin.module.faq.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        try {
             $data = array(
                        'title' => $req->title,
                        'content' => $req->content,
                        'updated_at'    => new DateTime,
                    ); 
           
            $this->faq->updateFAQ($data,$id);
            
        } catch (Exception $e) {
            
            return back()->with('alerterr','Wrong format');
        }
            return redirect()->back()->with('alertsuc','Thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        $data = $req->input('checked',[]);

        for ($i=0; $i < count($data) ; $i++) { 
             $this->faq->delFAQ($data[$i]);
        }
        return redirect()->route('faq.index')->with('alertsuc','Xóa thành công');
    }
}
