<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Client;
use DateTime;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $client;
    public function __construct(Client $client)
    {
       $this->client =  $client;
    }
    public function index()
    {
        $data['client'] = $this->client->ListClient();

        return view('admin.module.client.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['client'] = $this->client->editClient($id);

        return view('admin.module.client.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        try {
            $data  = array(
                'fullname' => $req->fullname,
                'phone'    => $req->phone,
                'khuvuc'    => $req->khuvuc,
                'thunhap'   => $req->thunhap,
                'ngaysinh'  => $req->ngaysinh,
                'thangsinh' => $req->thangsinh,
                'namsinh'   => $req->namsinh,
                'updated_at'    => new DateTime,
            );
            $this->client->updateClient($data,$id);
        } catch (Exception $e) {
            return back()->with('alerterr','Wrong format');
        }
            return redirect()->back()->with('alertsuc','Thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        $data = $req->input('checked',[]);

        for ($i=0; $i < count($data) ; $i++) { 
             $this->client->delClient($data[$i]);
        }
        return redirect()->route('client.index')->with('alertsuc','Xóa thành công');
    }
}
