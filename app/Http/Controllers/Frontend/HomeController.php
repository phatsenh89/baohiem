<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Client;
use App\Model\Lienhe;
use App\Model\Menu;
use App\Model\News;
use App\Model\Cauhinh;
use DateTime;
use DB;

class HomeController extends Controller
{
	private $client;
	private $lienhe;
	private $menu;
	private $news;
    private $cauhinh;
    public function __construct(
    	Client $client, 
    	Lienhe $lienhe,
    	Menu $menu,
    	News $news,
        Cauhinh $cauhinh
    )
    {
       $this->client =  $client;
       $this->lienhe = $lienhe;
       $this->menu = $menu;
       $this->news = $news;
       $this->cauhinh          = $cauhinh;
    }

    public function index()
    {
    	return view('frontend.pages.index');
    }
    public function GetBHNT()
    {
    	return view('frontend.pages.bhnttrenho');
    }
    public function gioithieu()
    {
        $data['gioithieu'] = $this->cauhinh->getData('gioithieu');
    	return view('frontend.pages.gioithieu',$data);
    }
    public function Getlienhe()
    {
    	return view('frontend.pages.lienhe');
    }
    public function Postlienhe(Request $req)
    {
    	try {
            $data  = array(
                'name' => $req->name,
                'phone'    => $req->phone,
                'message'    => $req->message,
                'created_at'    => new DateTime,
            );
            $this->lienhe->storeLienhe($data);
        } catch (Exception $e) {
            return back()->with('alerterr','Wrong format');
        }
            return redirect()->back()->with('success','Thành công');
    }
    public function dieukhoandichvu()
    {
        $data['dieukhoan'] = $this->cauhinh->getData('dieukhoandichvu');
       
    	return view('frontend.pages.dieukhoan',$data);
    }
    public function chinhsachbaomat()
    {
        $data['chinhsach'] = $this->cauhinh->getData('chinhsachbaomat');
    	return view('frontend.pages.chinhsach',$data);
    }
    public function GetTuvan()
    {
    	return view('frontend.pages.tuvanFree');
    }
    public function PostTuvan(Request $req)
    {
    	try {
            $data  = array(
                'fullname' => $req->fullname,
                'phone'    => $req->phone,
                'khuvuc'    => $req->khuvuc,
                'thunhap'   => $req->thunhap,
                'ngaysinh'  => $req->ngaysinh,
                'thangsinh' => $req->thangsinh,
                'namsinh'   => $req->namsinh,
                'created_at'    => new DateTime,
            );
            $this->client->storeClient($data);
        } catch (Exception $e) {
            return back()->with('alerterr','Wrong format');
        }
            return redirect()->back()->with('success','Thành công');
    }
    public function GetNews(Request $req)
    {
    	$data['news'] = $this->news->GetNewsList();
    	return view('frontend.pages.baiviet',$data);
    }
    public function GetNewsDetail($slug)
    {
    	$data['news_detail'] = DB::table('bh_tintuc')->where('slug',$slug)->first();
    	return view('frontend.pages.chitiettin',$data);
    }

    public function search(Request $request)
    {
        $tukhoa = $request->tukhoa;
        $tintuc  = DB::table('bh_tintuc')->where('title', 'like', "%$tukhoa%")->orWhere('intro','like',"%$tukhoa%")->get()->toArray();

        $news = $this->news->GetNewsList();
        return view('frontend.pages.search',
            [
                'tintuc' => $tintuc,
                'tukhoa' => $tukhoa,
                'news'     => $news
            ]
        );
    }
}
