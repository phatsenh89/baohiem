<?php

namespace App\Http\Middleware;

use Closure,Auth;

class LoginalClientInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if (Auth::guard('client')->check()) {
        if(Auth::guard('client')->user()->firstname != null || Auth::guard('client')->user()->lastname != null)
        {
            return $next($request);
        }else{
            return redirect()->route('get.changeInfo');
        }
        // }else{
        //      return $next($request);
        // }
    }
}
