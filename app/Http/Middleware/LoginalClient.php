<?php

namespace App\Http\Middleware;

use Closure,Auth;

class LoginalClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('client')->check()) {
            if(Auth::guard('client')->user()->firstname != null || Auth::guard('client')->user()->lastname != null)
            {
                return redirect()->back();
            }else{
                return redirect()->route('get.changeInfo');
            }
        }else{
             return $next($request);
        }
    }
}
