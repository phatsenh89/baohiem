<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMenu extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'name'          => 'required|max:50',
            
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => "Vui lòng nhập tên menu",
            'name.max'              => "Tên menu tối đa chỉ 50 ký tự.",
        ];
    }
}
