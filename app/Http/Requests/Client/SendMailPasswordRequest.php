<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class SendMailPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [    
            'checkemail'             => 'required|email',            
        ];
    }

    public function messages()
    {
        return [
            'checkemail.required'            => trans('message.email_required'),
            'checkemail.email'               => trans('message.email_email'),
        ];
    }
}
