<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'passwordnew'          => 'required|min:6|max:25',
            'repeatpasswordnew'    => 'required',
            
        ];
    }

    public function messages()
    {
        return [
            'passwordnew.min'              => trans('message.passwordnew_min'),
            'passwordnew.max'              => trans('message.passwordnew_max'),
            'passwordnew.required'         => trans('message.passwordnew_required'),
            'repeatpasswordnew.required'   => trans('message.repeatpasswordnew_required'),

        ];
    }
}
