<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class ChangeInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'      => 'required',
            'lastname'      => 'required',
            'phone'         => 'required',
            'email'        => 'required',
            'address'      => 'required'  
        ];
    }

    public function messages()
    {
        return [
            'firstname.required'         => trans('message.username_required'),
            'lastname.required'         => trans('message.fullname_required'),
            'phone.required'            => trans('message.phone_required'),
            'email.required'           => trans('message.diachi_required'),
            'address.required'         => trans('message.province_required'),
        ];
    }
}
