<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'          => 'required|unique:yu_client,username',
            'password'          => 'required|min:6|max:16',
            'email'             => 'required|unique:yu_client,email',
                // Rule::exists('email')->where(function ($query) {
                //     $query->where('email' , "!=", 'email');
                // }),
            // 'fullname'          => 'required',
            'phone'             => 'required|unique:yu_client,phone|min:10|max:11',
            'address'            => 'required'
            
        ];
    }

    public function messages()
    {
        return [
            'username.required'         => trans('message.username_required'),
            'username.unique'           => trans('message.username_unique'),
            'email.required'            => trans('message.email_required'),
            'email.unique'              => trans('message.email_unique'),
            'password.required'         => trans('message.password_required'),
            'password.min'              => trans('message.passwordnew_min'),
            'password.max'              => trans('message.passwordnew_max'),
            // 'fullname.required'         => trans('message.fullname_required'),
            'phone.required'            => trans('message.phone_required'),
            'phone.unique'              => trans('message.phone_unique'),
            'phone.min'                 => trans('message.phone_min'),
            'phone.max'                 => trans('message.phone_max'),
            'address.required'           => trans('message.address_required')
        ];
    }
}
