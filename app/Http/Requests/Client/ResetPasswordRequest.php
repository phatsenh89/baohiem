<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'newpassword'          => 'required|min:6|max:25',
            'renewpassword'        => 'required',

            
        ];
    }

    public function messages()
    {
        return [
            'newpassword.min'              => trans('message.passwordnew_min'),
            'newpassword.max'              => trans('message.passwordnew_max'),
            'newpassword.required'         => trans('message.passwordnew_required'),
            'renewpassword.required'       => trans('message.repeatpasswordnew_required'),

        ];
    }
}
