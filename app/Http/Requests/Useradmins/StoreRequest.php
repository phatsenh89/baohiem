<?php

namespace App\Http\Requests\Useradmins;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'          => 'required|unique:bh_admin,username',
            'password'          => 'required',
            'email'             => 'required|unique:bh_admin,email',
            'fullname'          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'username.required'         => trans('message.username_required'),
            'username.unique'           => trans('message.username_unique'),
            'email.required'            => trans('message.email_required'),
            'email.unique'              => trans('message.email_unique'),
            'password.required'         => trans('message.password_required'),
            'fullname.required'         => trans('message.fullname_required'),
        ];
    }
    
}
