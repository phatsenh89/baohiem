<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Admin')->group(function(){
	Route::get('sign-in','LoginController@getLogin')->middleware('loginal')->name('getLogin');
	Route::post('sign-in','LoginController@postLogin')->name('postLogin');
	Route::get('logout','LoginController@getLogout')->name('getLogout');

	Route::get('forgot-password', 'ResetPasswordController@getPassword')->name('getPassword');
	Route::post('forgot-password', 'ResetPasswordController@sendEmail')->name('sendEmail');
	Route::get('change-new-password', 'ResetPasswordController@getNewPassword')->name('getNewPassword');
	Route::post('change-new-password', 'ResetPasswordController@postNewPassword')->name('postNewPassword');
});
Route::namespace('Frontend')->group(function() {
	Route::get('','HomeController@index')->name('home');
	Route::get('bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh','HomeController@GetBHNT')->name('bao-hiem-nhan-tho-cho-be-tre-em-con-tre-so-sinh');
	
	Route::get('gioi-thieu','HomeController@gioithieu')->name('gioi-thieu');
	Route::get('lien-he','HomeController@Getlienhe')->name('lien-he');
	Route::post('lien-he','HomeController@Postlienhe')->name('lien-he');
	Route::get('dieu-khoan-dich-vu','HomeController@dieukhoandichvu')->name('dieu-khoan-dich-vu');
	Route::get('chinh-sach-bao-mat','HomeController@chinhsachbaomat')->name('chinh-sach-bao-mat');
	Route::get('tu-van-mien-phi','HomeController@GetTuvan')->name('tu-van-mien-phi');
	Route::post('tu-van-mien-phi','HomeController@PostTuvan')->name('tu-van-mien-phi');
	Route::get('bai-viet','HomeController@GetNews')->name('bai-viet');
	Route::get('bai-viet-detail/{slug}','HomeController@GetNewsDetail')->name('bai-viet-detail');

	Route::post('tim-kiem','HomeController@search');
	
});

Route::namespace('Admin')->middleware('login')->prefix('admin')->group(function(){

	Route::get('dashboard','HomeController@index')->name('dashboard');

	Route::prefix('useradmin')->name('useradmin.')->group(function(){
		Route::get('index','UseradminController@index')->name('index');

		Route::get('add','UseradminController@create')->name('create');
		Route::post('add','UseradminController@store')->name('store');

		Route::get('edit/{id}','UseradminController@edit')->name('edit');
		Route::post('edit/{id}','UseradminController@update')->name('update');

		Route::get('change-password','UseradminController@Getchangepass')->name('change-password');
		Route::post('change-password','UseradminController@Postchangepass');

		Route::get('delete/{id}','UseradminController@destroy')->name('destroy');
	});
	Route::prefix('menu')->name('menu.')->group(function(){
		Route::get('index','MenuController@index')->name('index');

		Route::get('add','MenuController@create')->name('create');
		Route::post('add','MenuController@store')->name('store');

		Route::get('show-detail/{id}','MenuController@show')->name('show');

		Route::get('edit/{id}','MenuController@edit')->name('edit');
		Route::post('edit/{id}','MenuController@update')->name('update');

		Route::post('delete/{id}','MenuController@destroy')->name('destroy');
	});
	Route::prefix('lien-he')->name('lienhe.')->group(function(){
		Route::get('index','LienheController@index')->name('index');

		Route::get('add','LienheController@create')->name('create');
		Route::post('add','LienheController@store')->name('store');

		Route::get('show-detail/{id}','LienheController@show')->name('show');

		Route::get('edit/{id}','LienheController@edit')->name('edit');
		Route::post('edit/{id}','LienheController@update')->name('update');
		
		Route::post('delete/{id}','LienheController@destroy')->name('destroy');
	});
	Route::prefix('FAQ')->name('faq.')->group(function(){
		Route::get('index','FAQController@index')->name('index');

		Route::get('add','FAQController@create')->name('create');
		Route::post('add','FAQController@store')->name('store');

		Route::get('show-detail/{id}','FAQController@show')->name('show');

		Route::get('edit/{id}','FAQController@edit')->name('edit');
		Route::post('edit/{id}','FAQController@update')->name('update');
		
		Route::post('delete/{id}','FAQController@destroy')->name('destroy');
	});
	Route::prefix('tin-tuc')->name('news.')->group(function(){
		Route::get('index','NewsController@index')->name('index');

		Route::get('add','NewsController@create')->name('create');
		Route::post('add','NewsController@store')->name('store');

		Route::get('show-detail/{id}','NewsController@show')->name('show');

		Route::get('edit/{id}','NewsController@edit')->name('edit');
		Route::post('edit/{id}','NewsController@update')->name('update');
		
		Route::post('delete/{id}','NewsController@destroy')->name('destroy');
	});
	Route::prefix('client')->name('client.')->group(function(){
		Route::get('index','ClientController@index')->name('index');

		Route::get('add','ClientController@create')->name('create');
		Route::post('add','ClientController@store')->name('store');

		Route::get('show-detail/{id}','ClientController@show')->name('show');

		Route::get('edit/{id}','ClientController@edit')->name('edit');
		Route::post('edit/{id}','ClientController@update')->name('update');
		
		Route::post('delete/{id}','ClientController@destroy')->name('destroy');
	});
	
	Route::get('cauhinh','CauhinhController@editNoId')->name('cauhinh.editNoId');
	Route::put('cauhinh','CauhinhController@updateNoId')->name('cauhinh.updateNoId');
});


